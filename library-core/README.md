Graphics-Core
===============

This module contains core graphics elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Agraphics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Agraphics/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:graphics-core:${DESIRED_VERSION}@aar"
