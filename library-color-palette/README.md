Graphics-Color-Palette
===============

This module contains **palette** with more than **1,000** colors.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Agraphics/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Agraphics/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:graphics-color-palette:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Color](https://bitbucket.org/android-universum/graphics/main/library-color-palette/src/main/java/universum/studios/android/graphics/Color.java)
