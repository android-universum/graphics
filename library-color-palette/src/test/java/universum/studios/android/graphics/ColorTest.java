/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.graphics;

import org.junit.Test;

import androidx.annotation.ColorInt;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

/**
 * @author Martin Albedinsky
 */
public final class ColorTest extends AndroidTestCase {

	@Test public void testGeneratedColors() {
		// Assert:
		assertThatColorIsNotTransparent(Color.AERO);
		assertThatColorIsNotTransparent(Color.AERO_BLUE);
		assertThatColorIsNotTransparent(Color.AFRICAN_VIOLET);
		assertThatColorIsNotTransparent(Color.AIR_FORCE_BLUE_RAF);
		assertThatColorIsNotTransparent(Color.AIR_FORCE_BLUE_USAF);
		assertThatColorIsNotTransparent(Color.AIR_SUPERIORITY_BLUE);
		assertThatColorIsNotTransparent(Color.ALABAMA_CRIMSON);
		assertThatColorIsNotTransparent(Color.ALICE_BLUE);
		assertThatColorIsNotTransparent(Color.ALIZARIN_CRIMSON);
		assertThatColorIsNotTransparent(Color.ALLOY_ORANGE);
		assertThatColorIsNotTransparent(Color.ALMOND);
		assertThatColorIsNotTransparent(Color.AMARANTH);
		assertThatColorIsNotTransparent(Color.AMAZON);
		assertThatColorIsNotTransparent(Color.AMBER);
		assertThatColorIsNotTransparent(Color.SAE_ECE_AMBER_COLOR);
		assertThatColorIsNotTransparent(Color.AMERICAN_ROSE);
		assertThatColorIsNotTransparent(Color.AMETHYST);
		assertThatColorIsNotTransparent(Color.ANDROID_GREEN);
		assertThatColorIsNotTransparent(Color.ANTI_FLASH_WHITE);
		assertThatColorIsNotTransparent(Color.ANTIQUE_BRASS);
		assertThatColorIsNotTransparent(Color.ANTIQUE_BRONZE);
		assertThatColorIsNotTransparent(Color.ANTIQUE_FUCHSIA);
		assertThatColorIsNotTransparent(Color.ANTIQUE_RUBY);
		assertThatColorIsNotTransparent(Color.ANTIQUE_WHITE);
		assertThatColorIsNotTransparent(Color.AO_ENGLISH);
		assertThatColorIsNotTransparent(Color.APPLE_GREEN);
		assertThatColorIsNotTransparent(Color.APRICOT);
		assertThatColorIsNotTransparent(Color.AQUA);
		assertThatColorIsNotTransparent(Color.AQUAMARINE);
		assertThatColorIsNotTransparent(Color.ARMY_GREEN);
		assertThatColorIsNotTransparent(Color.ARSENIC);
		assertThatColorIsNotTransparent(Color.ARYLIDE_YELLOW);
		assertThatColorIsNotTransparent(Color.ASH_GREY);
		assertThatColorIsNotTransparent(Color.ASPARAGUS);
		assertThatColorIsNotTransparent(Color.ATOMIC_TANGERINE);
		assertThatColorIsNotTransparent(Color.AUBURN);
		assertThatColorIsNotTransparent(Color.AUREOLIN);
		assertThatColorIsNotTransparent(Color.AUROMETALSAURUS);
		assertThatColorIsNotTransparent(Color.AVOCADO);
		assertThatColorIsNotTransparent(Color.AZURE);
		assertThatColorIsNotTransparent(Color.AZURE_MIST_WEB);
		assertThatColorIsNotTransparent(Color.BABY_BLUE);
		assertThatColorIsNotTransparent(Color.BABY_BLUE_EYES);
		assertThatColorIsNotTransparent(Color.BABY_PINK);
		assertThatColorIsNotTransparent(Color.BABY_POWDER);
		assertThatColorIsNotTransparent(Color.BAKER_MILLER_PINK);
		assertThatColorIsNotTransparent(Color.BALL_BLUE);
		assertThatColorIsNotTransparent(Color.BANANA_MANIA);
		assertThatColorIsNotTransparent(Color.BANANA_YELLOW);
		assertThatColorIsNotTransparent(Color.BARBIE_PINK);
		assertThatColorIsNotTransparent(Color.BARN_RED);
		assertThatColorIsNotTransparent(Color.BATTLESHIP_GREY);
		assertThatColorIsNotTransparent(Color.BAZAAR);
		assertThatColorIsNotTransparent(Color.BEAU_BLUE);
		assertThatColorIsNotTransparent(Color.BEAVER);
		assertThatColorIsNotTransparent(Color.BEIGE);
		assertThatColorIsNotTransparent(Color.B_DAZZLED_BLUE);
		assertThatColorIsNotTransparent(Color.BIG_DIP_O_RUBY);
		assertThatColorIsNotTransparent(Color.BISQUE);
		assertThatColorIsNotTransparent(Color.BISTRE);
		assertThatColorIsNotTransparent(Color.BISTRE_BROWN);
		assertThatColorIsNotTransparent(Color.BITTER_LEMON);
		assertThatColorIsNotTransparent(Color.BITTER_LIME);
		assertThatColorIsNotTransparent(Color.BITTERSWEET);
		assertThatColorIsNotTransparent(Color.BITTERSWEET_SHIMMER);
		assertThatColorIsNotTransparent(Color.BLACK);
		assertThatColorIsNotTransparent(Color.BLACK_BEAN);
		assertThatColorIsNotTransparent(Color.BLACK_LEATHER_JACKET);
		assertThatColorIsNotTransparent(Color.BLACK_OLIVE);
		assertThatColorIsNotTransparent(Color.BLANCHED_ALMOND);
		assertThatColorIsNotTransparent(Color.BLAST_OFF_BRONZE);
		assertThatColorIsNotTransparent(Color.BLEU_DE_FRANCE);
		assertThatColorIsNotTransparent(Color.BLIZZARD_BLUE);
		assertThatColorIsNotTransparent(Color.BLOND);
		// todo: other colors
	}

	private static void assertThatColorIsNotTransparent(@ColorInt int color) {
		assertThat(color, is(not(Color.TRANSPARENT)));
	}
}