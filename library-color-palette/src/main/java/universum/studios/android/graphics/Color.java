/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.graphics;

import androidx.annotation.ColorInt;

/**
 * Extended implementation of android's {@link android.graphics.Color} with a lot of predefined colors.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@SuppressWarnings("unused")
public class Color extends android.graphics.Color {

	/*
	 * All colors specified in this class were parsed from links below.
	 * <p>
	 * link_1: http://en.wikipedia.org/wiki/List_of_colors:_A-M#
	 * Colors_in_alphabetical_order_A-M
	 * <p>
	 * link_2: http://en.wikipedia.org/wiki/List_of_colors:_N-Z#
	 * Colors_in_alphabetical_order_N-Z
	 */

	/**
	 * Constant value: <b>-8603160 [0xff7cb9e8]</b>
	 * <p>
	 * Hex (RGB): <b>#7cb9e8</b>
	 */
	@ColorInt public static final int AERO = Color.rgb(124, 185, 232);

	/**
	 * Constant value: <b>-3538971 [0xffc9ffe5]</b>
	 * <p>
	 * Hex (RGB): <b>#c9ffe5</b>
	 */
	@ColorInt public static final int AERO_BLUE = Color.rgb(201, 255, 229);

	/**
	 * Constant value: <b>-5077826 [0xffb284be]</b>
	 * <p>
	 * Hex (RGB): <b>#b284be</b>
	 */
	@ColorInt public static final int AFRICAN_VIOLET = Color.rgb(178, 132, 190);

	/**
	 * Constant value: <b>-10646872 [0xff5d8aa8]</b>
	 * <p>
	 * Hex (RGB): <b>#5d8aa8</b>
	 */
	@ColorInt public static final int AIR_FORCE_BLUE_RAF = Color.rgb(93, 138, 168);

	/**
	 * Constant value: <b>-16764785 [0xff308f]</b>
	 * <p>
	 * Hex (RGB): <b>#00308f</b>
	 */
	@ColorInt public static final int AIR_FORCE_BLUE_USAF = Color.rgb(0, 48, 143);

	/**
	 * Constant value: <b>-9264959 [0xff72a0c1]</b>
	 * <p>
	 * Hex (RGB): <b>#72a0c1</b>
	 */
	@ColorInt public static final int AIR_SUPERIORITY_BLUE = Color.rgb(114, 160, 193);

	/**
	 * Constant value: <b>-6085064 [0xffa32638]</b>
	 * <p>
	 * Hex (RGB): <b>#a32638</b>
	 */
	@ColorInt public static final int ALABAMA_CRIMSON = Color.rgb(163, 38, 56);

	/**
	 * Constant value: <b>-984833 [0xfff0f8ff]</b>
	 * <p>
	 * Hex (RGB): <b>#f0f8ff</b>
	 */
	@ColorInt public static final int ALICE_BLUE = Color.rgb(240, 248, 255);

	/**
	 * Constant value: <b>-1890762 [0xffe32636]</b>
	 * <p>
	 * Hex (RGB): <b>#e32636</b>
	 */
	@ColorInt public static final int ALIZARIN_CRIMSON = Color.rgb(227, 38, 54);

	/**
	 * Constant value: <b>-3907056 [0xffc46210]</b>
	 * <p>
	 * Hex (RGB): <b>#c46210</b>
	 */
	@ColorInt public static final int ALLOY_ORANGE = Color.rgb(196, 98, 16);

	/**
	 * Constant value: <b>-1057075 [0xffefdecd]</b>
	 * <p>
	 * Hex (RGB): <b>#efdecd</b>
	 */
	@ColorInt public static final int ALMOND = Color.rgb(239, 222, 205);

	/**
	 * Constant value: <b>-1758384 [0xffe52b50]</b>
	 * <p>
	 * Hex (RGB): <b>#e52b50</b>
	 */
	@ColorInt public static final int AMARANTH = Color.rgb(229, 43, 80);

	/**
	 * Constant value: <b>-12879273 [0xff3b7a57]</b>
	 * <p>
	 * Hex (RGB): <b>#3b7a57</b>
	 */
	@ColorInt public static final int AMAZON = Color.rgb(59, 122, 87);

	/**
	 * Constant value: <b>-16640 [0xffffbf00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffbf00</b>
	 */
	@ColorInt public static final int AMBER = Color.rgb(255, 191, 0);

	/**
	 * Constant value: <b>-33280 [0xffff7e00]</b>
	 * <p>
	 * Hex (RGB): <b>#ff7e00</b>
	 */
	@ColorInt public static final int SAE_ECE_AMBER_COLOR = Color.rgb(255, 126, 0);

	/**
	 * Constant value: <b>-64706 [0xffff033e]</b>
	 * <p>
	 * Hex (RGB): <b>#ff033e</b>
	 */
	@ColorInt public static final int AMERICAN_ROSE = Color.rgb(255, 3, 62);

	/**
	 * Constant value: <b>-6723892 [0xff9966cc]</b>
	 * <p>
	 * Hex (RGB): <b>#9966cc</b>
	 */
	@ColorInt public static final int AMETHYST = Color.rgb(153, 102, 204);

	/**
	 * Constant value: <b>-5978567 [0xffa4c639]</b>
	 * <p>
	 * Hex (RGB): <b>#a4c639</b>
	 */
	@ColorInt public static final int ANDROID_GREEN = Color.rgb(164, 198, 57);

	/**
	 * Constant value: <b>-855052 [0xfff2f3f4]</b>
	 * <p>
	 * Hex (RGB): <b>#f2f3f4</b>
	 */
	@ColorInt public static final int ANTI_FLASH_WHITE = Color.rgb(242, 243, 244);

	/**
	 * Constant value: <b>-3304075 [0xffcd9575]</b>
	 * <p>
	 * Hex (RGB): <b>#cd9575</b>
	 */
	@ColorInt public static final int ANTIQUE_BRASS = Color.rgb(205, 149, 117);

	/**
	 * Constant value: <b>-10068706 [0xff665d1e]</b>
	 * <p>
	 * Hex (RGB): <b>#665d1e</b>
	 */
	@ColorInt public static final int ANTIQUE_BRONZE = Color.rgb(102, 93, 30);

	/**
	 * Constant value: <b>-7250813 [0xff915c83]</b>
	 * <p>
	 * Hex (RGB): <b>#915c83</b>
	 */
	@ColorInt public static final int ANTIQUE_FUCHSIA = Color.rgb(145, 92, 131);

	/**
	 * Constant value: <b>-8119507 [0xff841b2d]</b>
	 * <p>
	 * Hex (RGB): <b>#841b2d</b>
	 */
	@ColorInt public static final int ANTIQUE_RUBY = Color.rgb(132, 27, 45);

	/**
	 * Constant value: <b>-332841 [0xfffaebd7]</b>
	 * <p>
	 * Hex (RGB): <b>#faebd7</b>
	 */
	@ColorInt public static final int ANTIQUE_WHITE = Color.rgb(250, 235, 215);

	/**
	 * Constant value: <b>-16744448 [0xff8000]</b>
	 * <p>
	 * Hex (RGB): <b>#008000</b>
	 */
	@ColorInt public static final int AO_ENGLISH = Color.rgb(0, 128, 0);

	/**
	 * Constant value: <b>-7490048 [0xff8db600]</b>
	 * <p>
	 * Hex (RGB): <b>#8db600</b>
	 */
	@ColorInt public static final int APPLE_GREEN = Color.rgb(141, 182, 0);

	/**
	 * Constant value: <b>-274767 [0xfffbceb1]</b>
	 * <p>
	 * Hex (RGB): <b>#fbceb1</b>
	 */
	@ColorInt public static final int APRICOT = Color.rgb(251, 206, 177);

	/**
	 * Constant value: <b>-16711681 [0xffffff]</b>
	 * <p>
	 * Hex (RGB): <b>#00ffff</b>
	 */
	@ColorInt public static final int AQUA = Color.rgb(0, 255, 255);

	/**
	 * Constant value: <b>-8388652 [0xff7fffd4]</b>
	 * <p>
	 * Hex (RGB): <b>#7fffd4</b>
	 */
	@ColorInt public static final int AQUAMARINE = Color.rgb(127, 255, 212);

	/**
	 * Constant value: <b>-11840736 [0xff4b5320]</b>
	 * <p>
	 * Hex (RGB): <b>#4b5320</b>
	 */
	@ColorInt public static final int ARMY_GREEN = Color.rgb(75, 83, 32);

	/**
	 * Constant value: <b>-12893109 [0xff3b444b]</b>
	 * <p>
	 * Hex (RGB): <b>#3b444b</b>
	 */
	@ColorInt public static final int ARSENIC = Color.rgb(59, 68, 75);

	/**
	 * Constant value: <b>-1452437 [0xffe9d66b]</b>
	 * <p>
	 * Hex (RGB): <b>#e9d66b</b>
	 */
	@ColorInt public static final int ARYLIDE_YELLOW = Color.rgb(233, 214, 107);

	/**
	 * Constant value: <b>-5062987 [0xffb2beb5]</b>
	 * <p>
	 * Hex (RGB): <b>#b2beb5</b>
	 */
	@ColorInt public static final int ASH_GREY = Color.rgb(178, 190, 181);

	/**
	 * Constant value: <b>-7886485 [0xff87a96b]</b>
	 * <p>
	 * Hex (RGB): <b>#87a96b</b>
	 */
	@ColorInt public static final int ASPARAGUS = Color.rgb(135, 169, 107);

	/**
	 * Constant value: <b>-26266 [0xffff9966]</b>
	 * <p>
	 * Hex (RGB): <b>#ff9966</b>
	 */
	@ColorInt public static final int ATOMIC_TANGERINE = Color.rgb(255, 153, 102);

	/**
	 * Constant value: <b>-5952982 [0xffa52a2a]</b>
	 * <p>
	 * Hex (RGB): <b>#a52a2a</b>
	 */
	@ColorInt public static final int AUBURN = Color.rgb(165, 42, 42);

	/**
	 * Constant value: <b>-135680 [0xfffdee00]</b>
	 * <p>
	 * Hex (RGB): <b>#fdee00</b>
	 */
	@ColorInt public static final int AUREOLIN = Color.rgb(253, 238, 0);

	/**
	 * Constant value: <b>-9535616 [0xff6e7f80]</b>
	 * <p>
	 * Hex (RGB): <b>#6e7f80</b>
	 */
	@ColorInt public static final int AUROMETALSAURUS = Color.rgb(110, 127, 128);

	/**
	 * Constant value: <b>-11107837 [0xff568203]</b>
	 * <p>
	 * Hex (RGB): <b>#568203</b>
	 */
	@ColorInt public static final int AVOCADO = Color.rgb(86, 130, 3);

	/**
	 * Constant value: <b>-16744449 [0xff7fff]</b>
	 * <p>
	 * Hex (RGB): <b>#007fff</b>
	 */
	@ColorInt public static final int AZURE = Color.rgb(0, 127, 255);

	/**
	 * Constant value: <b>-983041 [0xfff0ffff]</b>
	 * <p>
	 * Hex (RGB): <b>#f0ffff</b>
	 */
	@ColorInt public static final int AZURE_MIST_WEB = Color.rgb(240, 255, 255);

	/**
	 * Constant value: <b>-7745552 [0xff89cff0]</b>
	 * <p>
	 * Hex (RGB): <b>#89cff0</b>
	 */
	@ColorInt public static final int BABY_BLUE = Color.rgb(137, 207, 240);

	/**
	 * Constant value: <b>-6173967 [0xffa1caf1]</b>
	 * <p>
	 * Hex (RGB): <b>#a1caf1</b>
	 */
	@ColorInt public static final int BABY_BLUE_EYES = Color.rgb(161, 202, 241);

	/**
	 * Constant value: <b>-736574 [0xfff4c2c2]</b>
	 * <p>
	 * Hex (RGB): <b>#f4c2c2</b>
	 */
	@ColorInt public static final int BABY_PINK = Color.rgb(244, 194, 194);

	/**
	 * Constant value: <b>-65798 [0xfffefefa]</b>
	 * <p>
	 * Hex (RGB): <b>#fefefa</b>
	 */
	@ColorInt public static final int BABY_POWDER = Color.rgb(254, 254, 250);

	/**
	 * Constant value: <b>-28241 [0xffff91af]</b>
	 * <p>
	 * Hex (RGB): <b>#ff91af</b>
	 */
	@ColorInt public static final int BAKER_MILLER_PINK = Color.rgb(255, 145, 175);

	/**
	 * Constant value: <b>-14570547 [0xff21abcd]</b>
	 * <p>
	 * Hex (RGB): <b>#21abcd</b>
	 */
	@ColorInt public static final int BALL_BLUE = Color.rgb(33, 171, 205);

	/**
	 * Constant value: <b>-333899 [0xfffae7b5]</b>
	 * <p>
	 * Hex (RGB): <b>#fae7b5</b>
	 */
	@ColorInt public static final int BANANA_MANIA = Color.rgb(250, 231, 181);

	/**
	 * Constant value: <b>-7883 [0xffffe135]</b>
	 * <p>
	 * Hex (RGB): <b>#ffe135</b>
	 */
	@ColorInt public static final int BANANA_YELLOW = Color.rgb(255, 225, 53);

	/**
	 * Constant value: <b>-2088566 [0xffe0218a]</b>
	 * <p>
	 * Hex (RGB): <b>#e0218a</b>
	 */
	@ColorInt public static final int BARBIE_PINK = Color.rgb(224, 33, 138);

	/**
	 * Constant value: <b>-8648190 [0xff7c0a02]</b>
	 * <p>
	 * Hex (RGB): <b>#7c0a02</b>
	 */
	@ColorInt public static final int BARN_RED = Color.rgb(124, 10, 2);

	/**
	 * Constant value: <b>-8092542 [0xff848482]</b>
	 * <p>
	 * Hex (RGB): <b>#848482</b>
	 */
	@ColorInt public static final int BATTLESHIP_GREY = Color.rgb(132, 132, 130);

	/**
	 * Constant value: <b>-6785157 [0xff98777b]</b>
	 * <p>
	 * Hex (RGB): <b>#98777b</b>
	 */
	@ColorInt public static final int BAZAAR = Color.rgb(152, 119, 123);

	/**
	 * Constant value: <b>-4401946 [0xffbcd4e6]</b>
	 * <p>
	 * Hex (RGB): <b>#bcd4e6</b>
	 */
	@ColorInt public static final int BEAU_BLUE = Color.rgb(188, 212, 230);

	/**
	 * Constant value: <b>-6323856 [0xff9f8170]</b>
	 * <p>
	 * Hex (RGB): <b>#9f8170</b>
	 */
	@ColorInt public static final int BEAVER = Color.rgb(159, 129, 112);

	/**
	 * Constant value: <b>-657956 [0xfff5f5dc]</b>
	 * <p>
	 * Hex (RGB): <b>#f5f5dc</b>
	 */
	@ColorInt public static final int BEIGE = Color.rgb(245, 245, 220);

	/**
	 * Constant value: <b>-13739884 [0xff2e5894]</b>
	 * <p>
	 * Hex (RGB): <b>#2e5894</b>
	 */
	@ColorInt public static final int B_DAZZLED_BLUE = Color.rgb(46, 88, 148);

	/**
	 * Constant value: <b>-6544062 [0xff9c2542]</b>
	 * <p>
	 * Hex (RGB): <b>#9c2542</b>
	 */
	@ColorInt public static final int BIG_DIP_O_RUBY = Color.rgb(156, 37, 66);

	/**
	 * Constant value: <b>-6972 [0xffffe4c4]</b>
	 * <p>
	 * Hex (RGB): <b>#ffe4c4</b>
	 */
	@ColorInt public static final int BISQUE = Color.rgb(255, 228, 196);

	/**
	 * Constant value: <b>-12768481 [0xff3d2b1f]</b>
	 * <p>
	 * Hex (RGB): <b>#3d2b1f</b>
	 */
	@ColorInt public static final int BISTRE = Color.rgb(61, 43, 31);

	/**
	 * Constant value: <b>-6917865 [0xff967117]</b>
	 * <p>
	 * Hex (RGB): <b>#967117</b>
	 */
	@ColorInt public static final int BISTRE_BROWN = Color.rgb(150, 113, 23);

	/**
	 * Constant value: <b>-3481587 [0xffcae00d]</b>
	 * <p>
	 * Hex (RGB): <b>#cae00d</b>
	 */
	@ColorInt public static final int BITTER_LEMON = Color.rgb(202, 224, 13);

	/**
	 * Constant value: <b>-10187759 [0xff648c11]</b>
	 * <p>
	 * Hex (RGB): <b>#648c11</b>
	 */
	@ColorInt public static final int BITTER_LIME = Color.rgb(100, 140, 17);

	/**
	 * Constant value: <b>-102562 [0xfffe6f5e]</b>
	 * <p>
	 * Hex (RGB): <b>#fe6f5e</b>
	 */
	@ColorInt public static final int BITTERSWEET = Color.rgb(254, 111, 94);

	/**
	 * Constant value: <b>-4239535 [0xffbf4f51]</b>
	 * <p>
	 * Hex (RGB): <b>#bf4f51</b>
	 */
	@ColorInt public static final int BITTERSWEET_SHIMMER = Color.rgb(191, 79, 81);

	/**
	 * Constant value: <b>-16777216 [0xff0]</b>
	 * <p>
	 * Hex (RGB): <b>#000000</b>
	 */
	@ColorInt public static final int BLACK = Color.rgb(0, 0, 0);

	/**
	 * Constant value: <b>-12776446 [0xff3d0c02]</b>
	 * <p>
	 * Hex (RGB): <b>#3d0c02</b>
	 */
	@ColorInt public static final int BLACK_BEAN = Color.rgb(61, 12, 2);

	/**
	 * Constant value: <b>-14338775 [0xff253529]</b>
	 * <p>
	 * Hex (RGB): <b>#253529</b>
	 */
	@ColorInt public static final int BLACK_LEATHER_JACKET = Color.rgb(37, 53, 41);

	/**
	 * Constant value: <b>-12895178 [0xff3b3c36]</b>
	 * <p>
	 * Hex (RGB): <b>#3b3c36</b>
	 */
	@ColorInt public static final int BLACK_OLIVE = Color.rgb(59, 60, 54);

	/**
	 * Constant value: <b>-5171 [0xffffebcd]</b>
	 * <p>
	 * Hex (RGB): <b>#ffebcd</b>
	 */
	@ColorInt public static final int BLANCHED_ALMOND = Color.rgb(255, 235, 205);

	/**
	 * Constant value: <b>-5934748 [0xffa57164]</b>
	 * <p>
	 * Hex (RGB): <b>#a57164</b>
	 */
	@ColorInt public static final int BLAST_OFF_BRONZE = Color.rgb(165, 113, 100);

	/**
	 * Constant value: <b>-13529881 [0xff318ce7]</b>
	 * <p>
	 * Hex (RGB): <b>#318ce7</b>
	 */
	@ColorInt public static final int BLEU_DE_FRANCE = Color.rgb(49, 140, 231);

	/**
	 * Constant value: <b>-5446162 [0xfface5ee]</b>
	 * <p>
	 * Hex (RGB): <b>#ace5ee</b>
	 */
	@ColorInt public static final int BLIZZARD_BLUE = Color.rgb(172, 229, 238);

	/**
	 * Constant value: <b>-331586 [0xfffaf0be]</b>
	 * <p>
	 * Hex (RGB): <b>#faf0be</b>
	 */
	@ColorInt public static final int BLOND = Color.rgb(250, 240, 190);

	/**
	 * Constant value: <b>-16776961 [0xffff]</b>
	 * <p>
	 * Hex (RGB): <b>#0000ff</b>
	 */
	@ColorInt public static final int BLUE = Color.rgb(0, 0, 255);

	/**
	 * Constant value: <b>-14715394 [0xff1f75fe]</b>
	 * <p>
	 * Hex (RGB): <b>#1f75fe</b>
	 */
	@ColorInt public static final int BLUE_CRAYOLA = Color.rgb(31, 117, 254);

	/**
	 * Constant value: <b>-16739409 [0xff93af]</b>
	 * <p>
	 * Hex (RGB): <b>#0093af</b>
	 */
	@ColorInt public static final int BLUE_MUNSELL = Color.rgb(0, 147, 175);

	/**
	 * Constant value: <b>-16742467 [0xff87bd]</b>
	 * <p>
	 * Hex (RGB): <b>#0087bd</b>
	 */
	@ColorInt public static final int BLUE_NCS = Color.rgb(0, 135, 189);

	/**
	 * Constant value: <b>-13421671 [0xff333399]</b>
	 * <p>
	 * Hex (RGB): <b>#333399</b>
	 */
	@ColorInt public static final int BLUE_PIGMENT = Color.rgb(51, 51, 153);

	/**
	 * Constant value: <b>-16627714 [0xff247fe]</b>
	 * <p>
	 * Hex (RGB): <b>#0247fe</b>
	 */
	@ColorInt public static final int BLUE_RYB = Color.rgb(2, 71, 254);

	/**
	 * Constant value: <b>-6118704 [0xffa2a2d0]</b>
	 * <p>
	 * Hex (RGB): <b>#a2a2d0</b>
	 */
	@ColorInt public static final int BLUE_BELL = Color.rgb(162, 162, 208);

	/**
	 * Constant value: <b>-10053172 [0xff6699cc]</b>
	 * <p>
	 * Hex (RGB): <b>#6699cc</b>
	 */
	@ColorInt public static final int BLUE_GRAY = Color.rgb(102, 153, 204);

	/**
	 * Constant value: <b>-15886150 [0xffd98ba]</b>
	 * <p>
	 * Hex (RGB): <b>#0d98ba</b>
	 */
	@ColorInt public static final int BLUE_GREEN = Color.rgb(13, 152, 186);

	/**
	 * Constant value: <b>-15572608 [0xff126180]</b>
	 * <p>
	 * Hex (RGB): <b>#126180</b>
	 */
	@ColorInt public static final int BLUE_SAPPHIRE = Color.rgb(18, 97, 128);

	/**
	 * Constant value: <b>-7722014 [0xff8a2be2]</b>
	 * <p>
	 * Hex (RGB): <b>#8a2be2</b>
	 */
	@ColorInt public static final int BLUE_VIOLET = Color.rgb(138, 43, 226);

	/**
	 * Constant value: <b>-11504985 [0xff5072a7]</b>
	 * <p>
	 * Hex (RGB): <b>#5072a7</b>
	 */
	@ColorInt public static final int BLUE_YONDER = Color.rgb(80, 114, 167);

	/**
	 * Constant value: <b>-11565321 [0xff4f86f7]</b>
	 * <p>
	 * Hex (RGB): <b>#4f86f7</b>
	 */
	@ColorInt public static final int BLUEBERRY = Color.rgb(79, 134, 247);

	/**
	 * Constant value: <b>-14934800 [0xff1c1cf0]</b>
	 * <p>
	 * Hex (RGB): <b>#1c1cf0</b>
	 */
	@ColorInt public static final int BLUEBONNET = Color.rgb(28, 28, 240);

	/**
	 * Constant value: <b>-2204285 [0xffde5d83]</b>
	 * <p>
	 * Hex (RGB): <b>#de5d83</b>
	 */
	@ColorInt public static final int BLUSH = Color.rgb(222, 93, 131);

	/**
	 * Constant value: <b>-8829893 [0xff79443b]</b>
	 * <p>
	 * Hex (RGB): <b>#79443b</b>
	 */
	@ColorInt public static final int BOLE = Color.rgb(121, 68, 59);

	/**
	 * Constant value: <b>-16738890 [0xff95b6]</b>
	 * <p>
	 * Hex (RGB): <b>#0095b6</b>
	 */
	@ColorInt public static final int BONDI_BLUE = Color.rgb(0, 149, 182);

	/**
	 * Constant value: <b>-1844535 [0xffe3dac9]</b>
	 * <p>
	 * Hex (RGB): <b>#e3dac9</b>
	 */
	@ColorInt public static final int BONE = Color.rgb(227, 218, 201);

	/**
	 * Constant value: <b>-3407872 [0xffcc0000]</b>
	 * <p>
	 * Hex (RGB): <b>#cc0000</b>
	 */
	@ColorInt public static final int BOSTON_UNIVERSITY_RED = Color.rgb(204, 0, 0);

	/**
	 * Constant value: <b>-16750002 [0xff6a4e]</b>
	 * <p>
	 * Hex (RGB): <b>#006a4e</b>
	 */
	@ColorInt public static final int BOTTLE_GREEN = Color.rgb(0, 106, 78);

	/**
	 * Constant value: <b>-7916960 [0xff873260]</b>
	 * <p>
	 * Hex (RGB): <b>#873260</b>
	 */
	@ColorInt public static final int BOYSENBERRY = Color.rgb(135, 50, 96);

	/**
	 * Constant value: <b>-16748289 [0xff70ff]</b>
	 * <p>
	 * Hex (RGB): <b>#0070ff</b>
	 */
	@ColorInt public static final int BRANDEIS_BLUE = Color.rgb(0, 112, 255);

	/**
	 * Constant value: <b>-4872638 [0xffb5a642]</b>
	 * <p>
	 * Hex (RGB): <b>#b5a642</b>
	 */
	@ColorInt public static final int BRASS = Color.rgb(181, 166, 66);

	/**
	 * Constant value: <b>-3456684 [0xffcb4154]</b>
	 * <p>
	 * Hex (RGB): <b>#cb4154</b>
	 */
	@ColorInt public static final int BRICK_RED = Color.rgb(203, 65, 84);

	/**
	 * Constant value: <b>-14832426 [0xff1dacd6]</b>
	 * <p>
	 * Hex (RGB): <b>#1dacd6</b>
	 */
	@ColorInt public static final int BRIGHT_CERULEAN = Color.rgb(29, 172, 214);

	/**
	 * Constant value: <b>-10027264 [0xff66ff00]</b>
	 * <p>
	 * Hex (RGB): <b>#66ff00</b>
	 */
	@ColorInt public static final int BRIGHT_GREEN = Color.rgb(102, 255, 0);

	/**
	 * Constant value: <b>-4221724 [0xffbf94e4]</b>
	 * <p>
	 * Hex (RGB): <b>#bf94e4</b>
	 */
	@ColorInt public static final int BRIGHT_LAVENDER = Color.rgb(191, 148, 228);

	/**
	 * Constant value: <b>-3989176 [0xffc32148]</b>
	 * <p>
	 * Hex (RGB): <b>#c32148</b>
	 */
	@ColorInt public static final int BRIGHT_MAROON = Color.rgb(195, 33, 72);

	/**
	 * Constant value: <b>-15108910 [0xff1974d2]</b>
	 * <p>
	 * Hex (RGB): <b>#1974d2</b>
	 */
	@ColorInt public static final int BRIGHT_NAVY_BLUE = Color.rgb(25, 116, 210);

	/**
	 * Constant value: <b>-65409 [0xffff007f]</b>
	 * <p>
	 * Hex (RGB): <b>#ff007f</b>
	 */
	@ColorInt public static final int BRIGHT_PINK = Color.rgb(255, 0, 127);

	/**
	 * Constant value: <b>-16193314 [0xff8e8de]</b>
	 * <p>
	 * Hex (RGB): <b>#08e8de</b>
	 */
	@ColorInt public static final int BRIGHT_TURQUOISE = Color.rgb(8, 232, 222);

	/**
	 * Constant value: <b>-3039256 [0xffd19fe8]</b>
	 * <p>
	 * Hex (RGB): <b>#d19fe8</b>
	 */
	@ColorInt public static final int BRIGHT_UBE = Color.rgb(209, 159, 232);

	/**
	 * Constant value: <b>-738305 [0xfff4bbff]</b>
	 * <p>
	 * Hex (RGB): <b>#f4bbff</b>
	 */
	@ColorInt public static final int BRILLIANT_LAVENDER = Color.rgb(244, 187, 255);

	/**
	 * Constant value: <b>-43613 [0xffff55a3]</b>
	 * <p>
	 * Hex (RGB): <b>#ff55a3</b>
	 */
	@ColorInt public static final int BRILLIANT_ROSE = Color.rgb(255, 85, 163);

	/**
	 * Constant value: <b>-302977 [0xfffb607f]</b>
	 * <p>
	 * Hex (RGB): <b>#fb607f</b>
	 */
	@ColorInt public static final int BRINK_PINK = Color.rgb(251, 96, 127);

	/**
	 * Constant value: <b>-16760283 [0xff4225]</b>
	 * <p>
	 * Hex (RGB): <b>#004225</b>
	 */
	@ColorInt public static final int BRITISH_RACING_GREEN = Color.rgb(0, 66, 37);

	/**
	 * Constant value: <b>-3309774 [0xffcd7f32]</b>
	 * <p>
	 * Hex (RGB): <b>#cd7f32</b>
	 */
	@ColorInt public static final int BRONZE = Color.rgb(205, 127, 50);

	/**
	 * Constant value: <b>-9211904 [0xff737000]</b>
	 * <p>
	 * Hex (RGB): <b>#737000</b>
	 */
	@ColorInt public static final int BRONZE_YELLOW = Color.rgb(115, 112, 0);

	/**
	 * Constant value: <b>-6927616 [0xff964b00]</b>
	 * <p>
	 * Hex (RGB): <b>#964b00</b>
	 */
	@ColorInt public static final int BROWN_TRADITIONAL = Color.rgb(150, 75, 0);

	/**
	 * Constant value: <b>-5952982 [0xffa52a2a]</b>
	 * <p>
	 * Hex (RGB): <b>#a52a2a</b>
	 */
	@ColorInt public static final int BROWN_WEB = Color.rgb(165, 42, 42);

	/**
	 * Constant value: <b>-10075101 [0xff664423]</b>
	 * <p>
	 * Hex (RGB): <b>#664423</b>
	 */
	@ColorInt public static final int BROWN_NOSE = Color.rgb(102, 68, 35);

	/**
	 * Constant value: <b>-14987970 [0xff1b4d3e]</b>
	 * <p>
	 * Hex (RGB): <b>#1b4d3e</b>
	 */
	@ColorInt public static final int BRUNSWICK_GREEN = Color.rgb(27, 77, 62);

	/**
	 * Constant value: <b>-15924 [0xffffc1cc]</b>
	 * <p>
	 * Hex (RGB): <b>#ffc1cc</b>
	 */
	@ColorInt public static final int BUBBLE_GUM = Color.rgb(255, 193, 204);

	/**
	 * Constant value: <b>-1573121 [0xffe7feff]</b>
	 * <p>
	 * Hex (RGB): <b>#e7feff</b>
	 */
	@ColorInt public static final int BUBBLES = Color.rgb(231, 254, 255);

	/**
	 * Constant value: <b>-992126 [0xfff0dc82]</b>
	 * <p>
	 * Hex (RGB): <b>#f0dc82</b>
	 */
	@ColorInt public static final int BUFF = Color.rgb(240, 220, 130);

	/**
	 * Constant value: <b>-8669599 [0xff7bb661]</b>
	 * <p>
	 * Hex (RGB): <b>#7bb661</b>
	 */
	@ColorInt public static final int BUD_GREEN = Color.rgb(123, 182, 97);

	/**
	 * Constant value: <b>-12057081 [0xff480607]</b>
	 * <p>
	 * Hex (RGB): <b>#480607</b>
	 */
	@ColorInt public static final int BULGARIAN_ROSE = Color.rgb(72, 6, 7);

	/**
	 * Constant value: <b>-8388576 [0xff800020]</b>
	 * <p>
	 * Hex (RGB): <b>#800020</b>
	 */
	@ColorInt public static final int BURGUNDY = Color.rgb(128, 0, 32);

	/**
	 * Constant value: <b>-2180985 [0xffdeb887]</b>
	 * <p>
	 * Hex (RGB): <b>#deb887</b>
	 */
	@ColorInt public static final int BURLYWOOD = Color.rgb(222, 184, 135);

	/**
	 * Constant value: <b>-3386112 [0xffcc5500]</b>
	 * <p>
	 * Hex (RGB): <b>#cc5500</b>
	 */
	@ColorInt public static final int BURNT_ORANGE = Color.rgb(204, 85, 0);

	/**
	 * Constant value: <b>-1477551 [0xffe97451]</b>
	 * <p>
	 * Hex (RGB): <b>#e97451</b>
	 */
	@ColorInt public static final int BURNT_SIENNA = Color.rgb(233, 116, 81);

	/**
	 * Constant value: <b>-7720156 [0xff8a3324]</b>
	 * <p>
	 * Hex (RGB): <b>#8a3324</b>
	 */
	@ColorInt public static final int BURNT_UMBER = Color.rgb(138, 51, 36);

	/**
	 * Constant value: <b>-4377692 [0xffbd33a4]</b>
	 * <p>
	 * Hex (RGB): <b>#bd33a4</b>
	 */
	@ColorInt public static final int BYZANTINE = Color.rgb(189, 51, 164);

	/**
	 * Constant value: <b>-9426589 [0xff702963]</b>
	 * <p>
	 * Hex (RGB): <b>#702963</b>
	 */
	@ColorInt public static final int BYZANTIUM = Color.rgb(112, 41, 99);

	/**
	 * Constant value: <b>-11310990 [0xff536872]</b>
	 * <p>
	 * Hex (RGB): <b>#536872</b>
	 */
	@ColorInt public static final int CADET = Color.rgb(83, 104, 114);

	/**
	 * Constant value: <b>-10510688 [0xff5f9ea0]</b>
	 * <p>
	 * Hex (RGB): <b>#5f9ea0</b>
	 */
	@ColorInt public static final int CADET_BLUE = Color.rgb(95, 158, 160);

	/**
	 * Constant value: <b>-7232592 [0xff91a3b0]</b>
	 * <p>
	 * Hex (RGB): <b>#91a3b0</b>
	 */
	@ColorInt public static final int CADET_GREY = Color.rgb(145, 163, 176);

	/**
	 * Constant value: <b>-16749764 [0xff6b3c]</b>
	 * <p>
	 * Hex (RGB): <b>#006b3c</b>
	 */
	@ColorInt public static final int CADMIUM_GREEN = Color.rgb(0, 107, 60);

	/**
	 * Constant value: <b>-1210579 [0xffed872d]</b>
	 * <p>
	 * Hex (RGB): <b>#ed872d</b>
	 */
	@ColorInt public static final int CADMIUM_ORANGE = Color.rgb(237, 135, 45);

	/**
	 * Constant value: <b>-1900510 [0xffe30022]</b>
	 * <p>
	 * Hex (RGB): <b>#e30022</b>
	 */
	@ColorInt public static final int CADMIUM_RED = Color.rgb(227, 0, 34);

	/**
	 * Constant value: <b>-2560 [0xfffff600]</b>
	 * <p>
	 * Hex (RGB): <b>#fff600</b>
	 */
	@ColorInt public static final int CADMIUM_YELLOW = Color.rgb(255, 246, 0);

	/**
	 * Constant value: <b>-5866661 [0xffa67b5b]</b>
	 * <p>
	 * Hex (RGB): <b>#a67b5b</b>
	 */
	@ColorInt public static final int CAFE_AU_LAIT = Color.rgb(166, 123, 91);

	/**
	 * Constant value: <b>-11848159 [0xff4b3621]</b>
	 * <p>
	 * Hex (RGB): <b>#4b3621</b>
	 */
	@ColorInt public static final int CAFE_NOIR = Color.rgb(75, 54, 33);

	/**
	 * Constant value: <b>-14791381 [0xff1e4d2b]</b>
	 * <p>
	 * Hex (RGB): <b>#1e4d2b</b>
	 */
	@ColorInt public static final int CAL_POLY_GREEN = Color.rgb(30, 77, 43);

	/**
	 * Constant value: <b>-6045267 [0xffa3c1ad]</b>
	 * <p>
	 * Hex (RGB): <b>#a3c1ad</b>
	 */
	@ColorInt public static final int CAMBRIDGE_BLUE = Color.rgb(163, 193, 173);

	/**
	 * Constant value: <b>-4089237 [0xffc19a6b]</b>
	 * <p>
	 * Hex (RGB): <b>#c19a6b</b>
	 */
	@ColorInt public static final int CAMEL = Color.rgb(193, 154, 107);

	/**
	 * Constant value: <b>-1066036 [0xffefbbcc]</b>
	 * <p>
	 * Hex (RGB): <b>#efbbcc</b>
	 */
	@ColorInt public static final int CAMEO_PINK = Color.rgb(239, 187, 204);

	/**
	 * Constant value: <b>-8878485 [0xff78866b]</b>
	 * <p>
	 * Hex (RGB): <b>#78866b</b>
	 */
	@ColorInt public static final int CAMOUFLAGE_GREEN = Color.rgb(120, 134, 107);

	/**
	 * Constant value: <b>-4352 [0xffffef00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffef00</b>
	 */
	@ColorInt public static final int CANARY_YELLOW = Color.rgb(255, 239, 0);

	/**
	 * Constant value: <b>-63488 [0xffff0800]</b>
	 * <p>
	 * Hex (RGB): <b>#ff0800</b>
	 */
	@ColorInt public static final int CANDY_APPLE_RED = Color.rgb(255, 8, 0);

	/**
	 * Constant value: <b>-1805958 [0xffe4717a]</b>
	 * <p>
	 * Hex (RGB): <b>#e4717a</b>
	 */
	@ColorInt public static final int CANDY_PINK = Color.rgb(228, 113, 122);

	/**
	 * Constant value: <b>-16728065 [0xffbfff]</b>
	 * <p>
	 * Hex (RGB): <b>#00bfff</b>
	 */
	@ColorInt public static final int CAPRI = Color.rgb(0, 191, 255);

	/**
	 * Constant value: <b>-10934496 [0xff592720]</b>
	 * <p>
	 * Hex (RGB): <b>#592720</b>
	 */
	@ColorInt public static final int CAPUT_MORTUUM = Color.rgb(89, 39, 32);

	/**
	 * Constant value: <b>-3924422 [0xffc41e3a]</b>
	 * <p>
	 * Hex (RGB): <b>#c41e3a</b>
	 */
	@ColorInt public static final int CARDINAL = Color.rgb(196, 30, 58);

	/**
	 * Constant value: <b>-16724839 [0xffcc99]</b>
	 * <p>
	 * Hex (RGB): <b>#00cc99</b>
	 */
	@ColorInt public static final int CARIBBEAN_GREEN = Color.rgb(0, 204, 153);

	/**
	 * Constant value: <b>-6946792 [0xff960018]</b>
	 * <p>
	 * Hex (RGB): <b>#960018</b>
	 */
	@ColorInt public static final int CARMINE = Color.rgb(150, 0, 24);

	/**
	 * Constant value: <b>-2686912 [0xffd70040]</b>
	 * <p>
	 * Hex (RGB): <b>#d70040</b>
	 */
	@ColorInt public static final int CARMINE_M_AND_P = Color.rgb(215, 0, 64);

	/**
	 * Constant value: <b>-1356734 [0xffeb4c42]</b>
	 * <p>
	 * Hex (RGB): <b>#eb4c42</b>
	 */
	@ColorInt public static final int CARMINE_PINK = Color.rgb(235, 76, 66);

	/**
	 * Constant value: <b>-65480 [0xffff0038]</b>
	 * <p>
	 * Hex (RGB): <b>#ff0038</b>
	 */
	@ColorInt public static final int CARMINE_RED = Color.rgb(255, 0, 56);

	/**
	 * Constant value: <b>-22839 [0xffffa6c9]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa6c9</b>
	 */
	@ColorInt public static final int CARNATION_PINK = Color.rgb(255, 166, 201);

	/**
	 * Constant value: <b>-5039333 [0xffb31b1b]</b>
	 * <p>
	 * Hex (RGB): <b>#b31b1b</b>
	 */
	@ColorInt public static final int CARNELIAN = Color.rgb(179, 27, 27);

	/**
	 * Constant value: <b>-6702371 [0xff99badd]</b>
	 * <p>
	 * Hex (RGB): <b>#99badd</b>
	 */
	@ColorInt public static final int CAROLINA_BLUE = Color.rgb(153, 186, 221);

	/**
	 * Constant value: <b>-1208031 [0xffed9121]</b>
	 * <p>
	 * Hex (RGB): <b>#ed9121</b>
	 */
	@ColorInt public static final int CARROT_ORANGE = Color.rgb(237, 145, 33);

	/**
	 * Constant value: <b>-16755137 [0xff563f]</b>
	 * <p>
	 * Hex (RGB): <b>#00563f</b>
	 */
	@ColorInt public static final int CASTLETON_GREEN = Color.rgb(0, 86, 63);

	/**
	 * Constant value: <b>-16373128 [0xff62a78]</b>
	 * <p>
	 * Hex (RGB): <b>#062a78</b>
	 */
	@ColorInt public static final int CATALINA_BLUE = Color.rgb(6, 42, 120);

	/**
	 * Constant value: <b>-9423294 [0xff703642]</b>
	 * <p>
	 * Hex (RGB): <b>#703642</b>
	 */
	@ColorInt public static final int CATAWBA = Color.rgb(112, 54, 66);

	/**
	 * Constant value: <b>-3581367 [0xffc95a49]</b>
	 * <p>
	 * Hex (RGB): <b>#c95a49</b>
	 */
	@ColorInt public static final int CEDAR_CHEST = Color.rgb(201, 90, 73);

	/**
	 * Constant value: <b>-7167537 [0xff92a1cf]</b>
	 * <p>
	 * Hex (RGB): <b>#92a1cf</b>
	 */
	@ColorInt public static final int CEIL = Color.rgb(146, 161, 207);

	/**
	 * Constant value: <b>-5447249 [0xfface1af]</b>
	 * <p>
	 * Hex (RGB): <b>#ace1af</b>
	 */
	@ColorInt public static final int CELADON = Color.rgb(172, 225, 175);

	/**
	 * Constant value: <b>-16745561 [0xff7ba7]</b>
	 * <p>
	 * Hex (RGB): <b>#007ba7</b>
	 */
	@ColorInt public static final int CELADON_BLUE = Color.rgb(0, 123, 167);

	/**
	 * Constant value: <b>-13663108 [0xff2f847c]</b>
	 * <p>
	 * Hex (RGB): <b>#2f847c</b>
	 */
	@ColorInt public static final int CELADON_GREEN = Color.rgb(47, 132, 124);

	/**
	 * Constant value: <b>-5046273 [0xffb2ffff]</b>
	 * <p>
	 * Hex (RGB): <b>#b2ffff</b>
	 */
	@ColorInt public static final int CELESTE_COLOUR = Color.rgb(178, 255, 255);

	/**
	 * Constant value: <b>-11954224 [0xff4997d0]</b>
	 * <p>
	 * Hex (RGB): <b>#4997d0</b>
	 */
	@ColorInt public static final int CELESTIAL_BLUE = Color.rgb(73, 151, 208);

	/**
	 * Constant value: <b>-2215581 [0xffde3163]</b>
	 * <p>
	 * Hex (RGB): <b>#de3163</b>
	 */
	@ColorInt public static final int CERISE = Color.rgb(222, 49, 99);

	/**
	 * Constant value: <b>-1295485 [0xffec3b83]</b>
	 * <p>
	 * Hex (RGB): <b>#ec3b83</b>
	 */
	@ColorInt public static final int CERISE_PINK = Color.rgb(236, 59, 131);

	/**
	 * Constant value: <b>-16745561 [0xff7ba7]</b>
	 * <p>
	 * Hex (RGB): <b>#007ba7</b>
	 */
	@ColorInt public static final int CERULEAN = Color.rgb(0, 123, 167);

	/**
	 * Constant value: <b>-14003522 [0xff2a52be]</b>
	 * <p>
	 * Hex (RGB): <b>#2a52be</b>
	 */
	@ColorInt public static final int CERULEAN_BLUE = Color.rgb(42, 82, 190);

	/**
	 * Constant value: <b>-9593917 [0xff6d9bc3]</b>
	 * <p>
	 * Hex (RGB): <b>#6d9bc3</b>
	 */
	@ColorInt public static final int CERULEAN_FROST = Color.rgb(109, 155, 195);

	/**
	 * Constant value: <b>-16745819 [0xff7aa5]</b>
	 * <p>
	 * Hex (RGB): <b>#007aa5</b>
	 */
	@ColorInt public static final int CG_BLUE = Color.rgb(0, 122, 165);

	/**
	 * Constant value: <b>-2081743 [0xffe03c31]</b>
	 * <p>
	 * Hex (RGB): <b>#e03c31</b>
	 */
	@ColorInt public static final int CG_RED = Color.rgb(224, 60, 49);

	/**
	 * Constant value: <b>-6260646 [0xffa0785a]</b>
	 * <p>
	 * Hex (RGB): <b>#a0785a</b>
	 */
	@ColorInt public static final int CHAMOISEE = Color.rgb(160, 120, 90);

	/**
	 * Constant value: <b>-530482 [0xfff7e7ce]</b>
	 * <p>
	 * Hex (RGB): <b>#f7e7ce</b>
	 */
	@ColorInt public static final int CHAMPAGNE = Color.rgb(247, 231, 206);

	/**
	 * Constant value: <b>-13220529 [0xff36454f]</b>
	 * <p>
	 * Hex (RGB): <b>#36454f</b>
	 */
	@ColorInt public static final int CHARCOAL = Color.rgb(54, 69, 79);

	/**
	 * Constant value: <b>-14472405 [0xff232b2b]</b>
	 * <p>
	 * Hex (RGB): <b>#232b2b</b>
	 */
	@ColorInt public static final int CHARLESTON_GREEN = Color.rgb(35, 43, 43);

	/**
	 * Constant value: <b>-1667156 [0xffe68fac]</b>
	 * <p>
	 * Hex (RGB): <b>#e68fac</b>
	 */
	@ColorInt public static final int CHARM_PINK = Color.rgb(230, 143, 172);

	/**
	 * Constant value: <b>-2097408 [0xffdfff00]</b>
	 * <p>
	 * Hex (RGB): <b>#dfff00</b>
	 */
	@ColorInt public static final int CHARTREUSE_TRADITIONAL = Color.rgb(223, 255, 0);

	/**
	 * Constant value: <b>-8388864 [0xff7fff00]</b>
	 * <p>
	 * Hex (RGB): <b>#7fff00</b>
	 */
	@ColorInt public static final int CHARTREUSE_WEB = Color.rgb(127, 255, 0);

	/**
	 * Constant value: <b>-2215581 [0xffde3163]</b>
	 * <p>
	 * Hex (RGB): <b>#de3163</b>
	 */
	@ColorInt public static final int CHERRY = Color.rgb(222, 49, 99);

	/**
	 * Constant value: <b>-18491 [0xffffb7c5]</b>
	 * <p>
	 * Hex (RGB): <b>#ffb7c5</b>
	 */
	@ColorInt public static final int CHERRY_BLOSSOM_PINK = Color.rgb(255, 183, 197);

	/**
	 * Constant value: <b>-7322315 [0xff904535]</b>
	 * <p>
	 * Hex (RGB): <b>#904535</b>
	 */
	@ColorInt public static final int CHESTNUT = Color.rgb(144, 69, 53);

	/**
	 * Constant value: <b>-2199647 [0xffde6fa1]</b>
	 * <p>
	 * Hex (RGB): <b>#de6fa1</b>
	 */
	@ColorInt public static final int CHINA_PINK = Color.rgb(222, 111, 161);

	/**
	 * Constant value: <b>-5746322 [0xffa8516e]</b>
	 * <p>
	 * Hex (RGB): <b>#a8516e</b>
	 */
	@ColorInt public static final int CHINA_ROSE = Color.rgb(168, 81, 110);

	/**
	 * Constant value: <b>-5621730 [0xffaa381e]</b>
	 * <p>
	 * Hex (RGB): <b>#aa381e</b>
	 */
	@ColorInt public static final int CHINESE_RED = Color.rgb(170, 56, 30);

	/**
	 * Constant value: <b>-8036216 [0xff856088]</b>
	 * <p>
	 * Hex (RGB): <b>#856088</b>
	 */
	@ColorInt public static final int CHINESE_VIOLET = Color.rgb(133, 96, 136);

	/**
	 * Constant value: <b>-8700160 [0xff7b3f00]</b>
	 * <p>
	 * Hex (RGB): <b>#7b3f00</b>
	 */
	@ColorInt public static final int CHOCOLATE_TRADITIONAL = Color.rgb(123, 63, 0);

	/**
	 * Constant value: <b>-2987746 [0xffd2691e]</b>
	 * <p>
	 * Hex (RGB): <b>#d2691e</b>
	 */
	@ColorInt public static final int CHOCOLATE_WEB = Color.rgb(210, 105, 30);

	/**
	 * Constant value: <b>-22784 [0xffffa700]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa700</b>
	 */
	@ColorInt public static final int CHROME_YELLOW = Color.rgb(255, 167, 0);

	/**
	 * Constant value: <b>-6782597 [0xff98817b]</b>
	 * <p>
	 * Hex (RGB): <b>#98817b</b>
	 */
	@ColorInt public static final int CINEREOUS = Color.rgb(152, 129, 123);

	/**
	 * Constant value: <b>-1883596 [0xffe34234]</b>
	 * <p>
	 * Hex (RGB): <b>#e34234</b>
	 */
	@ColorInt public static final int CINNABAR = Color.rgb(227, 66, 52);

	/**
	 * Constant value: <b>-2987746 [0xffd2691e]</b>
	 * <p>
	 * Hex (RGB): <b>#d2691e</b>
	 */
	@ColorInt public static final int CINNAMON = Color.rgb(210, 105, 30);

	/**
	 * Constant value: <b>-1781750 [0xffe4d00a]</b>
	 * <p>
	 * Hex (RGB): <b>#e4d00a</b>
	 */
	@ColorInt public static final int CITRINE = Color.rgb(228, 208, 10);

	/**
	 * Constant value: <b>-6379233 [0xff9ea91f]</b>
	 * <p>
	 * Hex (RGB): <b>#9ea91f</b>
	 */
	@ColorInt public static final int CITRON = Color.rgb(158, 169, 31);

	/**
	 * Constant value: <b>-8448204 [0xff7f1734]</b>
	 * <p>
	 * Hex (RGB): <b>#7f1734</b>
	 */
	@ColorInt public static final int CLARET = Color.rgb(127, 23, 52);

	/**
	 * Constant value: <b>-275225 [0xfffbcce7]</b>
	 * <p>
	 * Hex (RGB): <b>#fbcce7</b>
	 */
	@ColorInt public static final int CLASSIC_ROSE = Color.rgb(251, 204, 231);

	/**
	 * Constant value: <b>-16758869 [0xff47ab]</b>
	 * <p>
	 * Hex (RGB): <b>#0047ab</b>
	 */
	@ColorInt public static final int COBALT = Color.rgb(0, 71, 171);

	/**
	 * Constant value: <b>-2987746 [0xffd2691e]</b>
	 * <p>
	 * Hex (RGB): <b>#d2691e</b>
	 */
	@ColorInt public static final int COCOA_BROWN = Color.rgb(210, 105, 30);

	/**
	 * Constant value: <b>-6923714 [0xff965a3e]</b>
	 * <p>
	 * Hex (RGB): <b>#965a3e</b>
	 */
	@ColorInt public static final int COCONUT = Color.rgb(150, 90, 62);

	/**
	 * Constant value: <b>-9482697 [0xff6f4e37]</b>
	 * <p>
	 * Hex (RGB): <b>#6f4e37</b>
	 */
	@ColorInt public static final int COFFEE = Color.rgb(111, 78, 55);

	/**
	 * Constant value: <b>-6562305 [0xff9bddff]</b>
	 * <p>
	 * Hex (RGB): <b>#9bddff</b>
	 */
	@ColorInt public static final int COLUMBIA_BLUE = Color.rgb(155, 221, 255);

	/**
	 * Constant value: <b>-490631 [0xfff88379]</b>
	 * <p>
	 * Hex (RGB): <b>#f88379</b>
	 */
	@ColorInt public static final int CONGO_PINK = Color.rgb(248, 131, 121);

	/**
	 * Constant value: <b>-16765341 [0xff2e63]</b>
	 * <p>
	 * Hex (RGB): <b>#002e63</b>
	 */
	@ColorInt public static final int COOL_BLACK = Color.rgb(0, 46, 99);

	/**
	 * Constant value: <b>-7564628 [0xff8c92ac]</b>
	 * <p>
	 * Hex (RGB): <b>#8c92ac</b>
	 */
	@ColorInt public static final int COOL_GREY = Color.rgb(140, 146, 172);

	/**
	 * Constant value: <b>-4689101 [0xffb87333]</b>
	 * <p>
	 * Hex (RGB): <b>#b87333</b>
	 */
	@ColorInt public static final int COPPER = Color.rgb(184, 115, 51);

	/**
	 * Constant value: <b>-2454937 [0xffda8a67]</b>
	 * <p>
	 * Hex (RGB): <b>#da8a67</b>
	 */
	@ColorInt public static final int COPPER_CRAYOLA = Color.rgb(218, 138, 103);

	/**
	 * Constant value: <b>-5410967 [0xffad6f69]</b>
	 * <p>
	 * Hex (RGB): <b>#ad6f69</b>
	 */
	@ColorInt public static final int COPPER_PENNY = Color.rgb(173, 111, 105);

	/**
	 * Constant value: <b>-3445423 [0xffcb6d51]</b>
	 * <p>
	 * Hex (RGB): <b>#cb6d51</b>
	 */
	@ColorInt public static final int COPPER_RED = Color.rgb(203, 109, 81);

	/**
	 * Constant value: <b>-6723994 [0xff996666]</b>
	 * <p>
	 * Hex (RGB): <b>#996666</b>
	 */
	@ColorInt public static final int COPPER_ROSE = Color.rgb(153, 102, 102);

	/**
	 * Constant value: <b>-51200 [0xffff3800]</b>
	 * <p>
	 * Hex (RGB): <b>#ff3800</b>
	 */
	@ColorInt public static final int COQUELICOT = Color.rgb(255, 56, 0);

	/**
	 * Constant value: <b>-32944 [0xffff7f50]</b>
	 * <p>
	 * Hex (RGB): <b>#ff7f50</b>
	 */
	@ColorInt public static final int CORAL = Color.rgb(255, 127, 80);

	/**
	 * Constant value: <b>-490631 [0xfff88379]</b>
	 * <p>
	 * Hex (RGB): <b>#f88379</b>
	 */
	@ColorInt public static final int CORAL_PINK = Color.rgb(248, 131, 121);

	/**
	 * Constant value: <b>-49088 [0xffff4040]</b>
	 * <p>
	 * Hex (RGB): <b>#ff4040</b>
	 */
	@ColorInt public static final int CORAL_RED = Color.rgb(255, 64, 64);

	/**
	 * Constant value: <b>-7782587 [0xff893f45]</b>
	 * <p>
	 * Hex (RGB): <b>#893f45</b>
	 */
	@ColorInt public static final int CORDOVAN = Color.rgb(137, 63, 69);

	/**
	 * Constant value: <b>-267171 [0xfffbec5d]</b>
	 * <p>
	 * Hex (RGB): <b>#fbec5d</b>
	 */
	@ColorInt public static final int CORN = Color.rgb(251, 236, 93);

	/**
	 * Constant value: <b>-5039333 [0xffb31b1b]</b>
	 * <p>
	 * Hex (RGB): <b>#b31b1b</b>
	 */
	@ColorInt public static final int CORNELL_RED = Color.rgb(179, 27, 27);

	/**
	 * Constant value: <b>-10185235 [0xff6495ed]</b>
	 * <p>
	 * Hex (RGB): <b>#6495ed</b>
	 */
	@ColorInt public static final int CORNFLOWER_BLUE = Color.rgb(100, 149, 237);

	/**
	 * Constant value: <b>-1828 [0xfffff8dc]</b>
	 * <p>
	 * Hex (RGB): <b>#fff8dc</b>
	 */
	@ColorInt public static final int CORNSILK = Color.rgb(255, 248, 220);

	/**
	 * Constant value: <b>-1817 [0xfffff8e7]</b>
	 * <p>
	 * Hex (RGB): <b>#fff8e7</b>
	 */
	@ColorInt public static final int COSMIC_LATTE = Color.rgb(255, 248, 231);

	/**
	 * Constant value: <b>-17191 [0xffffbcd9]</b>
	 * <p>
	 * Hex (RGB): <b>#ffbcd9</b>
	 */
	@ColorInt public static final int COTTON_CANDY = Color.rgb(255, 188, 217);

	/**
	 * Constant value: <b>-560 [0xfffffdd0]</b>
	 * <p>
	 * Hex (RGB): <b>#fffdd0</b>
	 */
	@ColorInt public static final int CREAM = Color.rgb(255, 253, 208);

	/**
	 * Constant value: <b>-2354116 [0xffdc143c]</b>
	 * <p>
	 * Hex (RGB): <b>#dc143c</b>
	 */
	@ColorInt public static final int CRIMSON = Color.rgb(220, 20, 60);

	/**
	 * Constant value: <b>-4325326 [0xffbe0032]</b>
	 * <p>
	 * Hex (RGB): <b>#be0032</b>
	 */
	@ColorInt public static final int CRIMSON_GLORY = Color.rgb(190, 0, 50);

	/**
	 * Constant value: <b>-16711681 [0xffffff]</b>
	 * <p>
	 * Hex (RGB): <b>#00ffff</b>
	 */
	@ColorInt public static final int CYAN = Color.rgb(0, 255, 255);

	/**
	 * Constant value: <b>-16730133 [0xffb7eb]</b>
	 * <p>
	 * Hex (RGB): <b>#00b7eb</b>
	 */
	@ColorInt public static final int CYAN_PROCESS = Color.rgb(0, 183, 235);

	/**
	 * Constant value: <b>-10993028 [0xff58427c]</b>
	 * <p>
	 * Hex (RGB): <b>#58427c</b>
	 */
	@ColorInt public static final int CYBER_GRAPE = Color.rgb(88, 66, 124);

	/**
	 * Constant value: <b>-11520 [0xffffd300]</b>
	 * <p>
	 * Hex (RGB): <b>#ffd300</b>
	 */
	@ColorInt public static final int CYBER_YELLOW = Color.rgb(255, 211, 0);

	/**
	 * Constant value: <b>-207 [0xffffff31]</b>
	 * <p>
	 * Hex (RGB): <b>#ffff31</b>
	 */
	@ColorInt public static final int DAFFODIL = Color.rgb(255, 255, 49);

	/**
	 * Constant value: <b>-990928 [0xfff0e130]</b>
	 * <p>
	 * Hex (RGB): <b>#f0e130</b>
	 */
	@ColorInt public static final int DANDELION = Color.rgb(240, 225, 48);

	/**
	 * Constant value: <b>-16777077 [0xff8b]</b>
	 * <p>
	 * Hex (RGB): <b>#00008b</b>
	 */
	@ColorInt public static final int DARK_BLUE = Color.rgb(0, 0, 139);

	/**
	 * Constant value: <b>-10066279 [0xff666699]</b>
	 * <p>
	 * Hex (RGB): <b>#666699</b>
	 */
	@ColorInt public static final int DARK_BLUE_GRAY = Color.rgb(102, 102, 153);

	/**
	 * Constant value: <b>-10140895 [0xff654321]</b>
	 * <p>
	 * Hex (RGB): <b>#654321</b>
	 */
	@ColorInt public static final int DARK_BROWN = Color.rgb(101, 67, 33);

	/**
	 * Constant value: <b>-10667692 [0xff5d3954]</b>
	 * <p>
	 * Hex (RGB): <b>#5d3954</b>
	 */
	@ColorInt public static final int DARK_BYZANTIUM = Color.rgb(93, 57, 84);

	/**
	 * Constant value: <b>-6029312 [0xffa40000]</b>
	 * <p>
	 * Hex (RGB): <b>#a40000</b>
	 */
	@ColorInt public static final int DARK_CANDY_APPLE_RED = Color.rgb(164, 0, 0);

	/**
	 * Constant value: <b>-16235138 [0xff8457e]</b>
	 * <p>
	 * Hex (RGB): <b>#08457e</b>
	 */
	@ColorInt public static final int DARK_CERULEAN = Color.rgb(8, 69, 126);

	/**
	 * Constant value: <b>-6788768 [0xff986960]</b>
	 * <p>
	 * Hex (RGB): <b>#986960</b>
	 */
	@ColorInt public static final int DARK_CHESTNUT = Color.rgb(152, 105, 96);

	/**
	 * Constant value: <b>-3318971 [0xffcd5b45]</b>
	 * <p>
	 * Hex (RGB): <b>#cd5b45</b>
	 */
	@ColorInt public static final int DARK_CORAL = Color.rgb(205, 91, 69);

	/**
	 * Constant value: <b>-16741493 [0xff8b8b]</b>
	 * <p>
	 * Hex (RGB): <b>#008b8b</b>
	 */
	@ColorInt public static final int DARK_CYAN = Color.rgb(0, 139, 139);

	/**
	 * Constant value: <b>-11310984 [0xff536878]</b>
	 * <p>
	 * Hex (RGB): <b>#536878</b>
	 */
	@ColorInt public static final int DARK_ELECTRIC_BLUE = Color.rgb(83, 104, 120);

	/**
	 * Constant value: <b>-4684277 [0xffb8860b]</b>
	 * <p>
	 * Hex (RGB): <b>#b8860b</b>
	 */
	@ColorInt public static final int DARK_GOLDENROD = Color.rgb(184, 134, 11);

	/**
	 * Constant value: <b>-5658199 [0xffa9a9a9]</b>
	 * <p>
	 * Hex (RGB): <b>#a9a9a9</b>
	 */
	@ColorInt public static final int DARK_GRAY = Color.rgb(169, 169, 169);

	/**
	 * Constant value: <b>-16698848 [0xff13220]</b>
	 * <p>
	 * Hex (RGB): <b>#013220</b>
	 */
	@ColorInt public static final int DARK_GREEN = Color.rgb(1, 50, 32);

	/**
	 * Constant value: <b>-16760470 [0xff416a]</b>
	 * <p>
	 * Hex (RGB): <b>#00416a</b>
	 */
	@ColorInt public static final int DARK_IMPERIAL_BLUE = Color.rgb(0, 65, 106);

	/**
	 * Constant value: <b>-15064031 [0xff1a2421]</b>
	 * <p>
	 * Hex (RGB): <b>#1a2421</b>
	 */
	@ColorInt public static final int DARK_JUNGLE_GREEN = Color.rgb(26, 36, 33);

	/**
	 * Constant value: <b>-4343957 [0xffbdb76b]</b>
	 * <p>
	 * Hex (RGB): <b>#bdb76b</b>
	 */
	@ColorInt public static final int DARK_KHAKI = Color.rgb(189, 183, 107);

	/**
	 * Constant value: <b>-12043214 [0xff483c32]</b>
	 * <p>
	 * Hex (RGB): <b>#483c32</b>
	 */
	@ColorInt public static final int DARK_LAVA = Color.rgb(72, 60, 50);

	/**
	 * Constant value: <b>-9220202 [0xff734f96]</b>
	 * <p>
	 * Hex (RGB): <b>#734f96</b>
	 */
	@ColorInt public static final int DARK_LAVENDER = Color.rgb(115, 79, 150);

	/**
	 * Constant value: <b>-11318449 [0xff534b4f]</b>
	 * <p>
	 * Hex (RGB): <b>#534b4f</b>
	 */
	@ColorInt public static final int DARK_LIVER = Color.rgb(83, 75, 79);

	/**
	 * Constant value: <b>-11256521 [0xff543d37]</b>
	 * <p>
	 * Hex (RGB): <b>#543d37</b>
	 */
	@ColorInt public static final int DARK_LIVER_HORSES = Color.rgb(84, 61, 55);

	/**
	 * Constant value: <b>-7667573 [0xff8b008b]</b>
	 * <p>
	 * Hex (RGB): <b>#8b008b</b>
	 */
	@ColorInt public static final int DARK_MAGENTA = Color.rgb(139, 0, 139);

	/**
	 * Constant value: <b>-16764058 [0xff3366]</b>
	 * <p>
	 * Hex (RGB): <b>#003366</b>
	 */
	@ColorInt public static final int DARK_MIDNIGHT_BLUE = Color.rgb(0, 51, 102);

	/**
	 * Constant value: <b>-11903709 [0xff4a5d23]</b>
	 * <p>
	 * Hex (RGB): <b>#4a5d23</b>
	 */
	@ColorInt public static final int DARK_MOSS_GREEN = Color.rgb(74, 93, 35);

	/**
	 * Constant value: <b>-11179217 [0xff556b2f]</b>
	 * <p>
	 * Hex (RGB): <b>#556b2f</b>
	 */
	@ColorInt public static final int DARK_OLIVE_GREEN = Color.rgb(85, 107, 47);

	/**
	 * Constant value: <b>-29696 [0xffff8c00]</b>
	 * <p>
	 * Hex (RGB): <b>#ff8c00</b>
	 */
	@ColorInt public static final int DARK_ORANGE = Color.rgb(255, 140, 0);

	/**
	 * Constant value: <b>-6737204 [0xff9932cc]</b>
	 * <p>
	 * Hex (RGB): <b>#9932cc</b>
	 */
	@ColorInt public static final int DARK_ORCHID = Color.rgb(153, 50, 204);

	/**
	 * Constant value: <b>-8937781 [0xff779ecb]</b>
	 * <p>
	 * Hex (RGB): <b>#779ecb</b>
	 */
	@ColorInt public static final int DARK_PASTEL_BLUE = Color.rgb(119, 158, 203);

	/**
	 * Constant value: <b>-16531396 [0xff3c03c]</b>
	 * <p>
	 * Hex (RGB): <b>#03c03c</b>
	 */
	@ColorInt public static final int DARK_PASTEL_GREEN = Color.rgb(3, 192, 60);

	/**
	 * Constant value: <b>-6918186 [0xff966fd6]</b>
	 * <p>
	 * Hex (RGB): <b>#966fd6</b>
	 */
	@ColorInt public static final int DARK_PASTEL_PURPLE = Color.rgb(150, 111, 214);

	/**
	 * Constant value: <b>-4048094 [0xffc23b22]</b>
	 * <p>
	 * Hex (RGB): <b>#c23b22</b>
	 */
	@ColorInt public static final int DARK_PASTEL_RED = Color.rgb(194, 59, 34);

	/**
	 * Constant value: <b>-1616768 [0xffe75480]</b>
	 * <p>
	 * Hex (RGB): <b>#e75480</b>
	 */
	@ColorInt public static final int DARK_PINK = Color.rgb(231, 84, 128);

	/**
	 * Constant value: <b>-16764007 [0xff3399]</b>
	 * <p>
	 * Hex (RGB): <b>#003399</b>
	 */
	@ColorInt public static final int DARK_POWDER_BLUE = Color.rgb(0, 51, 153);

	/**
	 * Constant value: <b>-7920041 [0xff872657]</b>
	 * <p>
	 * Hex (RGB): <b>#872657</b>
	 */
	@ColorInt public static final int DARK_RASPBERRY = Color.rgb(135, 38, 87);

	/**
	 * Constant value: <b>-7667712 [0xff8b0000]</b>
	 * <p>
	 * Hex (RGB): <b>#8b0000</b>
	 */
	@ColorInt public static final int DARK_RED = Color.rgb(139, 0, 0);

	/**
	 * Constant value: <b>-1468806 [0xffe9967a]</b>
	 * <p>
	 * Hex (RGB): <b>#e9967a</b>
	 */
	@ColorInt public static final int DARK_SALMON = Color.rgb(233, 150, 122);

	/**
	 * Constant value: <b>-11140327 [0xff560319]</b>
	 * <p>
	 * Hex (RGB): <b>#560319</b>
	 */
	@ColorInt public static final int DARK_SCARLET = Color.rgb(86, 3, 25);

	/**
	 * Constant value: <b>-7357297 [0xff8fbc8f]</b>
	 * <p>
	 * Hex (RGB): <b>#8fbc8f</b>
	 */
	@ColorInt public static final int DARK_SEA_GREEN = Color.rgb(143, 188, 143);

	/**
	 * Constant value: <b>-12839916 [0xff3c1414]</b>
	 * <p>
	 * Hex (RGB): <b>#3c1414</b>
	 */
	@ColorInt public static final int DARK_SIENNA = Color.rgb(60, 20, 20);

	/**
	 * Constant value: <b>-7553322 [0xff8cbed6]</b>
	 * <p>
	 * Hex (RGB): <b>#8cbed6</b>
	 */
	@ColorInt public static final int DARK_SKY_BLUE = Color.rgb(140, 190, 214);

	/**
	 * Constant value: <b>-12042869 [0xff483d8b]</b>
	 * <p>
	 * Hex (RGB): <b>#483d8b</b>
	 */
	@ColorInt public static final int DARK_SLATE_BLUE = Color.rgb(72, 61, 139);

	/**
	 * Constant value: <b>-13676721 [0xff2f4f4f]</b>
	 * <p>
	 * Hex (RGB): <b>#2f4f4f</b>
	 */
	@ColorInt public static final int DARK_SLATE_GRAY = Color.rgb(47, 79, 79);

	/**
	 * Constant value: <b>-15240635 [0xff177245]</b>
	 * <p>
	 * Hex (RGB): <b>#177245</b>
	 */
	@ColorInt public static final int DARK_SPRING_GREEN = Color.rgb(23, 114, 69);

	/**
	 * Constant value: <b>-7241391 [0xff918151]</b>
	 * <p>
	 * Hex (RGB): <b>#918151</b>
	 */
	@ColorInt public static final int DARK_TAN = Color.rgb(145, 129, 81);

	/**
	 * Constant value: <b>-22510 [0xffffa812]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa812</b>
	 */
	@ColorInt public static final int DARK_TANGERINE = Color.rgb(255, 168, 18);

	/**
	 * Constant value: <b>-12043214 [0xff483c32]</b>
	 * <p>
	 * Hex (RGB): <b>#483c32</b>
	 */
	@ColorInt public static final int DARK_TAUPE = Color.rgb(72, 60, 50);

	/**
	 * Constant value: <b>-3387812 [0xffcc4e5c]</b>
	 * <p>
	 * Hex (RGB): <b>#cc4e5c</b>
	 */
	@ColorInt public static final int DARK_TERRA_COTTA = Color.rgb(204, 78, 92);

	/**
	 * Constant value: <b>-16724271 [0xffced1]</b>
	 * <p>
	 * Hex (RGB): <b>#00ced1</b>
	 */
	@ColorInt public static final int DARK_TURQUOISE = Color.rgb(0, 206, 209);

	/**
	 * Constant value: <b>-3031384 [0xffd1bea8]</b>
	 * <p>
	 * Hex (RGB): <b>#d1bea8</b>
	 */
	@ColorInt public static final int DARK_VANILLA = Color.rgb(209, 190, 168);

	/**
	 * Constant value: <b>-7077677 [0xff9400d3]</b>
	 * <p>
	 * Hex (RGB): <b>#9400d3</b>
	 */
	@ColorInt public static final int DARK_VIOLET = Color.rgb(148, 0, 211);

	/**
	 * Constant value: <b>-6584564 [0xff9b870c]</b>
	 * <p>
	 * Hex (RGB): <b>#9b870c</b>
	 */
	@ColorInt public static final int DARK_YELLOW = Color.rgb(155, 135, 12);

	/**
	 * Constant value: <b>-16748484 [0xff703c]</b>
	 * <p>
	 * Hex (RGB): <b>#00703c</b>
	 */
	@ColorInt public static final int DARTMOUTH_GREEN = Color.rgb(0, 112, 60);

	/**
	 * Constant value: <b>-11184811 [0xff555555]</b>
	 * <p>
	 * Hex (RGB): <b>#555555</b>
	 */
	@ColorInt public static final int DAVY_S_GREY = Color.rgb(85, 85, 85);

	/**
	 * Constant value: <b>-2684333 [0xffd70a53]</b>
	 * <p>
	 * Hex (RGB): <b>#d70a53</b>
	 */
	@ColorInt public static final int DEBIAN_RED = Color.rgb(215, 10, 83);

	/**
	 * Constant value: <b>-5693378 [0xffa9203e]</b>
	 * <p>
	 * Hex (RGB): <b>#a9203e</b>
	 */
	@ColorInt public static final int DEEP_CARMINE = Color.rgb(169, 32, 62);

	/**
	 * Constant value: <b>-1101768 [0xffef3038]</b>
	 * <p>
	 * Hex (RGB): <b>#ef3038</b>
	 */
	@ColorInt public static final int DEEP_CARMINE_PINK = Color.rgb(239, 48, 56);

	/**
	 * Constant value: <b>-1480404 [0xffe9692c]</b>
	 * <p>
	 * Hex (RGB): <b>#e9692c</b>
	 */
	@ColorInt public static final int DEEP_CARROT_ORANGE = Color.rgb(233, 105, 44);

	/**
	 * Constant value: <b>-2477433 [0xffda3287]</b>
	 * <p>
	 * Hex (RGB): <b>#da3287</b>
	 */
	@ColorInt public static final int DEEP_CERISE = Color.rgb(218, 50, 135);

	/**
	 * Constant value: <b>-338267 [0xfffad6a5]</b>
	 * <p>
	 * Hex (RGB): <b>#fad6a5</b>
	 */
	@ColorInt public static final int DEEP_CHAMPAGNE = Color.rgb(250, 214, 165);

	/**
	 * Constant value: <b>-4633016 [0xffb94e48]</b>
	 * <p>
	 * Hex (RGB): <b>#b94e48</b>
	 */
	@ColorInt public static final int DEEP_CHESTNUT = Color.rgb(185, 78, 72);

	/**
	 * Constant value: <b>-9420223 [0xff704241]</b>
	 * <p>
	 * Hex (RGB): <b>#704241</b>
	 */
	@ColorInt public static final int DEEP_COFFEE = Color.rgb(112, 66, 65);

	/**
	 * Constant value: <b>-4107071 [0xffc154c1]</b>
	 * <p>
	 * Hex (RGB): <b>#c154c1</b>
	 */
	@ColorInt public static final int DEEP_FUCHSIA = Color.rgb(193, 84, 193);

	/**
	 * Constant value: <b>-16757943 [0xff4b49]</b>
	 * <p>
	 * Hex (RGB): <b>#004b49</b>
	 */
	@ColorInt public static final int DEEP_JUNGLE_GREEN = Color.rgb(0, 75, 73);

	/**
	 * Constant value: <b>-669876 [0xfff5c74c]</b>
	 * <p>
	 * Hex (RGB): <b>#f5c74c</b>
	 */
	@ColorInt public static final int DEEP_LEMON = Color.rgb(245, 199, 76);

	/**
	 * Constant value: <b>-6728261 [0xff9955bb]</b>
	 * <p>
	 * Hex (RGB): <b>#9955bb</b>
	 */
	@ColorInt public static final int DEEP_LILAC = Color.rgb(153, 85, 187);

	/**
	 * Constant value: <b>-3407668 [0xffcc00cc]</b>
	 * <p>
	 * Hex (RGB): <b>#cc00cc</b>
	 */
	@ColorInt public static final int DEEP_MAGENTA = Color.rgb(204, 0, 204);

	/**
	 * Constant value: <b>-2853932 [0xffd473d4]</b>
	 * <p>
	 * Hex (RGB): <b>#d473d4</b>
	 */
	@ColorInt public static final int DEEP_MAUVE = Color.rgb(212, 115, 212);

	/**
	 * Constant value: <b>-13279685 [0xff355e3b]</b>
	 * <p>
	 * Hex (RGB): <b>#355e3b</b>
	 */
	@ColorInt public static final int DEEP_MOSS_GREEN = Color.rgb(53, 94, 59);

	/**
	 * Constant value: <b>-13404 [0xffffcba4]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcba4</b>
	 */
	@ColorInt public static final int DEEP_PEACH = Color.rgb(255, 203, 164);

	/**
	 * Constant value: <b>-60269 [0xffff1493]</b>
	 * <p>
	 * Hex (RGB): <b>#ff1493</b>
	 */
	@ColorInt public static final int DEEP_PINK = Color.rgb(255, 20, 147);

	/**
	 * Constant value: <b>-8110245 [0xff843f5b]</b>
	 * <p>
	 * Hex (RGB): <b>#843f5b</b>
	 */
	@ColorInt public static final int DEEP_RUBY = Color.rgb(132, 63, 91);

	/**
	 * Constant value: <b>-26317 [0xffff9933]</b>
	 * <p>
	 * Hex (RGB): <b>#ff9933</b>
	 */
	@ColorInt public static final int DEEP_SAFFRON = Color.rgb(255, 153, 51);

	/**
	 * Constant value: <b>-16728065 [0xffbfff]</b>
	 * <p>
	 * Hex (RGB): <b>#00bfff</b>
	 */
	@ColorInt public static final int DEEP_SKY_BLUE = Color.rgb(0, 191, 255);

	/**
	 * Constant value: <b>-11901844 [0xff4a646c]</b>
	 * <p>
	 * Hex (RGB): <b>#4a646c</b>
	 */
	@ColorInt public static final int DEEP_SPACE_SPARKLE = Color.rgb(74, 100, 108);

	/**
	 * Constant value: <b>-8495520 [0xff7e5e60]</b>
	 * <p>
	 * Hex (RGB): <b>#7e5e60</b>
	 */
	@ColorInt public static final int DEEP_TAUPE = Color.rgb(126, 94, 96);

	/**
	 * Constant value: <b>-10075571 [0xff66424d]</b>
	 * <p>
	 * Hex (RGB): <b>#66424d</b>
	 */
	@ColorInt public static final int DEEP_TUSCAN_RED = Color.rgb(102, 66, 77);

	/**
	 * Constant value: <b>-4552871 [0xffba8759]</b>
	 * <p>
	 * Hex (RGB): <b>#ba8759</b>
	 */
	@ColorInt public static final int DEER = Color.rgb(186, 135, 89);

	/**
	 * Constant value: <b>-15376195 [0xff1560bd]</b>
	 * <p>
	 * Hex (RGB): <b>#1560bd</b>
	 */
	@ColorInt public static final int DENIM = Color.rgb(21, 96, 189);

	/**
	 * Constant value: <b>-4089237 [0xffc19a6b]</b>
	 * <p>
	 * Hex (RGB): <b>#c19a6b</b>
	 */
	@ColorInt public static final int DESERT = Color.rgb(193, 154, 107);

	/**
	 * Constant value: <b>-1193553 [0xffedc9af]</b>
	 * <p>
	 * Hex (RGB): <b>#edc9af</b>
	 */
	@ColorInt public static final int DESERT_SAND = Color.rgb(237, 201, 175);

	/**
	 * Constant value: <b>-4590849 [0xffb9f2ff]</b>
	 * <p>
	 * Hex (RGB): <b>#b9f2ff</b>
	 */
	@ColorInt public static final int DIAMOND = Color.rgb(185, 242, 255);

	/**
	 * Constant value: <b>-9868951 [0xff696969]</b>
	 * <p>
	 * Hex (RGB): <b>#696969</b>
	 */
	@ColorInt public static final int DIM_GRAY = Color.rgb(105, 105, 105);

	/**
	 * Constant value: <b>-6588845 [0xff9b7653]</b>
	 * <p>
	 * Hex (RGB): <b>#9b7653</b>
	 */
	@ColorInt public static final int DIRT = Color.rgb(155, 118, 83);

	/**
	 * Constant value: <b>-14774017 [0xff1e90ff]</b>
	 * <p>
	 * Hex (RGB): <b>#1e90ff</b>
	 */
	@ColorInt public static final int DODGER_BLUE = Color.rgb(30, 144, 255);

	/**
	 * Constant value: <b>-2680728 [0xffd71868]</b>
	 * <p>
	 * Hex (RGB): <b>#d71868</b>
	 */
	@ColorInt public static final int DOGWOOD_ROSE = Color.rgb(215, 24, 104);

	/**
	 * Constant value: <b>-8012955 [0xff85bb65]</b>
	 * <p>
	 * Hex (RGB): <b>#85bb65</b>
	 */
	@ColorInt public static final int DOLLAR_BILL = Color.rgb(133, 187, 101);

	/**
	 * Constant value: <b>-10073048 [0xff664c28]</b>
	 * <p>
	 * Hex (RGB): <b>#664c28</b>
	 */
	@ColorInt public static final int DONKEY_BROWN = Color.rgb(102, 76, 40);

	/**
	 * Constant value: <b>-6917865 [0xff967117]</b>
	 * <p>
	 * Hex (RGB): <b>#967117</b>
	 */
	@ColorInt public static final int DRAB = Color.rgb(150, 113, 23);

	/**
	 * Constant value: <b>-16777060 [0xff9c]</b>
	 * <p>
	 * Hex (RGB): <b>#00009c</b>
	 */
	@ColorInt public static final int DUKE_BLUE = Color.rgb(0, 0, 156);

	/**
	 * Constant value: <b>-1717047 [0xffe5ccc9]</b>
	 * <p>
	 * Hex (RGB): <b>#e5ccc9</b>
	 */
	@ColorInt public static final int DUST_STORM = Color.rgb(229, 204, 201);

	/**
	 * Constant value: <b>-1988257 [0xffe1a95f]</b>
	 * <p>
	 * Hex (RGB): <b>#e1a95f</b>
	 */
	@ColorInt public static final int EARTH_YELLOW = Color.rgb(225, 169, 95);

	/**
	 * Constant value: <b>-11182768 [0xff555d50]</b>
	 * <p>
	 * Hex (RGB): <b>#555d50</b>
	 */
	@ColorInt public static final int EBONY = Color.rgb(85, 93, 80);

	/**
	 * Constant value: <b>-4017536 [0xffc2b280]</b>
	 * <p>
	 * Hex (RGB): <b>#c2b280</b>
	 */
	@ColorInt public static final int ECRU = Color.rgb(194, 178, 128);

	/**
	 * Constant value: <b>-10403759 [0xff614051]</b>
	 * <p>
	 * Hex (RGB): <b>#614051</b>
	 */
	@ColorInt public static final int EGGPLANT = Color.rgb(97, 64, 81);

	/**
	 * Constant value: <b>-988458 [0xfff0ead6]</b>
	 * <p>
	 * Hex (RGB): <b>#f0ead6</b>
	 */
	@ColorInt public static final int EGGSHELL = Color.rgb(240, 234, 214);

	/**
	 * Constant value: <b>-15715162 [0xff1034a6]</b>
	 * <p>
	 * Hex (RGB): <b>#1034a6</b>
	 */
	@ColorInt public static final int EGYPTIAN_BLUE = Color.rgb(16, 52, 166);

	/**
	 * Constant value: <b>-8521217 [0xff7df9ff]</b>
	 * <p>
	 * Hex (RGB): <b>#7df9ff</b>
	 */
	@ColorInt public static final int ELECTRIC_BLUE = Color.rgb(125, 249, 255);

	/**
	 * Constant value: <b>-65473 [0xffff003f]</b>
	 * <p>
	 * Hex (RGB): <b>#ff003f</b>
	 */
	@ColorInt public static final int ELECTRIC_CRIMSON = Color.rgb(255, 0, 63);

	/**
	 * Constant value: <b>-16711681 [0xffffff]</b>
	 * <p>
	 * Hex (RGB): <b>#00ffff</b>
	 */
	@ColorInt public static final int ELECTRIC_CYAN = Color.rgb(0, 255, 255);

	/**
	 * Constant value: <b>-16711936 [0xffff00]</b>
	 * <p>
	 * Hex (RGB): <b>#00ff00</b>
	 */
	@ColorInt public static final int ELECTRIC_GREEN = Color.rgb(0, 255, 0);

	/**
	 * Constant value: <b>-9502465 [0xff6f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#6f00ff</b>
	 */
	@ColorInt public static final int ELECTRIC_INDIGO = Color.rgb(111, 0, 255);

	/**
	 * Constant value: <b>-738305 [0xfff4bbff]</b>
	 * <p>
	 * Hex (RGB): <b>#f4bbff</b>
	 */
	@ColorInt public static final int ELECTRIC_LAVENDER = Color.rgb(244, 187, 255);

	/**
	 * Constant value: <b>-3342592 [0xffccff00]</b>
	 * <p>
	 * Hex (RGB): <b>#ccff00</b>
	 */
	@ColorInt public static final int ELECTRIC_LIME = Color.rgb(204, 255, 0);

	/**
	 * Constant value: <b>-4259585 [0xffbf00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#bf00ff</b>
	 */
	@ColorInt public static final int ELECTRIC_PURPLE = Color.rgb(191, 0, 255);

	/**
	 * Constant value: <b>-12648193 [0xff3f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#3f00ff</b>
	 */
	@ColorInt public static final int ELECTRIC_ULTRAMARINE = Color.rgb(63, 0, 255);

	/**
	 * Constant value: <b>-7405313 [0xff8f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#8f00ff</b>
	 */
	@ColorInt public static final int ELECTRIC_VIOLET = Color.rgb(143, 0, 255);

	/**
	 * Constant value: <b>-205 [0xffffff33]</b>
	 * <p>
	 * Hex (RGB): <b>#ffff33</b>
	 */
	@ColorInt public static final int ELECTRIC_YELLOW = Color.rgb(255, 255, 51);

	/**
	 * Constant value: <b>-11483016 [0xff50c878]</b>
	 * <p>
	 * Hex (RGB): <b>#50c878</b>
	 */
	@ColorInt public static final int EMERALD = Color.rgb(80, 200, 120);

	/**
	 * Constant value: <b>-14987970 [0xff1b4d3e]</b>
	 * <p>
	 * Hex (RGB): <b>#1b4d3e</b>
	 */
	@ColorInt public static final int ENGLISH_GREEN = Color.rgb(27, 77, 62);

	/**
	 * Constant value: <b>-4947051 [0xffb48395]</b>
	 * <p>
	 * Hex (RGB): <b>#b48395</b>
	 */
	@ColorInt public static final int ENGLISH_LAVENDER = Color.rgb(180, 131, 149);

	/**
	 * Constant value: <b>-5550510 [0xffab4e52]</b>
	 * <p>
	 * Hex (RGB): <b>#ab4e52</b>
	 */
	@ColorInt public static final int ENGLISH_RED = Color.rgb(171, 78, 82);

	/**
	 * Constant value: <b>-11125668 [0xff563c5c]</b>
	 * <p>
	 * Hex (RGB): <b>#563c5c</b>
	 */
	@ColorInt public static final int ENGLISH_VIOLET = Color.rgb(86, 60, 92);

	/**
	 * Constant value: <b>-6895454 [0xff96c8a2]</b>
	 * <p>
	 * Hex (RGB): <b>#96c8a2</b>
	 */
	@ColorInt public static final int ETON_BLUE = Color.rgb(150, 200, 162);

	/**
	 * Constant value: <b>-12265560 [0xff44d7a8]</b>
	 * <p>
	 * Hex (RGB): <b>#44d7a8</b>
	 */
	@ColorInt public static final int EUCALYPTUS = Color.rgb(68, 215, 168);

	/**
	 * Constant value: <b>-4089237 [0xffc19a6b]</b>
	 * <p>
	 * Hex (RGB): <b>#c19a6b</b>
	 */
	@ColorInt public static final int FALLOW = Color.rgb(193, 154, 107);

	/**
	 * Constant value: <b>-8382440 [0xff801818]</b>
	 * <p>
	 * Hex (RGB): <b>#801818</b>
	 */
	@ColorInt public static final int FALU_RED = Color.rgb(128, 24, 24);

	/**
	 * Constant value: <b>-4902007 [0xffb53389]</b>
	 * <p>
	 * Hex (RGB): <b>#b53389</b>
	 */
	@ColorInt public static final int FANDANGO = Color.rgb(181, 51, 137);

	/**
	 * Constant value: <b>-2207099 [0xffde5285]</b>
	 * <p>
	 * Hex (RGB): <b>#de5285</b>
	 */
	@ColorInt public static final int FANDANGO_PINK = Color.rgb(222, 82, 133);

	/**
	 * Constant value: <b>-786271 [0xfff400a1]</b>
	 * <p>
	 * Hex (RGB): <b>#f400a1</b>
	 */
	@ColorInt public static final int FASHION_FUCHSIA = Color.rgb(244, 0, 161);

	/**
	 * Constant value: <b>-1725840 [0xffe5aa70]</b>
	 * <p>
	 * Hex (RGB): <b>#e5aa70</b>
	 */
	@ColorInt public static final int FAWN = Color.rgb(229, 170, 112);

	/**
	 * Constant value: <b>-11707053 [0xff4d5d53]</b>
	 * <p>
	 * Hex (RGB): <b>#4d5d53</b>
	 */
	@ColorInt public static final int FELDGRAU = Color.rgb(77, 93, 83);

	/**
	 * Constant value: <b>-2050686 [0xffe0b582]</b>
	 * <p>
	 * Hex (RGB): <b>#e0b582</b>
	 */
	@ColorInt public static final int FELDSPAR = Color.rgb(224, 181, 130);

	/**
	 * Constant value: <b>-11568830 [0xff4f7942]</b>
	 * <p>
	 * Hex (RGB): <b>#4f7942</b>
	 */
	@ColorInt public static final int FERN_GREEN = Color.rgb(79, 121, 66);

	/**
	 * Constant value: <b>-55296 [0xffff2800]</b>
	 * <p>
	 * Hex (RGB): <b>#ff2800</b>
	 */
	@ColorInt public static final int FERRARI_RED = Color.rgb(255, 40, 0);

	/**
	 * Constant value: <b>-9677794 [0xff6c541e]</b>
	 * <p>
	 * Hex (RGB): <b>#6c541e</b>
	 */
	@ColorInt public static final int FIELD_DRAB = Color.rgb(108, 84, 30);

	/**
	 * Constant value: <b>-5103070 [0xffb22222]</b>
	 * <p>
	 * Hex (RGB): <b>#b22222</b>
	 */
	@ColorInt public static final int FIREBRICK = Color.rgb(178, 34, 34);

	/**
	 * Constant value: <b>-3268567 [0xffce2029]</b>
	 * <p>
	 * Hex (RGB): <b>#ce2029</b>
	 */
	@ColorInt public static final int FIRE_ENGINE_RED = Color.rgb(206, 32, 41);

	/**
	 * Constant value: <b>-1943518 [0xffe25822]</b>
	 * <p>
	 * Hex (RGB): <b>#e25822</b>
	 */
	@ColorInt public static final int FLAME = Color.rgb(226, 88, 34);

	/**
	 * Constant value: <b>-225620 [0xfffc8eac]</b>
	 * <p>
	 * Hex (RGB): <b>#fc8eac</b>
	 */
	@ColorInt public static final int FLAMINGO_PINK = Color.rgb(252, 142, 172);

	/**
	 * Constant value: <b>-10075101 [0xff664423]</b>
	 * <p>
	 * Hex (RGB): <b>#664423</b>
	 */
	@ColorInt public static final int FLATTERY = Color.rgb(102, 68, 35);

	/**
	 * Constant value: <b>-530034 [0xfff7e98e]</b>
	 * <p>
	 * Hex (RGB): <b>#f7e98e</b>
	 */
	@ColorInt public static final int FLAVESCENT = Color.rgb(247, 233, 142);

	/**
	 * Constant value: <b>-1123198 [0xffeedc82]</b>
	 * <p>
	 * Hex (RGB): <b>#eedc82</b>
	 */
	@ColorInt public static final int FLAX = Color.rgb(238, 220, 130);

	/**
	 * Constant value: <b>-6160275 [0xffa2006d]</b>
	 * <p>
	 * Hex (RGB): <b>#a2006d</b>
	 */
	@ColorInt public static final int FLIRT = Color.rgb(162, 0, 109);

	/**
	 * Constant value: <b>-1296 [0xfffffaf0]</b>
	 * <p>
	 * Hex (RGB): <b>#fffaf0</b>
	 */
	@ColorInt public static final int FLORAL_WHITE = Color.rgb(255, 250, 240);

	/**
	 * Constant value: <b>-16640 [0xffffbf00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffbf00</b>
	 */
	@ColorInt public static final int FLUORESCENT_ORANGE = Color.rgb(255, 191, 0);

	/**
	 * Constant value: <b>-60269 [0xffff1493]</b>
	 * <p>
	 * Hex (RGB): <b>#ff1493</b>
	 */
	@ColorInt public static final int FLUORESCENT_PINK = Color.rgb(255, 20, 147);

	/**
	 * Constant value: <b>-3342592 [0xffccff00]</b>
	 * <p>
	 * Hex (RGB): <b>#ccff00</b>
	 */
	@ColorInt public static final int FLUORESCENT_YELLOW = Color.rgb(204, 255, 0);

	/**
	 * Constant value: <b>-65457 [0xffff004f]</b>
	 * <p>
	 * Hex (RGB): <b>#ff004f</b>
	 */
	@ColorInt public static final int FOLLY = Color.rgb(255, 0, 79);

	/**
	 * Constant value: <b>-16694239 [0xff14421]</b>
	 * <p>
	 * Hex (RGB): <b>#014421</b>
	 */
	@ColorInt public static final int FOREST_GREEN_TRADITIONAL = Color.rgb(1, 68, 33);

	/**
	 * Constant value: <b>-14513374 [0xff228b22]</b>
	 * <p>
	 * Hex (RGB): <b>#228b22</b>
	 */
	@ColorInt public static final int FOREST_GREEN_WEB = Color.rgb(34, 139, 34);

	/**
	 * Constant value: <b>-5866661 [0xffa67b5b]</b>
	 * <p>
	 * Hex (RGB): <b>#a67b5b</b>
	 */
	@ColorInt public static final int FRENCH_BEIGE = Color.rgb(166, 123, 91);

	/**
	 * Constant value: <b>-8032947 [0xff856d4d]</b>
	 * <p>
	 * Hex (RGB): <b>#856d4d</b>
	 */
	@ColorInt public static final int FRENCH_BISTRE = Color.rgb(133, 109, 77);

	/**
	 * Constant value: <b>-16747845 [0xff72bb]</b>
	 * <p>
	 * Hex (RGB): <b>#0072bb</b>
	 */
	@ColorInt public static final int FRENCH_BLUE = Color.rgb(0, 114, 187);

	/**
	 * Constant value: <b>-7970674 [0xff86608e]</b>
	 * <p>
	 * Hex (RGB): <b>#86608e</b>
	 */
	@ColorInt public static final int FRENCH_LILAC = Color.rgb(134, 96, 142);

	/**
	 * Constant value: <b>-6357704 [0xff9efd38]</b>
	 * <p>
	 * Hex (RGB): <b>#9efd38</b>
	 */
	@ColorInt public static final int FRENCH_LIME = Color.rgb(158, 253, 56);

	/**
	 * Constant value: <b>-2853932 [0xffd473d4]</b>
	 * <p>
	 * Hex (RGB): <b>#d473d4</b>
	 */
	@ColorInt public static final int FRENCH_MAUVE = Color.rgb(212, 115, 212);

	/**
	 * Constant value: <b>-3724216 [0xffc72c48]</b>
	 * <p>
	 * Hex (RGB): <b>#c72c48</b>
	 */
	@ColorInt public static final int FRENCH_RASPBERRY = Color.rgb(199, 44, 72);

	/**
	 * Constant value: <b>-636278 [0xfff64a8a]</b>
	 * <p>
	 * Hex (RGB): <b>#f64a8a</b>
	 */
	@ColorInt public static final int FRENCH_ROSE = Color.rgb(246, 74, 138);

	/**
	 * Constant value: <b>-8931842 [0xff77b5fe]</b>
	 * <p>
	 * Hex (RGB): <b>#77b5fe</b>
	 */
	@ColorInt public static final int FRENCH_SKY_BLUE = Color.rgb(119, 181, 254);

	/**
	 * Constant value: <b>-5497276 [0xffac1e44]</b>
	 * <p>
	 * Hex (RGB): <b>#ac1e44</b>
	 */
	@ColorInt public static final int FRENCH_WINE = Color.rgb(172, 30, 68);

	/**
	 * Constant value: <b>-5838849 [0xffa6e7ff]</b>
	 * <p>
	 * Hex (RGB): <b>#a6e7ff</b>
	 */
	@ColorInt public static final int FRESH_AIR = Color.rgb(166, 231, 255);

	/**
	 * Constant value: <b>-65281 [0xffff00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#ff00ff</b>
	 */
	@ColorInt public static final int FUCHSIA = Color.rgb(255, 0, 255);

	/**
	 * Constant value: <b>-4107071 [0xffc154c1]</b>
	 * <p>
	 * Hex (RGB): <b>#c154c1</b>
	 */
	@ColorInt public static final int FUCHSIA_CRAYOLA = Color.rgb(193, 84, 193);

	/**
	 * Constant value: <b>-34817 [0xffff77ff]</b>
	 * <p>
	 * Hex (RGB): <b>#ff77ff</b>
	 */
	@ColorInt public static final int FUCHSIA_PINK = Color.rgb(255, 119, 255);

	/**
	 * Constant value: <b>-3718283 [0xffc74375]</b>
	 * <p>
	 * Hex (RGB): <b>#c74375</b>
	 */
	@ColorInt public static final int FUCHSIA_ROSE = Color.rgb(199, 67, 117);

	/**
	 * Constant value: <b>-1801216 [0xffe48400]</b>
	 * <p>
	 * Hex (RGB): <b>#e48400</b>
	 */
	@ColorInt public static final int FULVOUS = Color.rgb(228, 132, 0);

	/**
	 * Constant value: <b>-3381658 [0xffcc6666]</b>
	 * <p>
	 * Hex (RGB): <b>#cc6666</b>
	 */
	@ColorInt public static final int FUZZY_WUZZY = Color.rgb(204, 102, 102);

	/**
	 * Constant value: <b>-2302756 [0xffdcdcdc]</b>
	 * <p>
	 * Hex (RGB): <b>#dcdcdc</b>
	 */
	@ColorInt public static final int GAINSBORO = Color.rgb(220, 220, 220);

	/**
	 * Constant value: <b>-1795313 [0xffe49b0f]</b>
	 * <p>
	 * Hex (RGB): <b>#e49b0f</b>
	 */
	@ColorInt public static final int GAMBOGE = Color.rgb(228, 155, 15);

	/**
	 * Constant value: <b>-16744602 [0xff7f66]</b>
	 * <p>
	 * Hex (RGB): <b>#007f66</b>
	 */
	@ColorInt public static final int GENERIC_VIRIDIAN = Color.rgb(0, 127, 102);

	/**
	 * Constant value: <b>-460545 [0xfff8f8ff]</b>
	 * <p>
	 * Hex (RGB): <b>#f8f8ff</b>
	 */
	@ColorInt public static final int GHOST_WHITE = Color.rgb(248, 248, 255);

	/**
	 * Constant value: <b>-108003 [0xfffe5a1d]</b>
	 * <p>
	 * Hex (RGB): <b>#fe5a1d</b>
	 */
	@ColorInt public static final int GIANTS_ORANGE = Color.rgb(254, 90, 29);

	/**
	 * Constant value: <b>-5217024 [0xffb06500]</b>
	 * <p>
	 * Hex (RGB): <b>#b06500</b>
	 */
	@ColorInt public static final int GINGER = Color.rgb(176, 101, 0);

	/**
	 * Constant value: <b>-10452298 [0xff6082b6]</b>
	 * <p>
	 * Hex (RGB): <b>#6082b6</b>
	 */
	@ColorInt public static final int GLAUCOUS = Color.rgb(96, 130, 182);

	/**
	 * Constant value: <b>-1644294 [0xffe6e8fa]</b>
	 * <p>
	 * Hex (RGB): <b>#e6e8fa</b>
	 */
	@ColorInt public static final int GLITTER = Color.rgb(230, 232, 250);

	/**
	 * Constant value: <b>-16733338 [0xffab66]</b>
	 * <p>
	 * Hex (RGB): <b>#00ab66</b>
	 */
	@ColorInt public static final int GO_GREEN = Color.rgb(0, 171, 102);

	/**
	 * Constant value: <b>-2838729 [0xffd4af37]</b>
	 * <p>
	 * Hex (RGB): <b>#d4af37</b>
	 */
	@ColorInt public static final int GOLD_METALLIC = Color.rgb(212, 175, 55);

	/**
	 * Constant value: <b>-10496 [0xffffd700]</b>
	 * <p>
	 * Hex (RGB): <b>#ffd700</b>
	 */
	@ColorInt public static final int GOLD_WEB_GOLDEN = Color.rgb(255, 215, 0);

	/**
	 * Constant value: <b>-8030898 [0xff85754e]</b>
	 * <p>
	 * Hex (RGB): <b>#85754e</b>
	 */
	@ColorInt public static final int GOLD_FUSION = Color.rgb(133, 117, 78);

	/**
	 * Constant value: <b>-6724331 [0xff996515]</b>
	 * <p>
	 * Hex (RGB): <b>#996515</b>
	 */
	@ColorInt public static final int GOLDEN_BROWN = Color.rgb(153, 101, 21);

	/**
	 * Constant value: <b>-212480 [0xfffcc200]</b>
	 * <p>
	 * Hex (RGB): <b>#fcc200</b>
	 */
	@ColorInt public static final int GOLDEN_POPPY = Color.rgb(252, 194, 0);

	/**
	 * Constant value: <b>-8448 [0xffffdf00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffdf00</b>
	 */
	@ColorInt public static final int GOLDEN_YELLOW = Color.rgb(255, 223, 0);

	/**
	 * Constant value: <b>-2448096 [0xffdaa520]</b>
	 * <p>
	 * Hex (RGB): <b>#daa520</b>
	 */
	@ColorInt public static final int GOLDENROD = Color.rgb(218, 165, 32);

	/**
	 * Constant value: <b>-5708640 [0xffa8e4a0]</b>
	 * <p>
	 * Hex (RGB): <b>#a8e4a0</b>
	 */
	@ColorInt public static final int GRANNY_SMITH_APPLE = Color.rgb(168, 228, 160);

	/**
	 * Constant value: <b>-9818712 [0xff6a2da8]</b>
	 * <p>
	 * Hex (RGB): <b>#6a2da8</b>
	 */
	@ColorInt public static final int GRAPE = Color.rgb(106, 45, 168);

	/**
	 * Constant value: <b>-8355712 [0xff808080]</b>
	 * <p>
	 * Hex (RGB): <b>#808080</b>
	 */
	@ColorInt public static final int GRAY = Color.rgb(128, 128, 128);

	/**
	 * Constant value: <b>-8355712 [0xff808080]</b>
	 * <p>
	 * Hex (RGB): <b>#808080</b>
	 */
	@ColorInt public static final int GRAY_HTML_CSS_GRAY = Color.rgb(128, 128, 128);

	/**
	 * Constant value: <b>-4276546 [0xffbebebe]</b>
	 * <p>
	 * Hex (RGB): <b>#bebebe</b>
	 */
	@ColorInt public static final int GRAY_X11_GRAY = Color.rgb(190, 190, 190);

	/**
	 * Constant value: <b>-12166843 [0xff465945]</b>
	 * <p>
	 * Hex (RGB): <b>#465945</b>
	 */
	@ColorInt public static final int GRAY_ASPARAGUS = Color.rgb(70, 89, 69);

	/**
	 * Constant value: <b>-7564628 [0xff8c92ac]</b>
	 * <p>
	 * Hex (RGB): <b>#8c92ac</b>
	 */
	@ColorInt public static final int GRAY_BLUE = Color.rgb(140, 146, 172);

	/**
	 * Constant value: <b>-16711936 [0xffff00]</b>
	 * <p>
	 * Hex (RGB): <b>#00ff00</b>
	 */
	@ColorInt public static final int GREEN_COLOR_WHEEL_X11_GREEN = Color.rgb(0, 255, 0);

	/**
	 * Constant value: <b>-14898056 [0xff1cac78]</b>
	 * <p>
	 * Hex (RGB): <b>#1cac78</b>
	 */
	@ColorInt public static final int GREEN_CRAYOLA = Color.rgb(28, 172, 120);

	/**
	 * Constant value: <b>-16744448 [0xff8000]</b>
	 * <p>
	 * Hex (RGB): <b>#008000</b>
	 */
	@ColorInt public static final int GREEN_HTML_CSS_COLOR = Color.rgb(0, 128, 0);

	/**
	 * Constant value: <b>-16734089 [0xffa877]</b>
	 * <p>
	 * Hex (RGB): <b>#00a877</b>
	 */
	@ColorInt public static final int GREEN_MUNSELL = Color.rgb(0, 168, 119);

	/**
	 * Constant value: <b>-16736405 [0xff9f6b]</b>
	 * <p>
	 * Hex (RGB): <b>#009f6b</b>
	 */
	@ColorInt public static final int GREEN_NCS = Color.rgb(0, 159, 107);

	/**
	 * Constant value: <b>-16734896 [0xffa550]</b>
	 * <p>
	 * Hex (RGB): <b>#00a550</b>
	 */
	@ColorInt public static final int GREEN_PIGMENT = Color.rgb(0, 165, 80);

	/**
	 * Constant value: <b>-10047438 [0xff66b032]</b>
	 * <p>
	 * Hex (RGB): <b>#66b032</b>
	 */
	@ColorInt public static final int GREEN_RYB = Color.rgb(102, 176, 50);

	/**
	 * Constant value: <b>-5374161 [0xffadff2f]</b>
	 * <p>
	 * Hex (RGB): <b>#adff2f</b>
	 */
	@ColorInt public static final int GREEN_YELLOW = Color.rgb(173, 255, 47);

	/**
	 * Constant value: <b>-5662074 [0xffa99a86]</b>
	 * <p>
	 * Hex (RGB): <b>#a99a86</b>
	 */
	@ColorInt public static final int GRULLO = Color.rgb(169, 154, 134);

	/**
	 * Constant value: <b>-16711809 [0xffff7f]</b>
	 * <p>
	 * Hex (RGB): <b>#00ff7f</b>
	 */
	@ColorInt public static final int GUPPIE_GREEN = Color.rgb(0, 255, 127);

	/**
	 * Constant value: <b>-10078380 [0xff663754]</b>
	 * <p>
	 * Hex (RGB): <b>#663754</b>
	 */
	@ColorInt public static final int HALAYA_UBE = Color.rgb(102, 55, 84);

	/**
	 * Constant value: <b>-12292913 [0xff446ccf]</b>
	 * <p>
	 * Hex (RGB): <b>#446ccf</b>
	 */
	@ColorInt public static final int HAN_BLUE = Color.rgb(68, 108, 207);

	/**
	 * Constant value: <b>-11396870 [0xff5218fa]</b>
	 * <p>
	 * Hex (RGB): <b>#5218fa</b>
	 */
	@ColorInt public static final int HAN_PURPLE = Color.rgb(82, 24, 250);

	/**
	 * Constant value: <b>-1452437 [0xffe9d66b]</b>
	 * <p>
	 * Hex (RGB): <b>#e9d66b</b>
	 */
	@ColorInt public static final int HANSA_YELLOW = Color.rgb(233, 214, 107);

	/**
	 * Constant value: <b>-12583168 [0xff3fff00]</b>
	 * <p>
	 * Hex (RGB): <b>#3fff00</b>
	 */
	@ColorInt public static final int HARLEQUIN = Color.rgb(63, 255, 0);

	/**
	 * Constant value: <b>-3604458 [0xffc90016]</b>
	 * <p>
	 * Hex (RGB): <b>#c90016</b>
	 */
	@ColorInt public static final int HARVARD_CRIMSON = Color.rgb(201, 0, 22);

	/**
	 * Constant value: <b>-2453248 [0xffda9100]</b>
	 * <p>
	 * Hex (RGB): <b>#da9100</b>
	 */
	@ColorInt public static final int HARVEST_GOLD = Color.rgb(218, 145, 0);

	/**
	 * Constant value: <b>-8355840 [0xff808000]</b>
	 * <p>
	 * Hex (RGB): <b>#808000</b>
	 */
	@ColorInt public static final int HEART_GOLD = Color.rgb(128, 128, 0);

	/**
	 * Constant value: <b>-2132993 [0xffdf73ff]</b>
	 * <p>
	 * Hex (RGB): <b>#df73ff</b>
	 */
	@ColorInt public static final int HELIOTROPE = Color.rgb(223, 115, 255);

	/**
	 * Constant value: <b>-786271 [0xfff400a1]</b>
	 * <p>
	 * Hex (RGB): <b>#f400a1</b>
	 */
	@ColorInt public static final int HOLLYWOOD_CERISE = Color.rgb(244, 0, 161);

	/**
	 * Constant value: <b>-983056 [0xfff0fff0]</b>
	 * <p>
	 * Hex (RGB): <b>#f0fff0</b>
	 */
	@ColorInt public static final int HONEYDEW = Color.rgb(240, 255, 240);

	/**
	 * Constant value: <b>-16749136 [0xff6db0]</b>
	 * <p>
	 * Hex (RGB): <b>#006db0</b>
	 */
	@ColorInt public static final int HONOLULU_BLUE = Color.rgb(0, 109, 176);

	/**
	 * Constant value: <b>-11962005 [0xff49796b]</b>
	 * <p>
	 * Hex (RGB): <b>#49796b</b>
	 */
	@ColorInt public static final int HOOKER_S_GREEN = Color.rgb(73, 121, 107);

	/**
	 * Constant value: <b>-57906 [0xffff1dce]</b>
	 * <p>
	 * Hex (RGB): <b>#ff1dce</b>
	 */
	@ColorInt public static final int HOT_MAGENTA = Color.rgb(255, 29, 206);

	/**
	 * Constant value: <b>-38476 [0xffff69b4]</b>
	 * <p>
	 * Hex (RGB): <b>#ff69b4</b>
	 */
	@ColorInt public static final int HOT_PINK = Color.rgb(255, 105, 180);

	/**
	 * Constant value: <b>-13279685 [0xff355e3b]</b>
	 * <p>
	 * Hex (RGB): <b>#355e3b</b>
	 */
	@ColorInt public static final int HUNTER_GREEN = Color.rgb(53, 94, 59);

	/**
	 * Constant value: <b>-9328942 [0xff71a6d2]</b>
	 * <p>
	 * Hex (RGB): <b>#71a6d2</b>
	 */
	@ColorInt public static final int ICEBERG = Color.rgb(113, 166, 210);

	/**
	 * Constant value: <b>-198818 [0xfffcf75e]</b>
	 * <p>
	 * Hex (RGB): <b>#fcf75e</b>
	 */
	@ColorInt public static final int ICTERINE = Color.rgb(252, 247, 94);

	/**
	 * Constant value: <b>-13528713 [0xff319177]</b>
	 * <p>
	 * Hex (RGB): <b>#319177</b>
	 */
	@ColorInt public static final int ILLUMINATING_EMERALD = Color.rgb(49, 145, 119);

	/**
	 * Constant value: <b>-10473621 [0xff602f6b]</b>
	 * <p>
	 * Hex (RGB): <b>#602f6b</b>
	 */
	@ColorInt public static final int IMPERIAL = Color.rgb(96, 47, 107);

	/**
	 * Constant value: <b>-16768107 [0xff2395]</b>
	 * <p>
	 * Hex (RGB): <b>#002395</b>
	 */
	@ColorInt public static final int IMPERIAL_BLUE = Color.rgb(0, 35, 149);

	/**
	 * Constant value: <b>-10091972 [0xff66023c]</b>
	 * <p>
	 * Hex (RGB): <b>#66023c</b>
	 */
	@ColorInt public static final int IMPERIAL_PURPLE = Color.rgb(102, 2, 60);

	/**
	 * Constant value: <b>-1234631 [0xffed2939]</b>
	 * <p>
	 * Hex (RGB): <b>#ed2939</b>
	 */
	@ColorInt public static final int IMPERIAL_RED = Color.rgb(237, 41, 57);

	/**
	 * Constant value: <b>-5051299 [0xffb2ec5d]</b>
	 * <p>
	 * Hex (RGB): <b>#b2ec5d</b>
	 */
	@ColorInt public static final int INCHWORM = Color.rgb(178, 236, 93);

	/**
	 * Constant value: <b>-11775635 [0xff4c516d]</b>
	 * <p>
	 * Hex (RGB): <b>#4c516d</b>
	 */
	@ColorInt public static final int INDEPENDENCE = Color.rgb(76, 81, 109);

	/**
	 * Constant value: <b>-15497208 [0xff138808]</b>
	 * <p>
	 * Hex (RGB): <b>#138808</b>
	 */
	@ColorInt public static final int INDIA_GREEN = Color.rgb(19, 136, 8);

	/**
	 * Constant value: <b>-3318692 [0xffcd5c5c]</b>
	 * <p>
	 * Hex (RGB): <b>#cd5c5c</b>
	 */
	@ColorInt public static final int INDIAN_RED = Color.rgb(205, 92, 92);

	/**
	 * Constant value: <b>-1857449 [0xffe3a857]</b>
	 * <p>
	 * Hex (RGB): <b>#e3a857</b>
	 */
	@ColorInt public static final int INDIAN_YELLOW = Color.rgb(227, 168, 87);

	/**
	 * Constant value: <b>-9502465 [0xff6f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#6f00ff</b>
	 */
	@ColorInt public static final int INDIGO = Color.rgb(111, 0, 255);

	/**
	 * Constant value: <b>-16179310 [0xff91f92]</b>
	 * <p>
	 * Hex (RGB): <b>#091f92</b>
	 */
	@ColorInt public static final int INDIGO_DYE = Color.rgb(9, 31, 146);

	/**
	 * Constant value: <b>-11861886 [0xff4b0082]</b>
	 * <p>
	 * Hex (RGB): <b>#4b0082</b>
	 */
	@ColorInt public static final int INDIGO_WEB = Color.rgb(75, 0, 130);

	/**
	 * Constant value: <b>-16765017 [0xff2fa7]</b>
	 * <p>
	 * Hex (RGB): <b>#002fa7</b>
	 */
	@ColorInt public static final int INTERNATIONAL_KLEIN_BLUE = Color.rgb(0, 47, 167);

	/**
	 * Constant value: <b>-45312 [0xffff4f00]</b>
	 * <p>
	 * Hex (RGB): <b>#ff4f00</b>
	 */
	@ColorInt public static final int INTERNATIONAL_ORANGE_AEROSPACE = Color.rgb(255, 79, 0);

	/**
	 * Constant value: <b>-4581876 [0xffba160c]</b>
	 * <p>
	 * Hex (RGB): <b>#ba160c</b>
	 */
	@ColorInt public static final int INTERNATIONAL_ORANGE_ENGINEERING = Color.rgb(186, 22, 12);

	/**
	 * Constant value: <b>-4180436 [0xffc0362c]</b>
	 * <p>
	 * Hex (RGB): <b>#c0362c</b>
	 */
	@ColorInt public static final int INTERNATIONAL_ORANGE_GOLDEN_GATE_BRIDGE = Color.rgb(192, 54, 44);

	/**
	 * Constant value: <b>-10858545 [0xff5a4fcf]</b>
	 * <p>
	 * Hex (RGB): <b>#5a4fcf</b>
	 */
	@ColorInt public static final int IRIS = Color.rgb(90, 79, 207);

	/**
	 * Constant value: <b>-5028756 [0xffb3446c]</b>
	 * <p>
	 * Hex (RGB): <b>#b3446c</b>
	 */
	@ColorInt public static final int IRRESISTIBLE = Color.rgb(179, 68, 108);

	/**
	 * Constant value: <b>-724756 [0xfff4f0ec]</b>
	 * <p>
	 * Hex (RGB): <b>#f4f0ec</b>
	 */
	@ColorInt public static final int ISABELLINE = Color.rgb(244, 240, 236);

	/**
	 * Constant value: <b>-16740352 [0xff9000]</b>
	 * <p>
	 * Hex (RGB): <b>#009000</b>
	 */
	@ColorInt public static final int ISLAMIC_GREEN = Color.rgb(0, 144, 0);

	/**
	 * Constant value: <b>-5046273 [0xffb2ffff]</b>
	 * <p>
	 * Hex (RGB): <b>#b2ffff</b>
	 */
	@ColorInt public static final int ITALIAN_SKY_BLUE = Color.rgb(178, 255, 255);

	/**
	 * Constant value: <b>-16 [0xfffffff0]</b>
	 * <p>
	 * Hex (RGB): <b>#fffff0</b>
	 */
	@ColorInt public static final int IVORY = Color.rgb(255, 255, 240);

	/**
	 * Constant value: <b>-16734101 [0xffa86b]</b>
	 * <p>
	 * Hex (RGB): <b>#00a86b</b>
	 */
	@ColorInt public static final int JADE = Color.rgb(0, 168, 107);

	/**
	 * Constant value: <b>-14269624 [0xff264348]</b>
	 * <p>
	 * Hex (RGB): <b>#264348</b>
	 */
	@ColorInt public static final int JAPANESE_INDIGO = Color.rgb(38, 67, 72);

	/**
	 * Constant value: <b>-10800554 [0xff5b3256]</b>
	 * <p>
	 * Hex (RGB): <b>#5b3256</b>
	 */
	@ColorInt public static final int JAPANESE_VIOLET = Color.rgb(91, 50, 86);

	/**
	 * Constant value: <b>-467330 [0xfff8de7e]</b>
	 * <p>
	 * Hex (RGB): <b>#f8de7e</b>
	 */
	@ColorInt public static final int JASMINE = Color.rgb(248, 222, 126);

	/**
	 * Constant value: <b>-2671810 [0xffd73b3e]</b>
	 * <p>
	 * Hex (RGB): <b>#d73b3e</b>
	 */
	@ColorInt public static final int JASPER = Color.rgb(215, 59, 62);

	/**
	 * Constant value: <b>-5960866 [0xffa50b5e]</b>
	 * <p>
	 * Hex (RGB): <b>#a50b5e</b>
	 */
	@ColorInt public static final int JAZZBERRY_JAM = Color.rgb(165, 11, 94);

	/**
	 * Constant value: <b>-2465458 [0xffda614e]</b>
	 * <p>
	 * Hex (RGB): <b>#da614e</b>
	 */
	@ColorInt public static final int JELLY_BEAN = Color.rgb(218, 97, 78);

	/**
	 * Constant value: <b>-13355980 [0xff343434]</b>
	 * <p>
	 * Hex (RGB): <b>#343434</b>
	 */
	@ColorInt public static final int JET = Color.rgb(52, 52, 52);

	/**
	 * Constant value: <b>-734698 [0xfff4ca16]</b>
	 * <p>
	 * Hex (RGB): <b>#f4ca16</b>
	 */
	@ColorInt public static final int JONQUIL = Color.rgb(244, 202, 22);

	/**
	 * Constant value: <b>-4335017 [0xffbdda57]</b>
	 * <p>
	 * Hex (RGB): <b>#bdda57</b>
	 */
	@ColorInt public static final int JUNE_BUD = Color.rgb(189, 218, 87);

	/**
	 * Constant value: <b>-14046329 [0xff29ab87]</b>
	 * <p>
	 * Hex (RGB): <b>#29ab87</b>
	 */
	@ColorInt public static final int JUNGLE_GREEN = Color.rgb(41, 171, 135);

	/**
	 * Constant value: <b>-11748585 [0xff4cbb17]</b>
	 * <p>
	 * Hex (RGB): <b>#4cbb17</b>
	 */
	@ColorInt public static final int KELLY_GREEN = Color.rgb(76, 187, 23);

	/**
	 * Constant value: <b>-8643579 [0xff7c1c05]</b>
	 * <p>
	 * Hex (RGB): <b>#7c1c05</b>
	 */
	@ColorInt public static final int KENYAN_COPPER = Color.rgb(124, 28, 5);

	/**
	 * Constant value: <b>-12930914 [0xff3ab09e]</b>
	 * <p>
	 * Hex (RGB): <b>#3ab09e</b>
	 */
	@ColorInt public static final int KEPPEL = Color.rgb(58, 176, 158);

	/**
	 * Constant value: <b>-3952495 [0xffc3b091]</b>
	 * <p>
	 * Hex (RGB): <b>#c3b091</b>
	 */
	@ColorInt public static final int KHAKI_HTML_CSS_KHAKI = Color.rgb(195, 176, 145);

	/**
	 * Constant value: <b>-989556 [0xfff0e68c]</b>
	 * <p>
	 * Hex (RGB): <b>#f0e68c</b>
	 */
	@ColorInt public static final int KHAKI_X11_LIGHT_KHAKI = Color.rgb(240, 230, 140);

	/**
	 * Constant value: <b>-7852777 [0xff882d17]</b>
	 * <p>
	 * Hex (RGB): <b>#882d17</b>
	 */
	@ColorInt public static final int KOBE = Color.rgb(136, 45, 23);

	/**
	 * Constant value: <b>-1597500 [0xffe79fc4]</b>
	 * <p>
	 * Hex (RGB): <b>#e79fc4</b>
	 */
	@ColorInt public static final int KOBI = Color.rgb(231, 159, 196);

	/**
	 * Constant value: <b>-13286864 [0xff354230]</b>
	 * <p>
	 * Hex (RGB): <b>#354230</b>
	 */
	@ColorInt public static final int KOMBU_GREEN = Color.rgb(53, 66, 48);

	/**
	 * Constant value: <b>-1572851 [0xffe8000d]</b>
	 * <p>
	 * Hex (RGB): <b>#e8000d</b>
	 */
	@ColorInt public static final int KU_CRIMSON = Color.rgb(232, 0, 13);

	/**
	 * Constant value: <b>-16222160 [0xff87830]</b>
	 * <p>
	 * Hex (RGB): <b>#087830</b>
	 */
	@ColorInt public static final int LA_SALLE_GREEN = Color.rgb(8, 120, 48);

	/**
	 * Constant value: <b>-2700579 [0xffd6cadd]</b>
	 * <p>
	 * Hex (RGB): <b>#d6cadd</b>
	 */
	@ColorInt public static final int LANGUID_LAVENDER = Color.rgb(214, 202, 221);

	/**
	 * Constant value: <b>-14261860 [0xff26619c]</b>
	 * <p>
	 * Hex (RGB): <b>#26619c</b>
	 */
	@ColorInt public static final int LAPIS_LAZULI = Color.rgb(38, 97, 156);

	/**
	 * Constant value: <b>-154 [0xffffff66]</b>
	 * <p>
	 * Hex (RGB): <b>#ffff66</b>
	 */
	@ColorInt public static final int LASER_LEMON = Color.rgb(255, 255, 102);

	/**
	 * Constant value: <b>-5653859 [0xffa9ba9d]</b>
	 * <p>
	 * Hex (RGB): <b>#a9ba9d</b>
	 */
	@ColorInt public static final int LAUREL_GREEN = Color.rgb(169, 186, 157);

	/**
	 * Constant value: <b>-3207136 [0xffcf1020]</b>
	 * <p>
	 * Hex (RGB): <b>#cf1020</b>
	 */
	@ColorInt public static final int LAVA = Color.rgb(207, 16, 32);

	/**
	 * Constant value: <b>-4882724 [0xffb57edc]</b>
	 * <p>
	 * Hex (RGB): <b>#b57edc</b>
	 */
	@ColorInt public static final int LAVENDER_FLORAL = Color.rgb(181, 126, 220);

	/**
	 * Constant value: <b>-1644806 [0xffe6e6fa]</b>
	 * <p>
	 * Hex (RGB): <b>#e6e6fa</b>
	 */
	@ColorInt public static final int LAVENDER_WEB = Color.rgb(230, 230, 250);

	/**
	 * Constant value: <b>-3355393 [0xffccccff]</b>
	 * <p>
	 * Hex (RGB): <b>#ccccff</b>
	 */
	@ColorInt public static final int LAVENDER_BLUE = Color.rgb(204, 204, 255);

	/**
	 * Constant value: <b>-3851 [0xfffff0f5]</b>
	 * <p>
	 * Hex (RGB): <b>#fff0f5</b>
	 */
	@ColorInt public static final int LAVENDER_BLUSH = Color.rgb(255, 240, 245);

	/**
	 * Constant value: <b>-3882032 [0xffc4c3d0]</b>
	 * <p>
	 * Hex (RGB): <b>#c4c3d0</b>
	 */
	@ColorInt public static final int LAVENDER_GRAY = Color.rgb(196, 195, 208);

	/**
	 * Constant value: <b>-7055381 [0xff9457eb]</b>
	 * <p>
	 * Hex (RGB): <b>#9457eb</b>
	 */
	@ColorInt public static final int LAVENDER_INDIGO = Color.rgb(148, 87, 235);

	/**
	 * Constant value: <b>-1146130 [0xffee82ee]</b>
	 * <p>
	 * Hex (RGB): <b>#ee82ee</b>
	 */
	@ColorInt public static final int LAVENDER_MAGENTA = Color.rgb(238, 130, 238);

	/**
	 * Constant value: <b>-1644806 [0xffe6e6fa]</b>
	 * <p>
	 * Hex (RGB): <b>#e6e6fa</b>
	 */
	@ColorInt public static final int LAVENDER_MIST = Color.rgb(230, 230, 250);

	/**
	 * Constant value: <b>-282926 [0xfffbaed2]</b>
	 * <p>
	 * Hex (RGB): <b>#fbaed2</b>
	 */
	@ColorInt public static final int LAVENDER_PINK = Color.rgb(251, 174, 210);

	/**
	 * Constant value: <b>-6915146 [0xff967bb6]</b>
	 * <p>
	 * Hex (RGB): <b>#967bb6</b>
	 */
	@ColorInt public static final int LAVENDER_PURPLE = Color.rgb(150, 123, 182);

	/**
	 * Constant value: <b>-286493 [0xfffba0e3]</b>
	 * <p>
	 * Hex (RGB): <b>#fba0e3</b>
	 */
	@ColorInt public static final int LAVENDER_ROSE = Color.rgb(251, 160, 227);

	/**
	 * Constant value: <b>-8586240 [0xff7cfc00]</b>
	 * <p>
	 * Hex (RGB): <b>#7cfc00</b>
	 */
	@ColorInt public static final int LAWN_GREEN = Color.rgb(124, 252, 0);

	/**
	 * Constant value: <b>-2304 [0xfffff700]</b>
	 * <p>
	 * Hex (RGB): <b>#fff700</b>
	 */
	@ColorInt public static final int LEMON = Color.rgb(255, 247, 0);

	/**
	 * Constant value: <b>-1331 [0xfffffacd]</b>
	 * <p>
	 * Hex (RGB): <b>#fffacd</b>
	 */
	@ColorInt public static final int LEMON_CHIFFON = Color.rgb(255, 250, 205);

	/**
	 * Constant value: <b>-3366883 [0xffcca01d]</b>
	 * <p>
	 * Hex (RGB): <b>#cca01d</b>
	 */
	@ColorInt public static final int LEMON_CURRY = Color.rgb(204, 160, 29);

	/**
	 * Constant value: <b>-131328 [0xfffdff00]</b>
	 * <p>
	 * Hex (RGB): <b>#fdff00</b>
	 */
	@ColorInt public static final int LEMON_GLACIER = Color.rgb(253, 255, 0);

	/**
	 * Constant value: <b>-1835264 [0xffe3ff00]</b>
	 * <p>
	 * Hex (RGB): <b>#e3ff00</b>
	 */
	@ColorInt public static final int LEMON_LIME = Color.rgb(227, 255, 0);

	/**
	 * Constant value: <b>-595266 [0xfff6eabe]</b>
	 * <p>
	 * Hex (RGB): <b>#f6eabe</b>
	 */
	@ColorInt public static final int LEMON_MERINGUE = Color.rgb(246, 234, 190);

	/**
	 * Constant value: <b>-2993 [0xfffff44f]</b>
	 * <p>
	 * Hex (RGB): <b>#fff44f</b>
	 */
	@ColorInt public static final int LEMON_YELLOW = Color.rgb(255, 244, 79);

	/**
	 * Constant value: <b>-15068912 [0xff1a1110]</b>
	 * <p>
	 * Hex (RGB): <b>#1a1110</b>
	 */
	@ColorInt public static final int LICORICE = Color.rgb(26, 17, 16);

	/**
	 * Constant value: <b>-11248985 [0xff545aa7]</b>
	 * <p>
	 * Hex (RGB): <b>#545aa7</b>
	 */
	@ColorInt public static final int LIBERTY = Color.rgb(84, 90, 167);

	/**
	 * Constant value: <b>-141903 [0xfffdd5b1]</b>
	 * <p>
	 * Hex (RGB): <b>#fdd5b1</b>
	 */
	@ColorInt public static final int LIGHT_APRICOT = Color.rgb(253, 213, 177);

	/**
	 * Constant value: <b>-5383962 [0xffadd8e6]</b>
	 * <p>
	 * Hex (RGB): <b>#add8e6</b>
	 */
	@ColorInt public static final int LIGHT_BLUE = Color.rgb(173, 216, 230);

	/**
	 * Constant value: <b>-4889315 [0xffb5651d]</b>
	 * <p>
	 * Hex (RGB): <b>#b5651d</b>
	 */
	@ColorInt public static final int LIGHT_BROWN = Color.rgb(181, 101, 29);

	/**
	 * Constant value: <b>-1677455 [0xffe66771]</b>
	 * <p>
	 * Hex (RGB): <b>#e66771</b>
	 */
	@ColorInt public static final int LIGHT_CARMINE_PINK = Color.rgb(230, 103, 113);

	/**
	 * Constant value: <b>-1015680 [0xfff08080]</b>
	 * <p>
	 * Hex (RGB): <b>#f08080</b>
	 */
	@ColorInt public static final int LIGHT_CORAL = Color.rgb(240, 128, 128);

	/**
	 * Constant value: <b>-7090966 [0xff93ccea]</b>
	 * <p>
	 * Hex (RGB): <b>#93ccea</b>
	 */
	@ColorInt public static final int LIGHT_CORNFLOWER_BLUE = Color.rgb(147, 204, 234);

	/**
	 * Constant value: <b>-693871 [0xfff56991]</b>
	 * <p>
	 * Hex (RGB): <b>#f56991</b>
	 */
	@ColorInt public static final int LIGHT_CRIMSON = Color.rgb(245, 105, 145);

	/**
	 * Constant value: <b>-2031617 [0xffe0ffff]</b>
	 * <p>
	 * Hex (RGB): <b>#e0ffff</b>
	 */
	@ColorInt public static final int LIGHT_CYAN = Color.rgb(224, 255, 255);

	/**
	 * Constant value: <b>-424721 [0xfff984ef]</b>
	 * <p>
	 * Hex (RGB): <b>#f984ef</b>
	 */
	@ColorInt public static final int LIGHT_FUCHSIA_PINK = Color.rgb(249, 132, 239);

	/**
	 * Constant value: <b>-329006 [0xfffafad2]</b>
	 * <p>
	 * Hex (RGB): <b>#fafad2</b>
	 */
	@ColorInt public static final int LIGHT_GOLDENROD_YELLOW = Color.rgb(250, 250, 210);

	/**
	 * Constant value: <b>-2894893 [0xffd3d3d3]</b>
	 * <p>
	 * Hex (RGB): <b>#d3d3d3</b>
	 */
	@ColorInt public static final int LIGHT_GRAY = Color.rgb(211, 211, 211);

	/**
	 * Constant value: <b>-7278960 [0xff90ee90]</b>
	 * <p>
	 * Hex (RGB): <b>#90ee90</b>
	 */
	@ColorInt public static final int LIGHT_GREEN = Color.rgb(144, 238, 144);

	/**
	 * Constant value: <b>-989556 [0xfff0e68c]</b>
	 * <p>
	 * Hex (RGB): <b>#f0e68c</b>
	 */
	@ColorInt public static final int LIGHT_KHAKI = Color.rgb(240, 230, 140);

	/**
	 * Constant value: <b>-2909237 [0xffd39bcb]</b>
	 * <p>
	 * Hex (RGB): <b>#d39bcb</b>
	 */
	@ColorInt public static final int LIGHT_MEDIUM_ORCHID = Color.rgb(211, 155, 203);

	/**
	 * Constant value: <b>-5388115 [0xffadc8ad]</b>
	 * <p>
	 * Hex (RGB): <b>#adc8ad</b>
	 */
	@ColorInt public static final int LIGHT_MOSS_GREEN = Color.rgb(173, 200, 173);

	/**
	 * Constant value: <b>-1660713 [0xffe6a8d7]</b>
	 * <p>
	 * Hex (RGB): <b>#e6a8d7</b>
	 */
	@ColorInt public static final int LIGHT_ORCHID = Color.rgb(230, 168, 215);

	/**
	 * Constant value: <b>-5137191 [0xffb19cd9]</b>
	 * <p>
	 * Hex (RGB): <b>#b19cd9</b>
	 */
	@ColorInt public static final int LIGHT_PASTEL_PURPLE = Color.rgb(177, 156, 217);

	/**
	 * Constant value: <b>-18751 [0xffffb6c1]</b>
	 * <p>
	 * Hex (RGB): <b>#ffb6c1</b>
	 */
	@ColorInt public static final int LIGHT_PINK = Color.rgb(255, 182, 193);

	/**
	 * Constant value: <b>-1477551 [0xffe97451]</b>
	 * <p>
	 * Hex (RGB): <b>#e97451</b>
	 */
	@ColorInt public static final int LIGHT_RED_OCHRE = Color.rgb(233, 116, 81);

	/**
	 * Constant value: <b>-24454 [0xffffa07a]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa07a</b>
	 */
	@ColorInt public static final int LIGHT_SALMON = Color.rgb(255, 160, 122);

	/**
	 * Constant value: <b>-26215 [0xffff9999]</b>
	 * <p>
	 * Hex (RGB): <b>#ff9999</b>
	 */
	@ColorInt public static final int LIGHT_SALMON_PINK = Color.rgb(255, 153, 153);

	/**
	 * Constant value: <b>-14634326 [0xff20b2aa]</b>
	 * <p>
	 * Hex (RGB): <b>#20b2aa</b>
	 */
	@ColorInt public static final int LIGHT_SEA_GREEN = Color.rgb(32, 178, 170);

	/**
	 * Constant value: <b>-7876870 [0xff87cefa]</b>
	 * <p>
	 * Hex (RGB): <b>#87cefa</b>
	 */
	@ColorInt public static final int LIGHT_SKY_BLUE = Color.rgb(135, 206, 250);

	/**
	 * Constant value: <b>-8943463 [0xff778899]</b>
	 * <p>
	 * Hex (RGB): <b>#778899</b>
	 */
	@ColorInt public static final int LIGHT_SLATE_GRAY = Color.rgb(119, 136, 153);

	/**
	 * Constant value: <b>-5192482 [0xffb0c4de]</b>
	 * <p>
	 * Hex (RGB): <b>#b0c4de</b>
	 */
	@ColorInt public static final int LIGHT_STEEL_BLUE = Color.rgb(176, 196, 222);

	/**
	 * Constant value: <b>-5010579 [0xffb38b6d]</b>
	 * <p>
	 * Hex (RGB): <b>#b38b6d</b>
	 */
	@ColorInt public static final int LIGHT_TAUPE = Color.rgb(179, 139, 109);

	/**
	 * Constant value: <b>-1667156 [0xffe68fac]</b>
	 * <p>
	 * Hex (RGB): <b>#e68fac</b>
	 */
	@ColorInt public static final int LIGHT_THULIAN_PINK = Color.rgb(230, 143, 172);

	/**
	 * Constant value: <b>-32 [0xffffffe0]</b>
	 * <p>
	 * Hex (RGB): <b>#ffffe0</b>
	 */
	@ColorInt public static final int LIGHT_YELLOW = Color.rgb(255, 255, 224);

	/**
	 * Constant value: <b>-3628344 [0xffc8a2c8]</b>
	 * <p>
	 * Hex (RGB): <b>#c8a2c8</b>
	 */
	@ColorInt public static final int LILAC = Color.rgb(200, 162, 200);

	/**
	 * Constant value: <b>-4194560 [0xffbfff00]</b>
	 * <p>
	 * Hex (RGB): <b>#bfff00</b>
	 */
	@ColorInt public static final int LIME_COLOR_WHEEL = Color.rgb(191, 255, 0);

	/**
	 * Constant value: <b>-16711936 [0xffff00]</b>
	 * <p>
	 * Hex (RGB): <b>#00ff00</b>
	 */
	@ColorInt public static final int LIME_WEB_X11_GREEN = Color.rgb(0, 255, 0);

	/**
	 * Constant value: <b>-13447886 [0xff32cd32]</b>
	 * <p>
	 * Hex (RGB): <b>#32cd32</b>
	 */
	@ColorInt public static final int LIME_GREEN = Color.rgb(50, 205, 50);

	/**
	 * Constant value: <b>-6438391 [0xff9dc209]</b>
	 * <p>
	 * Hex (RGB): <b>#9dc209</b>
	 */
	@ColorInt public static final int LIMERICK = Color.rgb(157, 194, 9);

	/**
	 * Constant value: <b>-15116027 [0xff195905]</b>
	 * <p>
	 * Hex (RGB): <b>#195905</b>
	 */
	@ColorInt public static final int LINCOLN_GREEN = Color.rgb(25, 89, 5);

	/**
	 * Constant value: <b>-331546 [0xfffaf0e6]</b>
	 * <p>
	 * Hex (RGB): <b>#faf0e6</b>
	 */
	@ColorInt public static final int LINEN = Color.rgb(250, 240, 230);

	/**
	 * Constant value: <b>-4089237 [0xffc19a6b]</b>
	 * <p>
	 * Hex (RGB): <b>#c19a6b</b>
	 */
	@ColorInt public static final int LION = Color.rgb(193, 154, 107);

	/**
	 * Constant value: <b>-9658148 [0xff6ca0dc]</b>
	 * <p>
	 * Hex (RGB): <b>#6ca0dc</b>
	 */
	@ColorInt public static final int LITTLE_BOY_BLUE = Color.rgb(108, 160, 220);

	/**
	 * Constant value: <b>-10007481 [0xff674c47]</b>
	 * <p>
	 * Hex (RGB): <b>#674c47</b>
	 */
	@ColorInt public static final int LIVER = Color.rgb(103, 76, 71);

	/**
	 * Constant value: <b>-4690647 [0xffb86d29]</b>
	 * <p>
	 * Hex (RGB): <b>#b86d29</b>
	 */
	@ColorInt public static final int LIVER_DOGS = Color.rgb(184, 109, 41);

	/**
	 * Constant value: <b>-9687521 [0xff6c2e1f]</b>
	 * <p>
	 * Hex (RGB): <b>#6c2e1f</b>
	 */
	@ColorInt public static final int LIVER_ORGAN = Color.rgb(108, 46, 31);

	/**
	 * Constant value: <b>-6785962 [0xff987456]</b>
	 * <p>
	 * Hex (RGB): <b>#987456</b>
	 */
	@ColorInt public static final int LIVER_CHESTNUT = Color.rgb(152, 116, 86);

	/**
	 * Constant value: <b>-6963 [0xffffe4cd]</b>
	 * <p>
	 * Hex (RGB): <b>#ffe4cd</b>
	 */
	@ColorInt public static final int LUMBER = Color.rgb(255, 228, 205);

	/**
	 * Constant value: <b>-1695712 [0xffe62020]</b>
	 * <p>
	 * Hex (RGB): <b>#e62020</b>
	 */
	@ColorInt public static final int LUST = Color.rgb(230, 32, 32);

	/**
	 * Constant value: <b>-65281 [0xffff00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#ff00ff</b>
	 */
	@ColorInt public static final int MAGENTA = Color.rgb(255, 0, 255);

	/**
	 * Constant value: <b>-43613 [0xffff55a3]</b>
	 * <p>
	 * Hex (RGB): <b>#ff55a3</b>
	 */
	@ColorInt public static final int MAGENTA_CRAYOLA = Color.rgb(255, 85, 163);

	/**
	 * Constant value: <b>-3530885 [0xffca1f7b]</b>
	 * <p>
	 * Hex (RGB): <b>#ca1f7b</b>
	 */
	@ColorInt public static final int MAGENTA_DYE = Color.rgb(202, 31, 123);

	/**
	 * Constant value: <b>-3128962 [0xffd0417e]</b>
	 * <p>
	 * Hex (RGB): <b>#d0417e</b>
	 */
	@ColorInt public static final int MAGENTA_PANTONE = Color.rgb(208, 65, 126);

	/**
	 * Constant value: <b>-65392 [0xffff0090]</b>
	 * <p>
	 * Hex (RGB): <b>#ff0090</b>
	 */
	@ColorInt public static final int MAGENTA_PROCESS = Color.rgb(255, 0, 144);

	/**
	 * Constant value: <b>-6339210 [0xff9f4576]</b>
	 * <p>
	 * Hex (RGB): <b>#9f4576</b>
	 */
	@ColorInt public static final int MAGENTA_HAZE = Color.rgb(159, 69, 118);

	/**
	 * Constant value: <b>-5574447 [0xffaaf0d1]</b>
	 * <p>
	 * Hex (RGB): <b>#aaf0d1</b>
	 */
	@ColorInt public static final int MAGIC_MINT = Color.rgb(170, 240, 209);

	/**
	 * Constant value: <b>-461569 [0xfff8f4ff]</b>
	 * <p>
	 * Hex (RGB): <b>#f8f4ff</b>
	 */
	@ColorInt public static final int MAGNOLIA = Color.rgb(248, 244, 255);

	/**
	 * Constant value: <b>-4177920 [0xffc04000]</b>
	 * <p>
	 * Hex (RGB): <b>#c04000</b>
	 */
	@ColorInt public static final int MAHOGANY = Color.rgb(192, 64, 0);

	/**
	 * Constant value: <b>-267171 [0xfffbec5d]</b>
	 * <p>
	 * Hex (RGB): <b>#fbec5d</b>
	 */
	@ColorInt public static final int MAIZE = Color.rgb(251, 236, 93);

	/**
	 * Constant value: <b>-10465060 [0xff6050dc]</b>
	 * <p>
	 * Hex (RGB): <b>#6050dc</b>
	 */
	@ColorInt public static final int MAJORELLE_BLUE = Color.rgb(96, 80, 220);

	/**
	 * Constant value: <b>-16000431 [0xffbda51]</b>
	 * <p>
	 * Hex (RGB): <b>#0bda51</b>
	 */
	@ColorInt public static final int MALACHITE = Color.rgb(11, 218, 81);

	/**
	 * Constant value: <b>-6841686 [0xff979aaa]</b>
	 * <p>
	 * Hex (RGB): <b>#979aaa</b>
	 */
	@ColorInt public static final int MANATEE = Color.rgb(151, 154, 170);

	/**
	 * Constant value: <b>-32189 [0xffff8243]</b>
	 * <p>
	 * Hex (RGB): <b>#ff8243</b>
	 */
	@ColorInt public static final int MANGO_TANGO = Color.rgb(255, 130, 67);

	/**
	 * Constant value: <b>-9125019 [0xff74c365]</b>
	 * <p>
	 * Hex (RGB): <b>#74c365</b>
	 */
	@ColorInt public static final int MANTIS = Color.rgb(116, 195, 101);

	/**
	 * Constant value: <b>-7864183 [0xff880089]</b>
	 * <p>
	 * Hex (RGB): <b>#880089</b>
	 */
	@ColorInt public static final int MARDI_GRAS = Color.rgb(136, 0, 137);

	/**
	 * Constant value: <b>-3989176 [0xffc32148]</b>
	 * <p>
	 * Hex (RGB): <b>#c32148</b>
	 */
	@ColorInt public static final int MAROON_CRAYOLA = Color.rgb(195, 33, 72);

	/**
	 * Constant value: <b>-8388608 [0xff800000]</b>
	 * <p>
	 * Hex (RGB): <b>#800000</b>
	 */
	@ColorInt public static final int MAROON_HTML_CSS = Color.rgb(128, 0, 0);

	/**
	 * Constant value: <b>-5230496 [0xffb03060]</b>
	 * <p>
	 * Hex (RGB): <b>#b03060</b>
	 */
	@ColorInt public static final int MAROON_X11 = Color.rgb(176, 48, 96);

	/**
	 * Constant value: <b>-2051841 [0xffe0b0ff]</b>
	 * <p>
	 * Hex (RGB): <b>#e0b0ff</b>
	 */
	@ColorInt public static final int MAUVE = Color.rgb(224, 176, 255);

	/**
	 * Constant value: <b>-7250067 [0xff915f6d]</b>
	 * <p>
	 * Hex (RGB): <b>#915f6d</b>
	 */
	@ColorInt public static final int MAUVE_TAUPE = Color.rgb(145, 95, 109);

	/**
	 * Constant value: <b>-1075030 [0xffef98aa]</b>
	 * <p>
	 * Hex (RGB): <b>#ef98aa</b>
	 */
	@ColorInt public static final int MAUVELOUS = Color.rgb(239, 152, 170);

	/**
	 * Constant value: <b>-9190661 [0xff73c2fb]</b>
	 * <p>
	 * Hex (RGB): <b>#73c2fb</b>
	 */
	@ColorInt public static final int MAYA_BLUE = Color.rgb(115, 194, 251);

	/**
	 * Constant value: <b>-1722565 [0xffe5b73b]</b>
	 * <p>
	 * Hex (RGB): <b>#e5b73b</b>
	 */
	@ColorInt public static final int MEAT_BROWN = Color.rgb(229, 183, 59);

	/**
	 * Constant value: <b>-10035798 [0xff66ddaa]</b>
	 * <p>
	 * Hex (RGB): <b>#66ddaa</b>
	 */
	@ColorInt public static final int MEDIUM_AQUAMARINE = Color.rgb(102, 221, 170);

	/**
	 * Constant value: <b>-16777011 [0xffcd]</b>
	 * <p>
	 * Hex (RGB): <b>#0000cd</b>
	 */
	@ColorInt public static final int MEDIUM_BLUE = Color.rgb(0, 0, 205);

	/**
	 * Constant value: <b>-1964500 [0xffe2062c]</b>
	 * <p>
	 * Hex (RGB): <b>#e2062c</b>
	 */
	@ColorInt public static final int MEDIUM_CANDY_APPLE_RED = Color.rgb(226, 6, 44);

	/**
	 * Constant value: <b>-5291979 [0xffaf4035]</b>
	 * <p>
	 * Hex (RGB): <b>#af4035</b>
	 */
	@ColorInt public static final int MEDIUM_CARMINE = Color.rgb(175, 64, 53);

	/**
	 * Constant value: <b>-793173 [0xfff3e5ab]</b>
	 * <p>
	 * Hex (RGB): <b>#f3e5ab</b>
	 */
	@ColorInt public static final int MEDIUM_CHAMPAGNE = Color.rgb(243, 229, 171);

	/**
	 * Constant value: <b>-16559978 [0xff35096]</b>
	 * <p>
	 * Hex (RGB): <b>#035096</b>
	 */
	@ColorInt public static final int MEDIUM_ELECTRIC_BLUE = Color.rgb(3, 80, 150);

	/**
	 * Constant value: <b>-14928595 [0xff1c352d]</b>
	 * <p>
	 * Hex (RGB): <b>#1c352d</b>
	 */
	@ColorInt public static final int MEDIUM_JUNGLE_GREEN = Color.rgb(28, 53, 45);

	/**
	 * Constant value: <b>-2252579 [0xffdda0dd]</b>
	 * <p>
	 * Hex (RGB): <b>#dda0dd</b>
	 */
	@ColorInt public static final int MEDIUM_LAVENDER_MAGENTA = Color.rgb(221, 160, 221);

	/**
	 * Constant value: <b>-4565549 [0xffba55d3]</b>
	 * <p>
	 * Hex (RGB): <b>#ba55d3</b>
	 */
	@ColorInt public static final int MEDIUM_ORCHID = Color.rgb(186, 85, 211);

	/**
	 * Constant value: <b>-16750683 [0xff67a5]</b>
	 * <p>
	 * Hex (RGB): <b>#0067a5</b>
	 */
	@ColorInt public static final int MEDIUM_PERSIAN_BLUE = Color.rgb(0, 103, 165);

	/**
	 * Constant value: <b>-7114533 [0xff9370db]</b>
	 * <p>
	 * Hex (RGB): <b>#9370db</b>
	 */
	@ColorInt public static final int MEDIUM_PURPLE = Color.rgb(147, 112, 219);

	/**
	 * Constant value: <b>-4508795 [0xffbb3385]</b>
	 * <p>
	 * Hex (RGB): <b>#bb3385</b>
	 */
	@ColorInt public static final int MEDIUM_RED_VIOLET = Color.rgb(187, 51, 133);

	/**
	 * Constant value: <b>-5619607 [0xffaa4069]</b>
	 * <p>
	 * Hex (RGB): <b>#aa4069</b>
	 */
	@ColorInt public static final int MEDIUM_RUBY = Color.rgb(170, 64, 105);

	/**
	 * Constant value: <b>-12799119 [0xff3cb371]</b>
	 * <p>
	 * Hex (RGB): <b>#3cb371</b>
	 */
	@ColorInt public static final int MEDIUM_SEA_GREEN = Color.rgb(60, 179, 113);

	/**
	 * Constant value: <b>-8332565 [0xff80daeb]</b>
	 * <p>
	 * Hex (RGB): <b>#80daeb</b>
	 */
	@ColorInt public static final int MEDIUM_SKY_BLUE = Color.rgb(128, 218, 235);

	/**
	 * Constant value: <b>-8689426 [0xff7b68ee]</b>
	 * <p>
	 * Hex (RGB): <b>#7b68ee</b>
	 */
	@ColorInt public static final int MEDIUM_SLATE_BLUE = Color.rgb(123, 104, 238);

	/**
	 * Constant value: <b>-3548025 [0xffc9dc87]</b>
	 * <p>
	 * Hex (RGB): <b>#c9dc87</b>
	 */
	@ColorInt public static final int MEDIUM_SPRING_BUD = Color.rgb(201, 220, 135);

	/**
	 * Constant value: <b>-16713062 [0xfffa9a]</b>
	 * <p>
	 * Hex (RGB): <b>#00fa9a</b>
	 */
	@ColorInt public static final int MEDIUM_SPRING_GREEN = Color.rgb(0, 250, 154);

	/**
	 * Constant value: <b>-10007481 [0xff674c47]</b>
	 * <p>
	 * Hex (RGB): <b>#674c47</b>
	 */
	@ColorInt public static final int MEDIUM_TAUPE = Color.rgb(103, 76, 71);

	/**
	 * Constant value: <b>-12004916 [0xff48d1cc]</b>
	 * <p>
	 * Hex (RGB): <b>#48d1cc</b>
	 */
	@ColorInt public static final int MEDIUM_TURQUOISE = Color.rgb(72, 209, 204);

	/**
	 * Constant value: <b>-8829893 [0xff79443b]</b>
	 * <p>
	 * Hex (RGB): <b>#79443b</b>
	 */
	@ColorInt public static final int MEDIUM_TUSCAN_RED = Color.rgb(121, 68, 59);

	/**
	 * Constant value: <b>-2531269 [0xffd9603b]</b>
	 * <p>
	 * Hex (RGB): <b>#d9603b</b>
	 */
	@ColorInt public static final int MEDIUM_VERMILION = Color.rgb(217, 96, 59);

	/**
	 * Constant value: <b>-3730043 [0xffc71585]</b>
	 * <p>
	 * Hex (RGB): <b>#c71585</b>
	 */
	@ColorInt public static final int MEDIUM_VIOLET_RED = Color.rgb(199, 21, 133);

	/**
	 * Constant value: <b>-477064 [0xfff8b878]</b>
	 * <p>
	 * Hex (RGB): <b>#f8b878</b>
	 */
	@ColorInt public static final int MELLOW_APRICOT = Color.rgb(248, 184, 120);

	/**
	 * Constant value: <b>-467330 [0xfff8de7e]</b>
	 * <p>
	 * Hex (RGB): <b>#f8de7e</b>
	 */
	@ColorInt public static final int MELLOW_YELLOW = Color.rgb(248, 222, 126);

	/**
	 * Constant value: <b>-148300 [0xfffdbcb4]</b>
	 * <p>
	 * Hex (RGB): <b>#fdbcb4</b>
	 */
	@ColorInt public static final int MELON = Color.rgb(253, 188, 180);

	/**
	 * Constant value: <b>-16220532 [0xff87e8c]</b>
	 * <p>
	 * Hex (RGB): <b>#087e8c</b>
	 */
	@ColorInt public static final int METALLIC_SEAWEED = Color.rgb(8, 126, 140);

	/**
	 * Constant value: <b>-6521800 [0xff9c7c38]</b>
	 * <p>
	 * Hex (RGB): <b>#9c7c38</b>
	 */
	@ColorInt public static final int METALLIC_SUNBURST = Color.rgb(156, 124, 56);

	/**
	 * Constant value: <b>-1834884 [0xffe4007c]</b>
	 * <p>
	 * Hex (RGB): <b>#e4007c</b>
	 */
	@ColorInt public static final int MEXICAN_PINK = Color.rgb(228, 0, 124);

	/**
	 * Constant value: <b>-15132304 [0xff191970]</b>
	 * <p>
	 * Hex (RGB): <b>#191970</b>
	 */
	@ColorInt public static final int MIDNIGHT_BLUE = Color.rgb(25, 25, 112);

	/**
	 * Constant value: <b>-16758445 [0xff4953]</b>
	 * <p>
	 * Hex (RGB): <b>#004953</b>
	 */
	@ColorInt public static final int MIDNIGHT_GREEN_EAGLE_GREEN = Color.rgb(0, 73, 83);

	/**
	 * Constant value: <b>-1836664 [0xffe3f988]</b>
	 * <p>
	 * Hex (RGB): <b>#e3f988</b>
	 */
	@ColorInt public static final int MIDORI = Color.rgb(227, 249, 136);

	/**
	 * Constant value: <b>-15348 [0xffffc40c]</b>
	 * <p>
	 * Hex (RGB): <b>#ffc40c</b>
	 */
	@ColorInt public static final int MIKADO_YELLOW = Color.rgb(255, 196, 12);

	/**
	 * Constant value: <b>-12667767 [0xff3eb489]</b>
	 * <p>
	 * Hex (RGB): <b>#3eb489</b>
	 */
	@ColorInt public static final int MINT = Color.rgb(62, 180, 137);

	/**
	 * Constant value: <b>-655366 [0xfff5fffa]</b>
	 * <p>
	 * Hex (RGB): <b>#f5fffa</b>
	 */
	@ColorInt public static final int MINT_CREAM = Color.rgb(245, 255, 250);

	/**
	 * Constant value: <b>-6750312 [0xff98ff98]</b>
	 * <p>
	 * Hex (RGB): <b>#98ff98</b>
	 */
	@ColorInt public static final int MINT_GREEN = Color.rgb(152, 255, 152);

	/**
	 * Constant value: <b>-6943 [0xffffe4e1]</b>
	 * <p>
	 * Hex (RGB): <b>#ffe4e1</b>
	 */
	@ColorInt public static final int MISTY_ROSE = Color.rgb(255, 228, 225);

	/**
	 * Constant value: <b>-332841 [0xfffaebd7]</b>
	 * <p>
	 * Hex (RGB): <b>#faebd7</b>
	 */
	@ColorInt public static final int MOCCASIN = Color.rgb(250, 235, 215);

	/**
	 * Constant value: <b>-6917865 [0xff967117]</b>
	 * <p>
	 * Hex (RGB): <b>#967117</b>
	 */
	@ColorInt public static final int MODE_BEIGE = Color.rgb(150, 113, 23);

	/**
	 * Constant value: <b>-9197118 [0xff73a9c2]</b>
	 * <p>
	 * Hex (RGB): <b>#73a9c2</b>
	 */
	@ColorInt public static final int MOONSTONE_BLUE = Color.rgb(115, 169, 194);

	/**
	 * Constant value: <b>-5370880 [0xffae0c00]</b>
	 * <p>
	 * Hex (RGB): <b>#ae0c00</b>
	 */
	@ColorInt public static final int MORDANT_RED_19 = Color.rgb(174, 12, 0);

	/**
	 * Constant value: <b>-7693733 [0xff8a9a5b]</b>
	 * <p>
	 * Hex (RGB): <b>#8a9a5b</b>
	 */
	@ColorInt public static final int MOSS_GREEN = Color.rgb(138, 154, 91);

	/**
	 * Constant value: <b>-13583729 [0xff30ba8f]</b>
	 * <p>
	 * Hex (RGB): <b>#30ba8f</b>
	 */
	@ColorInt public static final int MOUNTAIN_MEADOW = Color.rgb(48, 186, 143);

	/**
	 * Constant value: <b>-6718835 [0xff997a8d]</b>
	 * <p>
	 * Hex (RGB): <b>#997a8d</b>
	 */
	@ColorInt public static final int MOUNTBATTEN_PINK = Color.rgb(153, 122, 141);

	/**
	 * Constant value: <b>-15186629 [0xff18453b]</b>
	 * <p>
	 * Hex (RGB): <b>#18453b</b>
	 */
	@ColorInt public static final int MSU_GREEN = Color.rgb(24, 69, 59);

	/**
	 * Constant value: <b>-13606864 [0xff306030]</b>
	 * <p>
	 * Hex (RGB): <b>#306030</b>
	 */
	@ColorInt public static final int MUGHAL_GREEN = Color.rgb(48, 96, 48);

	/**
	 * Constant value: <b>-3847284 [0xffc54b8c]</b>
	 * <p>
	 * Hex (RGB): <b>#c54b8c</b>
	 */
	@ColorInt public static final int MULBERRY = Color.rgb(197, 75, 140);

	/**
	 * Constant value: <b>-9384 [0xffffdb58]</b>
	 * <p>
	 * Hex (RGB): <b>#ffdb58</b>
	 */
	@ColorInt public static final int MUSTARD = Color.rgb(255, 219, 88);

	/**
	 * Constant value: <b>-13535117 [0xff317873]</b>
	 * <p>
	 * Hex (RGB): <b>#317873</b>
	 */
	@ColorInt public static final int MYRTLE_GREEN = Color.rgb(49, 120, 115);

	/**
	 * Constant value: <b>-610874 [0xfff6adc6]</b>
	 * <p>
	 * Hex (RGB): <b>#f6adc6</b>
	 */
	@ColorInt public static final int NADESHIKO_PINK = Color.rgb(246, 173, 198);

	/**
	 * Constant value: <b>-13991936 [0xff2a8000]</b>
	 * <p>
	 * Hex (RGB): <b>#2a8000</b>
	 */
	@ColorInt public static final int NAPIER_GREEN = Color.rgb(42, 128, 0);

	/**
	 * Constant value: <b>-337314 [0xfffada5e]</b>
	 * <p>
	 * Hex (RGB): <b>#fada5e</b>
	 */
	@ColorInt public static final int NAPLES_YELLOW = Color.rgb(250, 218, 94);

	/**
	 * Constant value: <b>-8531 [0xffffdead]</b>
	 * <p>
	 * Hex (RGB): <b>#ffdead</b>
	 */
	@ColorInt public static final int NAVAJO_WHITE = Color.rgb(255, 222, 173);

	/**
	 * Constant value: <b>-16777088 [0xff80]</b>
	 * <p>
	 * Hex (RGB): <b>#000080</b>
	 */
	@ColorInt public static final int NAVY_BLUE = Color.rgb(0, 0, 128);

	/**
	 * Constant value: <b>-7055381 [0xff9457eb]</b>
	 * <p>
	 * Hex (RGB): <b>#9457eb</b>
	 */
	@ColorInt public static final int NAVY_PURPLE = Color.rgb(148, 87, 235);

	/**
	 * Constant value: <b>-23741 [0xffffa343]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa343</b>
	 */
	@ColorInt public static final int NEON_CARROT = Color.rgb(255, 163, 67);

	/**
	 * Constant value: <b>-114332 [0xfffe4164]</b>
	 * <p>
	 * Hex (RGB): <b>#fe4164</b>
	 */
	@ColorInt public static final int NEON_FUCHSIA = Color.rgb(254, 65, 100);

	/**
	 * Constant value: <b>-12976364 [0xff39ff14]</b>
	 * <p>
	 * Hex (RGB): <b>#39ff14</b>
	 */
	@ColorInt public static final int NEON_GREEN = Color.rgb(57, 255, 20);

	/**
	 * Constant value: <b>-14594106 [0xff214fc6]</b>
	 * <p>
	 * Hex (RGB): <b>#214fc6</b>
	 */
	@ColorInt public static final int NEW_CAR = Color.rgb(33, 79, 198);

	/**
	 * Constant value: <b>-2653313 [0xffd7837f]</b>
	 * <p>
	 * Hex (RGB): <b>#d7837f</b>
	 */
	@ColorInt public static final int NEW_YORK_PINK = Color.rgb(215, 131, 127);

	/**
	 * Constant value: <b>-5972499 [0xffa4dded]</b>
	 * <p>
	 * Hex (RGB): <b>#a4dded</b>
	 */
	@ColorInt public static final int NON_PHOTO_BLUE = Color.rgb(164, 221, 237);

	/**
	 * Constant value: <b>-16412621 [0xff59033]</b>
	 * <p>
	 * Hex (RGB): <b>#059033</b>
	 */
	@ColorInt public static final int NORTH_TEXAS_GREEN = Color.rgb(5, 144, 51);

	/**
	 * Constant value: <b>-1441829 [0xffe9ffdb]</b>
	 * <p>
	 * Hex (RGB): <b>#e9ffdb</b>
	 */
	@ColorInt public static final int NYANZA = Color.rgb(233, 255, 219);

	/**
	 * Constant value: <b>-16746562 [0xff77be]</b>
	 * <p>
	 * Hex (RGB): <b>#0077be</b>
	 */
	@ColorInt public static final int OCEAN_BOAT_BLUE = Color.rgb(0, 119, 190);

	/**
	 * Constant value: <b>-3377374 [0xffcc7722]</b>
	 * <p>
	 * Hex (RGB): <b>#cc7722</b>
	 */
	@ColorInt public static final int OCHRE = Color.rgb(204, 119, 34);

	/**
	 * Constant value: <b>-16744448 [0xff8000]</b>
	 * <p>
	 * Hex (RGB): <b>#008000</b>
	 */
	@ColorInt public static final int OFFICE_GREEN = Color.rgb(0, 128, 0);

	/**
	 * Constant value: <b>-12373970 [0xff43302e]</b>
	 * <p>
	 * Hex (RGB): <b>#43302e</b>
	 */
	@ColorInt public static final int OLD_BURGUNDY = Color.rgb(67, 48, 46);

	/**
	 * Constant value: <b>-3164869 [0xffcfb53b]</b>
	 * <p>
	 * Hex (RGB): <b>#cfb53b</b>
	 */
	@ColorInt public static final int OLD_GOLD = Color.rgb(207, 181, 59);

	/**
	 * Constant value: <b>-133658 [0xfffdf5e6]</b>
	 * <p>
	 * Hex (RGB): <b>#fdf5e6</b>
	 */
	@ColorInt public static final int OLD_LACE = Color.rgb(253, 245, 230);

	/**
	 * Constant value: <b>-8820616 [0xff796878]</b>
	 * <p>
	 * Hex (RGB): <b>#796878</b>
	 */
	@ColorInt public static final int OLD_LAVENDER = Color.rgb(121, 104, 120);

	/**
	 * Constant value: <b>-10014393 [0xff673147]</b>
	 * <p>
	 * Hex (RGB): <b>#673147</b>
	 */
	@ColorInt public static final int OLD_MAUVE = Color.rgb(103, 49, 71);

	/**
	 * Constant value: <b>-7963082 [0xff867e36]</b>
	 * <p>
	 * Hex (RGB): <b>#867e36</b>
	 */
	@ColorInt public static final int OLD_MOSS_GREEN = Color.rgb(134, 126, 54);

	/**
	 * Constant value: <b>-4161407 [0xffc08081]</b>
	 * <p>
	 * Hex (RGB): <b>#c08081</b>
	 */
	@ColorInt public static final int OLD_ROSE = Color.rgb(192, 128, 129);

	/**
	 * Constant value: <b>-8092542 [0xff848482]</b>
	 * <p>
	 * Hex (RGB): <b>#848482</b>
	 */
	@ColorInt public static final int OLD_SILVER = Color.rgb(132, 132, 130);

	/**
	 * Constant value: <b>-8355840 [0xff808000]</b>
	 * <p>
	 * Hex (RGB): <b>#808000</b>
	 */
	@ColorInt public static final int OLIVE = Color.rgb(128, 128, 0);

	/**
	 * Constant value: <b>-9728477 [0xff6b8e23]</b>
	 * <p>
	 * Hex (RGB): <b>#6b8e23</b>
	 */
	@ColorInt public static final int OLIVE_DRAB_WEB_OLIVE_DRAB_3 = Color.rgb(107, 142, 35);

	/**
	 * Constant value: <b>-12831713 [0xff3c341f]</b>
	 * <p>
	 * Hex (RGB): <b>#3c341f</b>
	 */
	@ColorInt public static final int OLIVE_DRAB_7 = Color.rgb(60, 52, 31);

	/**
	 * Constant value: <b>-6637197 [0xff9ab973]</b>
	 * <p>
	 * Hex (RGB): <b>#9ab973</b>
	 */
	@ColorInt public static final int OLIVINE = Color.rgb(154, 185, 115);

	/**
	 * Constant value: <b>-13289415 [0xff353839]</b>
	 * <p>
	 * Hex (RGB): <b>#353839</b>
	 */
	@ColorInt public static final int ONYX = Color.rgb(53, 56, 57);

	/**
	 * Constant value: <b>-4750169 [0xffb784a7]</b>
	 * <p>
	 * Hex (RGB): <b>#b784a7</b>
	 */
	@ColorInt public static final int OPERA_MAUVE = Color.rgb(183, 132, 167);

	/**
	 * Constant value: <b>-33024 [0xffff7f00]</b>
	 * <p>
	 * Hex (RGB): <b>#ff7f00</b>
	 */
	@ColorInt public static final int ORANGE_COLOR_WHEEL = Color.rgb(255, 127, 0);

	/**
	 * Constant value: <b>-35528 [0xffff7538]</b>
	 * <p>
	 * Hex (RGB): <b>#ff7538</b>
	 */
	@ColorInt public static final int ORANGE_CRAYOLA = Color.rgb(255, 117, 56);

	/**
	 * Constant value: <b>-43008 [0xffff5800]</b>
	 * <p>
	 * Hex (RGB): <b>#ff5800</b>
	 */
	@ColorInt public static final int ORANGE_PANTONE = Color.rgb(255, 88, 00);

	/**
	 * Constant value: <b>-288510 [0xfffb9902]</b>
	 * <p>
	 * Hex (RGB): <b>#fb9902</b>
	 */
	@ColorInt public static final int ORANGE_RYB = Color.rgb(251, 153, 2);

	/**
	 * Constant value: <b>-23296 [0xffffa500]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa500</b>
	 */
	@ColorInt public static final int ORANGE_WEB_COLOR = Color.rgb(255, 165, 0);

	/**
	 * Constant value: <b>-24832 [0xffff9f00]</b>
	 * <p>
	 * Hex (RGB): <b>#ff9f00</b>
	 */
	@ColorInt public static final int ORANGE_PEEL = Color.rgb(255, 159, 0);

	/**
	 * Constant value: <b>-47872 [0xffff4500]</b>
	 * <p>
	 * Hex (RGB): <b>#ff4500</b>
	 */
	@ColorInt public static final int ORANGE_RED = Color.rgb(255, 69, 0);

	/**
	 * Constant value: <b>-2461482 [0xffda70d6]</b>
	 * <p>
	 * Hex (RGB): <b>#da70d6</b>
	 */
	@ColorInt public static final int ORCHID = Color.rgb(218, 112, 214);

	/**
	 * Constant value: <b>-868915 [0xfff2bdcd]</b>
	 * <p>
	 * Hex (RGB): <b>#f2bdcd</b>
	 */
	@ColorInt public static final int ORCHID_PINK = Color.rgb(242, 189, 205);

	/**
	 * Constant value: <b>-307436 [0xfffb4f14]</b>
	 * <p>
	 * Hex (RGB): <b>#fb4f14</b>
	 */
	@ColorInt public static final int ORIOLES_ORANGE = Color.rgb(251, 79, 20);

	/**
	 * Constant value: <b>-10140895 [0xff654321]</b>
	 * <p>
	 * Hex (RGB): <b>#654321</b>
	 */
	@ColorInt public static final int OTTER_BROWN = Color.rgb(101, 67, 33);

	/**
	 * Constant value: <b>-12498356 [0xff414a4c]</b>
	 * <p>
	 * Hex (RGB): <b>#414a4c</b>
	 */
	@ColorInt public static final int OUTER_SPACE = Color.rgb(65, 74, 76);

	/**
	 * Constant value: <b>-37302 [0xffff6e4a]</b>
	 * <p>
	 * Hex (RGB): <b>#ff6e4a</b>
	 */
	@ColorInt public static final int OUTRAGEOUS_ORANGE = Color.rgb(255, 110, 74);

	/**
	 * Constant value: <b>-16768697 [0xff2147]</b>
	 * <p>
	 * Hex (RGB): <b>#002147</b>
	 */
	@ColorInt public static final int OXFORD_BLUE = Color.rgb(0, 33, 71);

	/**
	 * Constant value: <b>-6750208 [0xff990000]</b>
	 * <p>
	 * Hex (RGB): <b>#990000</b>
	 */
	@ColorInt public static final int OU_CRIMSON_RED = Color.rgb(153, 0, 0);

	/**
	 * Constant value: <b>-16751104 [0xff6600]</b>
	 * <p>
	 * Hex (RGB): <b>#006600</b>
	 */
	@ColorInt public static final int PAKISTAN_GREEN = Color.rgb(0, 102, 0);

	/**
	 * Constant value: <b>-14205982 [0xff273be2]</b>
	 * <p>
	 * Hex (RGB): <b>#273be2</b>
	 */
	@ColorInt public static final int PALATINATE_BLUE = Color.rgb(39, 59, 226);

	/**
	 * Constant value: <b>-9951136 [0xff682860]</b>
	 * <p>
	 * Hex (RGB): <b>#682860</b>
	 */
	@ColorInt public static final int PALATINATE_PURPLE = Color.rgb(104, 40, 96);

	/**
	 * Constant value: <b>-4401946 [0xffbcd4e6]</b>
	 * <p>
	 * Hex (RGB): <b>#bcd4e6</b>
	 */
	@ColorInt public static final int PALE_AQUA = Color.rgb(188, 212, 230);

	/**
	 * Constant value: <b>-5247250 [0xffafeeee]</b>
	 * <p>
	 * Hex (RGB): <b>#afeeee</b>
	 */
	@ColorInt public static final int PALE_BLUE = Color.rgb(175, 238, 238);

	/**
	 * Constant value: <b>-6785452 [0xff987654]</b>
	 * <p>
	 * Hex (RGB): <b>#987654</b>
	 */
	@ColorInt public static final int PALE_BROWN = Color.rgb(152, 118, 84);

	/**
	 * Constant value: <b>-5291979 [0xffaf4035]</b>
	 * <p>
	 * Hex (RGB): <b>#af4035</b>
	 */
	@ColorInt public static final int PALE_CARMINE = Color.rgb(175, 64, 53);

	/**
	 * Constant value: <b>-6568734 [0xff9bc4e2]</b>
	 * <p>
	 * Hex (RGB): <b>#9bc4e2</b>
	 */
	@ColorInt public static final int PALE_CERULEAN = Color.rgb(155, 196, 226);

	/**
	 * Constant value: <b>-2249297 [0xffddadaf]</b>
	 * <p>
	 * Hex (RGB): <b>#ddadaf</b>
	 */
	@ColorInt public static final int PALE_CHESTNUT = Color.rgb(221, 173, 175);

	/**
	 * Constant value: <b>-2454937 [0xffda8a67]</b>
	 * <p>
	 * Hex (RGB): <b>#da8a67</b>
	 */
	@ColorInt public static final int PALE_COPPER = Color.rgb(218, 138, 103);

	/**
	 * Constant value: <b>-5517841 [0xffabcdef]</b>
	 * <p>
	 * Hex (RGB): <b>#abcdef</b>
	 */
	@ColorInt public static final int PALE_CORNFLOWER_BLUE = Color.rgb(171, 205, 239);

	/**
	 * Constant value: <b>-1655158 [0xffe6be8a]</b>
	 * <p>
	 * Hex (RGB): <b>#e6be8a</b>
	 */
	@ColorInt public static final int PALE_GOLD = Color.rgb(230, 190, 138);

	/**
	 * Constant value: <b>-1120086 [0xffeee8aa]</b>
	 * <p>
	 * Hex (RGB): <b>#eee8aa</b>
	 */
	@ColorInt public static final int PALE_GOLDENROD = Color.rgb(238, 232, 170);

	/**
	 * Constant value: <b>-6751336 [0xff98fb98]</b>
	 * <p>
	 * Hex (RGB): <b>#98fb98</b>
	 */
	@ColorInt public static final int PALE_GREEN = Color.rgb(152, 251, 152);

	/**
	 * Constant value: <b>-2305793 [0xffdcd0ff]</b>
	 * <p>
	 * Hex (RGB): <b>#dcd0ff</b>
	 */
	@ColorInt public static final int PALE_LAVENDER = Color.rgb(220, 208, 255);

	/**
	 * Constant value: <b>-424731 [0xfff984e5]</b>
	 * <p>
	 * Hex (RGB): <b>#f984e5</b>
	 */
	@ColorInt public static final int PALE_MAGENTA = Color.rgb(249, 132, 229);

	/**
	 * Constant value: <b>-337187 [0xfffadadd]</b>
	 * <p>
	 * Hex (RGB): <b>#fadadd</b>
	 */
	@ColorInt public static final int PALE_PINK = Color.rgb(250, 218, 221);

	/**
	 * Constant value: <b>-2252579 [0xffdda0dd]</b>
	 * <p>
	 * Hex (RGB): <b>#dda0dd</b>
	 */
	@ColorInt public static final int PALE_PLUM = Color.rgb(221, 160, 221);

	/**
	 * Constant value: <b>-2396013 [0xffdb7093]</b>
	 * <p>
	 * Hex (RGB): <b>#db7093</b>
	 */
	@ColorInt public static final int PALE_RED_VIOLET = Color.rgb(219, 112, 147);

	/**
	 * Constant value: <b>-6889775 [0xff96ded1]</b>
	 * <p>
	 * Hex (RGB): <b>#96ded1</b>
	 */
	@ColorInt public static final int PALE_ROBIN_EGG_BLUE = Color.rgb(150, 222, 209);

	/**
	 * Constant value: <b>-3555141 [0xffc9c0bb]</b>
	 * <p>
	 * Hex (RGB): <b>#c9c0bb</b>
	 */
	@ColorInt public static final int PALE_SILVER = Color.rgb(201, 192, 187);

	/**
	 * Constant value: <b>-1250371 [0xffecebbd]</b>
	 * <p>
	 * Hex (RGB): <b>#ecebbd</b>
	 */
	@ColorInt public static final int PALE_SPRING_BUD = Color.rgb(236, 235, 189);

	/**
	 * Constant value: <b>-4417410 [0xffbc987e]</b>
	 * <p>
	 * Hex (RGB): <b>#bc987e</b>
	 */
	@ColorInt public static final int PALE_TAUPE = Color.rgb(188, 152, 126);

	/**
	 * Constant value: <b>-5247250 [0xffafeeee]</b>
	 * <p>
	 * Hex (RGB): <b>#afeeee</b>
	 */
	@ColorInt public static final int PALE_TURQUOISE = Color.rgb(175, 238, 238);

	/**
	 * Constant value: <b>-2396013 [0xffdb7093]</b>
	 * <p>
	 * Hex (RGB): <b>#db7093</b>
	 */
	@ColorInt public static final int PALE_VIOLET_RED = Color.rgb(219, 112, 147);

	/**
	 * Constant value: <b>-8906678 [0xff78184a]</b>
	 * <p>
	 * Hex (RGB): <b>#78184a</b>
	 */
	@ColorInt public static final int PANSY_PURPLE = Color.rgb(120, 24, 74);

	/**
	 * Constant value: <b>-16737411 [0xff9b7d]</b>
	 * <p>
	 * Hex (RGB): <b>#009b7d</b>
	 */
	@ColorInt public static final int PAOLO_VERONESE_GREEN = Color.rgb(0, 155, 125);

	/**
	 * Constant value: <b>-4139 [0xffffefd5]</b>
	 * <p>
	 * Hex (RGB): <b>#ffefd5</b>
	 */
	@ColorInt public static final int PAPAYA_WHIP = Color.rgb(255, 239, 213);

	/**
	 * Constant value: <b>-11483016 [0xff50c878]</b>
	 * <p>
	 * Hex (RGB): <b>#50c878</b>
	 */
	@ColorInt public static final int PARIS_GREEN = Color.rgb(80, 200, 120);

	/**
	 * Constant value: <b>-5323057 [0xffaec6cf]</b>
	 * <p>
	 * Hex (RGB): <b>#aec6cf</b>
	 */
	@ColorInt public static final int PASTEL_BLUE = Color.rgb(174, 198, 207);

	/**
	 * Constant value: <b>-8230573 [0xff826953]</b>
	 * <p>
	 * Hex (RGB): <b>#826953</b>
	 */
	@ColorInt public static final int PASTEL_BROWN = Color.rgb(130, 105, 83);

	/**
	 * Constant value: <b>-3158076 [0xffcfcfc4]</b>
	 * <p>
	 * Hex (RGB): <b>#cfcfc4</b>
	 */
	@ColorInt public static final int PASTEL_GRAY = Color.rgb(207, 207, 196);

	/**
	 * Constant value: <b>-8921737 [0xff77dd77]</b>
	 * <p>
	 * Hex (RGB): <b>#77dd77</b>
	 */
	@ColorInt public static final int PASTEL_GREEN = Color.rgb(119, 221, 119);

	/**
	 * Constant value: <b>-746814 [0xfff49ac2]</b>
	 * <p>
	 * Hex (RGB): <b>#f49ac2</b>
	 */
	@ColorInt public static final int PASTEL_MAGENTA = Color.rgb(244, 154, 194);

	/**
	 * Constant value: <b>-19641 [0xffffb347]</b>
	 * <p>
	 * Hex (RGB): <b>#ffb347</b>
	 */
	@ColorInt public static final int PASTEL_ORANGE = Color.rgb(255, 179, 71);

	/**
	 * Constant value: <b>-2185820 [0xffdea5a4]</b>
	 * <p>
	 * Hex (RGB): <b>#dea5a4</b>
	 */
	@ColorInt public static final int PASTEL_PINK = Color.rgb(222, 165, 164);

	/**
	 * Constant value: <b>-5005643 [0xffb39eb5]</b>
	 * <p>
	 * Hex (RGB): <b>#b39eb5</b>
	 */
	@ColorInt public static final int PASTEL_PURPLE = Color.rgb(179, 158, 181);

	/**
	 * Constant value: <b>-38559 [0xffff6961]</b>
	 * <p>
	 * Hex (RGB): <b>#ff6961</b>
	 */
	@ColorInt public static final int PASTEL_RED = Color.rgb(255, 105, 97);

	/**
	 * Constant value: <b>-3434039 [0xffcb99c9]</b>
	 * <p>
	 * Hex (RGB): <b>#cb99c9</b>
	 */
	@ColorInt public static final int PASTEL_VIOLET = Color.rgb(203, 153, 201);

	/**
	 * Constant value: <b>-131690 [0xfffdfd96]</b>
	 * <p>
	 * Hex (RGB): <b>#fdfd96</b>
	 */
	@ColorInt public static final int PASTEL_YELLOW = Color.rgb(253, 253, 150);

	/**
	 * Constant value: <b>-8388480 [0xff800080]</b>
	 * <p>
	 * Hex (RGB): <b>#800080</b>
	 */
	@ColorInt public static final int PATRIARCH = Color.rgb(128, 0, 128);

	/**
	 * Constant value: <b>-11310984 [0xff536878]</b>
	 * <p>
	 * Hex (RGB): <b>#536878</b>
	 */
	@ColorInt public static final int PAYNE_S_GREY = Color.rgb(83, 104, 120);

	/**
	 * Constant value: <b>-6732 [0xffffe5b4]</b>
	 * <p>
	 * Hex (RGB): <b>#ffe5b4</b>
	 */
	@ColorInt public static final int PEACH = Color.rgb(255, 229, 180);

	/**
	 * Constant value: <b>-13404 [0xffffcba4]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcba4</b>
	 */
	@ColorInt public static final int PEACH_CRAYOLA = Color.rgb(255, 203, 164);

	/**
	 * Constant value: <b>-13159 [0xffffcc99]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcc99</b>
	 */
	@ColorInt public static final int PEACH_ORANGE = Color.rgb(255, 204, 153);

	/**
	 * Constant value: <b>-9543 [0xffffdab9]</b>
	 * <p>
	 * Hex (RGB): <b>#ffdab9</b>
	 */
	@ColorInt public static final int PEACH_PUFF = Color.rgb(255, 218, 185);

	/**
	 * Constant value: <b>-335955 [0xfffadfad]</b>
	 * <p>
	 * Hex (RGB): <b>#fadfad</b>
	 */
	@ColorInt public static final int PEACH_YELLOW = Color.rgb(250, 223, 173);

	/**
	 * Constant value: <b>-3022287 [0xffd1e231]</b>
	 * <p>
	 * Hex (RGB): <b>#d1e231</b>
	 */
	@ColorInt public static final int PEAR = Color.rgb(209, 226, 49);

	/**
	 * Constant value: <b>-1384248 [0xffeae0c8]</b>
	 * <p>
	 * Hex (RGB): <b>#eae0c8</b>
	 */
	@ColorInt public static final int PEARL = Color.rgb(234, 224, 200);

	/**
	 * Constant value: <b>-7808832 [0xff88d8c0]</b>
	 * <p>
	 * Hex (RGB): <b>#88d8c0</b>
	 */
	@ColorInt public static final int PEARL_AQUA = Color.rgb(136, 216, 192);

	/**
	 * Constant value: <b>-4757342 [0xffb768a2]</b>
	 * <p>
	 * Hex (RGB): <b>#b768a2</b>
	 */
	@ColorInt public static final int PEARLY_PURPLE = Color.rgb(183, 104, 162);

	/**
	 * Constant value: <b>-1646080 [0xffe6e200]</b>
	 * <p>
	 * Hex (RGB): <b>#e6e200</b>
	 */
	@ColorInt public static final int PERIDOT = Color.rgb(230, 226, 0);

	/**
	 * Constant value: <b>-3355393 [0xffccccff]</b>
	 * <p>
	 * Hex (RGB): <b>#ccccff</b>
	 */
	@ColorInt public static final int PERIWINKLE = Color.rgb(204, 204, 255);

	/**
	 * Constant value: <b>-14927429 [0xff1c39bb]</b>
	 * <p>
	 * Hex (RGB): <b>#1c39bb</b>
	 */
	@ColorInt public static final int PERSIAN_BLUE = Color.rgb(28, 57, 187);

	/**
	 * Constant value: <b>-16734573 [0xffa693]</b>
	 * <p>
	 * Hex (RGB): <b>#00a693</b>
	 */
	@ColorInt public static final int PERSIAN_GREEN = Color.rgb(0, 166, 147);

	/**
	 * Constant value: <b>-13495686 [0xff32127a]</b>
	 * <p>
	 * Hex (RGB): <b>#32127a</b>
	 */
	@ColorInt public static final int PERSIAN_INDIGO = Color.rgb(50, 18, 122);

	/**
	 * Constant value: <b>-2518952 [0xffd99058]</b>
	 * <p>
	 * Hex (RGB): <b>#d99058</b>
	 */
	@ColorInt public static final int PERSIAN_ORANGE = Color.rgb(217, 144, 88);

	/**
	 * Constant value: <b>-557122 [0xfff77fbe]</b>
	 * <p>
	 * Hex (RGB): <b>#f77fbe</b>
	 */
	@ColorInt public static final int PERSIAN_PINK = Color.rgb(247, 127, 190);

	/**
	 * Constant value: <b>-9429988 [0xff701c1c]</b>
	 * <p>
	 * Hex (RGB): <b>#701c1c</b>
	 */
	@ColorInt public static final int PERSIAN_PLUM = Color.rgb(112, 28, 28);

	/**
	 * Constant value: <b>-3394765 [0xffcc3333]</b>
	 * <p>
	 * Hex (RGB): <b>#cc3333</b>
	 */
	@ColorInt public static final int PERSIAN_RED = Color.rgb(204, 51, 51);

	/**
	 * Constant value: <b>-120670 [0xfffe28a2]</b>
	 * <p>
	 * Hex (RGB): <b>#fe28a2</b>
	 */
	@ColorInt public static final int PERSIAN_ROSE = Color.rgb(254, 40, 162);

	/**
	 * Constant value: <b>-1288192 [0xffec5800]</b>
	 * <p>
	 * Hex (RGB): <b>#ec5800</b>
	 */
	@ColorInt public static final int PERSIMMON = Color.rgb(236, 88, 0);

	/**
	 * Constant value: <b>-3308225 [0xffcd853f]</b>
	 * <p>
	 * Hex (RGB): <b>#cd853f</b>
	 */
	@ColorInt public static final int PERU = Color.rgb(205, 133, 63);

	/**
	 * Constant value: <b>-2162433 [0xffdf00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#df00ff</b>
	 */
	@ColorInt public static final int PHLOX = Color.rgb(223, 0, 255);

	/**
	 * Constant value: <b>-16773239 [0xfff89]</b>
	 * <p>
	 * Hex (RGB): <b>#000f89</b>
	 */
	@ColorInt public static final int PHTHALO_BLUE = Color.rgb(0, 15, 137);

	/**
	 * Constant value: <b>-15583964 [0xff123524]</b>
	 * <p>
	 * Hex (RGB): <b>#123524</b>
	 */
	@ColorInt public static final int PHTHALO_GREEN = Color.rgb(18, 53, 36);

	/**
	 * Constant value: <b>-3994802 [0xffc30b4e]</b>
	 * <p>
	 * Hex (RGB): <b>#c30b4e</b>
	 */
	@ColorInt public static final int PICTORIAL_CARMINE = Color.rgb(195, 11, 78);

	/**
	 * Constant value: <b>-139802 [0xfffddde6]</b>
	 * <p>
	 * Hex (RGB): <b>#fddde6</b>
	 */
	@ColorInt public static final int PIGGY_PINK = Color.rgb(253, 221, 230);

	/**
	 * Constant value: <b>-16680593 [0xff1796f]</b>
	 * <p>
	 * Hex (RGB): <b>#01796f</b>
	 */
	@ColorInt public static final int PINE_GREEN = Color.rgb(1, 121, 111);

	/**
	 * Constant value: <b>-16181 [0xffffc0cb]</b>
	 * <p>
	 * Hex (RGB): <b>#ffc0cb</b>
	 */
	@ColorInt public static final int PINK = Color.rgb(255, 192, 203);

	/**
	 * Constant value: <b>-8716 [0xffffddf4]</b>
	 * <p>
	 * Hex (RGB): <b>#ffddf4</b>
	 */
	@ColorInt public static final int PINK_LACE = Color.rgb(255, 221, 244);

	/**
	 * Constant value: <b>-2379055 [0xffdbb2d1]</b>
	 * <p>
	 * Hex (RGB): <b>#dbb2d1</b>
	 */
	@ColorInt public static final int PINK_LAVENDER = Color.rgb(219, 178, 209);

	/**
	 * Constant value: <b>-26266 [0xffff9966]</b>
	 * <p>
	 * Hex (RGB): <b>#ff9966</b>
	 */
	@ColorInt public static final int PINK_ORANGE = Color.rgb(255, 153, 102);

	/**
	 * Constant value: <b>-1594161 [0xffe7accf]</b>
	 * <p>
	 * Hex (RGB): <b>#e7accf</b>
	 */
	@ColorInt public static final int PINK_PEARL = Color.rgb(231, 172, 207);

	/**
	 * Constant value: <b>-553049 [0xfff78fa7]</b>
	 * <p>
	 * Hex (RGB): <b>#f78fa7</b>
	 */
	@ColorInt public static final int PINK_SHERBET = Color.rgb(247, 143, 167);

	/**
	 * Constant value: <b>-7092878 [0xff93c572]</b>
	 * <p>
	 * Hex (RGB): <b>#93c572</b>
	 */
	@ColorInt public static final int PISTACHIO = Color.rgb(147, 197, 114);

	/**
	 * Constant value: <b>-1710878 [0xffe5e4e2]</b>
	 * <p>
	 * Hex (RGB): <b>#e5e4e2</b>
	 */
	@ColorInt public static final int PLATINUM = Color.rgb(229, 228, 226);

	/**
	 * Constant value: <b>-7453307 [0xff8e4585]</b>
	 * <p>
	 * Hex (RGB): <b>#8e4585</b>
	 */
	@ColorInt public static final int PLUM_TRADITIONAL = Color.rgb(142, 69, 133);

	/**
	 * Constant value: <b>-3368500 [0xffcc99cc]</b>
	 * <p>
	 * Hex (RGB): <b>#cc99cc</b>
	 */
	@ColorInt public static final int PLUM_WEB = Color.rgb(204, 153, 204);

	/**
	 * Constant value: <b>-7970674 [0xff86608e]</b>
	 * <p>
	 * Hex (RGB): <b>#86608e</b>
	 */
	@ColorInt public static final int POMP_AND_POWER = Color.rgb(134, 96, 142);

	/**
	 * Constant value: <b>-4305054 [0xffbe4f62]</b>
	 * <p>
	 * Hex (RGB): <b>#be4f62</b>
	 */
	@ColorInt public static final int POPSTAR = Color.rgb(190, 79, 98);

	/**
	 * Constant value: <b>-42442 [0xffff5a36]</b>
	 * <p>
	 * Hex (RGB): <b>#ff5a36</b>
	 */
	@ColorInt public static final int PORTLAND_ORANGE = Color.rgb(255, 90, 54);

	/**
	 * Constant value: <b>-5185306 [0xffb0e0e6]</b>
	 * <p>
	 * Hex (RGB): <b>#b0e0e6</b>
	 */
	@ColorInt public static final int POWDER_BLUE_WEB = Color.rgb(176, 224, 230);

	/**
	 * Constant value: <b>-28928 [0xffff8f00]</b>
	 * <p>
	 * Hex (RGB): <b>#ff8f00</b>
	 */
	@ColorInt public static final int PRINCETON_ORANGE = Color.rgb(255, 143, 0);

	/**
	 * Constant value: <b>-9429988 [0xff701c1c]</b>
	 * <p>
	 * Hex (RGB): <b>#701c1c</b>
	 */
	@ColorInt public static final int PRUNE = Color.rgb(112, 28, 28);

	/**
	 * Constant value: <b>-16764589 [0xff3153]</b>
	 * <p>
	 * Hex (RGB): <b>#003153</b>
	 */
	@ColorInt public static final int PRUSSIAN_BLUE = Color.rgb(0, 49, 83);

	/**
	 * Constant value: <b>-2162433 [0xffdf00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#df00ff</b>
	 */
	@ColorInt public static final int PSYCHEDELIC_PURPLE = Color.rgb(223, 0, 255);

	/**
	 * Constant value: <b>-3372903 [0xffcc8899]</b>
	 * <p>
	 * Hex (RGB): <b>#cc8899</b>
	 */
	@ColorInt public static final int PUCE = Color.rgb(204, 136, 153);

	/**
	 * Constant value: <b>-35560 [0xffff7518]</b>
	 * <p>
	 * Hex (RGB): <b>#ff7518</b>
	 */
	@ColorInt public static final int PUMPKIN = Color.rgb(255, 117, 24);

	/**
	 * Constant value: <b>-8388480 [0xff800080]</b>
	 * <p>
	 * Hex (RGB): <b>#800080</b>
	 */
	@ColorInt public static final int PURPLE_HTML_CSS = Color.rgb(128, 0, 128);

	/**
	 * Constant value: <b>-6356795 [0xff9f00c5]</b>
	 * <p>
	 * Hex (RGB): <b>#9f00c5</b>
	 */
	@ColorInt public static final int PURPLE_MUNSELL = Color.rgb(159, 0, 197);

	/**
	 * Constant value: <b>-6283024 [0xffa020f0]</b>
	 * <p>
	 * Hex (RGB): <b>#a020f0</b>
	 */
	@ColorInt public static final int PURPLE_X11 = Color.rgb(160, 32, 240);

	/**
	 * Constant value: <b>-9882212 [0xff69359c]</b>
	 * <p>
	 * Hex (RGB): <b>#69359c</b>
	 */
	@ColorInt public static final int PURPLE_HEART = Color.rgb(105, 53, 156);

	/**
	 * Constant value: <b>-6915914 [0xff9678b6]</b>
	 * <p>
	 * Hex (RGB): <b>#9678b6</b>
	 */
	@ColorInt public static final int PURPLE_MOUNTAIN_MAJESTY = Color.rgb(150, 120, 182);

	/**
	 * Constant value: <b>-11644544 [0xff4e5180]</b>
	 * <p>
	 * Hex (RGB): <b>#4e5180</b>
	 */
	@ColorInt public static final int PURPLE_NAVY = Color.rgb(78, 81, 128);

	/**
	 * Constant value: <b>-110886 [0xfffe4eda]</b>
	 * <p>
	 * Hex (RGB): <b>#fe4eda</b>
	 */
	@ColorInt public static final int PURPLE_PIZZAZZ = Color.rgb(254, 78, 218);

	/**
	 * Constant value: <b>-11517875 [0xff50404d]</b>
	 * <p>
	 * Hex (RGB): <b>#50404d</b>
	 */
	@ColorInt public static final int PURPLE_TAUPE = Color.rgb(80, 64, 77);

	/**
	 * Constant value: <b>-11450289 [0xff51484f]</b>
	 * <p>
	 * Hex (RGB): <b>#51484f</b>
	 */
	@ColorInt public static final int QUARTZ = Color.rgb(81, 72, 79);

	/**
	 * Constant value: <b>-12358763 [0xff436b95]</b>
	 * <p>
	 * Hex (RGB): <b>#436b95</b>
	 */
	@ColorInt public static final int QUEEN_BLUE = Color.rgb(67, 107, 149);

	/**
	 * Constant value: <b>-1520425 [0xffe8ccd7]</b>
	 * <p>
	 * Hex (RGB): <b>#e8ccd7</b>
	 */
	@ColorInt public static final int QUEEN_PINK = Color.rgb(232, 204, 215);

	/**
	 * Constant value: <b>-10646872 [0xff5d8aa8]</b>
	 * <p>
	 * Hex (RGB): <b>#5d8aa8</b>
	 */
	@ColorInt public static final int RACKLEY = Color.rgb(93, 138, 168);

	/**
	 * Constant value: <b>-51874 [0xffff355e]</b>
	 * <p>
	 * Hex (RGB): <b>#ff355e</b>
	 */
	@ColorInt public static final int RADICAL_RED = Color.rgb(255, 53, 94);

	/**
	 * Constant value: <b>-283040 [0xfffbae60]</b>
	 * <p>
	 * Hex (RGB): <b>#fbae60</b>
	 */
	@ColorInt public static final int RAJAH = Color.rgb(251, 174, 96);

	/**
	 * Constant value: <b>-1897636 [0xffe30b5c]</b>
	 * <p>
	 * Hex (RGB): <b>#e30b5c</b>
	 */
	@ColorInt public static final int RASPBERRY = Color.rgb(227, 11, 92);

	/**
	 * Constant value: <b>-7250067 [0xff915f6d]</b>
	 * <p>
	 * Hex (RGB): <b>#915f6d</b>
	 */
	@ColorInt public static final int RASPBERRY_GLACE = Color.rgb(145, 95, 109);

	/**
	 * Constant value: <b>-1945448 [0xffe25098]</b>
	 * <p>
	 * Hex (RGB): <b>#e25098</b>
	 */
	@ColorInt public static final int RASPBERRY_PINK = Color.rgb(226, 80, 152);

	/**
	 * Constant value: <b>-5028756 [0xffb3446c]</b>
	 * <p>
	 * Hex (RGB): <b>#b3446c</b>
	 */
	@ColorInt public static final int RASPBERRY_ROSE = Color.rgb(179, 68, 108);

	/**
	 * Constant value: <b>-8231356 [0xff826644]</b>
	 * <p>
	 * Hex (RGB): <b>#826644</b>
	 */
	@ColorInt public static final int RAW_UMBER = Color.rgb(130, 102, 68);

	/**
	 * Constant value: <b>-52276 [0xffff33cc]</b>
	 * <p>
	 * Hex (RGB): <b>#ff33cc</b>
	 */
	@ColorInt public static final int RAZZLE_DAZZLE_ROSE = Color.rgb(255, 51, 204);

	/**
	 * Constant value: <b>-1890965 [0xffe3256b]</b>
	 * <p>
	 * Hex (RGB): <b>#e3256b</b>
	 */
	@ColorInt public static final int RAZZMATAZZ = Color.rgb(227, 37, 107);

	/**
	 * Constant value: <b>-7516539 [0xff8d4e85]</b>
	 * <p>
	 * Hex (RGB): <b>#8d4e85</b>
	 */
	@ColorInt public static final int RAZZMIC_BERRY = Color.rgb(141, 78, 133);

	/**
	 * Constant value: <b>-65536 [0xffff0000]</b>
	 * <p>
	 * Hex (RGB): <b>#ff0000</b>
	 */
	@ColorInt public static final int RED = Color.rgb(255, 0, 0);

	/**
	 * Constant value: <b>-1171379 [0xffee204d]</b>
	 * <p>
	 * Hex (RGB): <b>#ee204d</b>
	 */
	@ColorInt public static final int RED_CRAYOLA = Color.rgb(238, 32, 77);

	/**
	 * Constant value: <b>-917444 [0xfff2003c]</b>
	 * <p>
	 * Hex (RGB): <b>#f2003c</b>
	 */
	@ColorInt public static final int RED_MUNSELL = Color.rgb(242, 0, 60);

	/**
	 * Constant value: <b>-3931597 [0xffc40233]</b>
	 * <p>
	 * Hex (RGB): <b>#c40233</b>
	 */
	@ColorInt public static final int RED_NCS = Color.rgb(196, 2, 51);

	/**
	 * Constant value: <b>-1234631 [0xffed2939]</b>
	 * <p>
	 * Hex (RGB): <b>#ed2939</b>
	 */
	@ColorInt public static final int RED_PANTONE = Color.rgb(237, 41, 57);

	/**
	 * Constant value: <b>-1237980 [0xffed1c24]</b>
	 * <p>
	 * Hex (RGB): <b>#ed1c24</b>
	 */
	@ColorInt public static final int RED_PIGMENT = Color.rgb(237, 28, 36);

	/**
	 * Constant value: <b>-121070 [0xfffe2712]</b>
	 * <p>
	 * Hex (RGB): <b>#fe2712</b>
	 */
	@ColorInt public static final int RED_RYB = Color.rgb(254, 39, 18);

	/**
	 * Constant value: <b>-5952982 [0xffa52a2a]</b>
	 * <p>
	 * Hex (RGB): <b>#a52a2a</b>
	 */
	@ColorInt public static final int RED_BROWN = Color.rgb(165, 42, 42);

	/**
	 * Constant value: <b>-7995119 [0xff860111]</b>
	 * <p>
	 * Hex (RGB): <b>#860111</b>
	 */
	@ColorInt public static final int RED_DEVIL = Color.rgb(134, 1, 17);

	/**
	 * Constant value: <b>-44215 [0xffff5349]</b>
	 * <p>
	 * Hex (RGB): <b>#ff5349</b>
	 */
	@ColorInt public static final int RED_ORANGE = Color.rgb(255, 83, 73);

	/**
	 * Constant value: <b>-3730043 [0xffc71585]</b>
	 * <p>
	 * Hex (RGB): <b>#c71585</b>
	 */
	@ColorInt public static final int RED_VIOLET = Color.rgb(199, 21, 133);

	/**
	 * Constant value: <b>-6006190 [0xffa45a52]</b>
	 * <p>
	 * Hex (RGB): <b>#a45a52</b>
	 */
	@ColorInt public static final int REDWOOD = Color.rgb(164, 90, 82);

	/**
	 * Constant value: <b>-11391616 [0xff522d80]</b>
	 * <p>
	 * Hex (RGB): <b>#522d80</b>
	 */
	@ColorInt public static final int REGALIA = Color.rgb(82, 45, 128);

	/**
	 * Constant value: <b>-16768121 [0xff2387]</b>
	 * <p>
	 * Hex (RGB): <b>#002387</b>
	 */
	@ColorInt public static final int RESOLUTION_BLUE = Color.rgb(0, 35, 135);

	/**
	 * Constant value: <b>-8948074 [0xff777696]</b>
	 * <p>
	 * Hex (RGB): <b>#777696</b>
	 */
	@ColorInt public static final int RHYTHM = Color.rgb(119, 118, 150);

	/**
	 * Constant value: <b>-16760768 [0xff4040]</b>
	 * <p>
	 * Hex (RGB): <b>#004040</b>
	 */
	@ColorInt public static final int RICH_BLACK = Color.rgb(0, 64, 64);

	/**
	 * Constant value: <b>-940034 [0xfff1a7fe]</b>
	 * <p>
	 * Hex (RGB): <b>#f1a7fe</b>
	 */
	@ColorInt public static final int RICH_BRILLIANT_LAVENDER = Color.rgb(241, 167, 254);

	/**
	 * Constant value: <b>-2686912 [0xffd70040]</b>
	 * <p>
	 * Hex (RGB): <b>#d70040</b>
	 */
	@ColorInt public static final int RICH_CARMINE = Color.rgb(215, 0, 64);

	/**
	 * Constant value: <b>-16215344 [0xff892d0]</b>
	 * <p>
	 * Hex (RGB): <b>#0892d0</b>
	 */
	@ColorInt public static final int RICH_ELECTRIC_BLUE = Color.rgb(8, 146, 208);

	/**
	 * Constant value: <b>-5805105 [0xffa76bcf]</b>
	 * <p>
	 * Hex (RGB): <b>#a76bcf</b>
	 */
	@ColorInt public static final int RICH_LAVENDER = Color.rgb(167, 107, 207);

	/**
	 * Constant value: <b>-4823342 [0xffb666d2]</b>
	 * <p>
	 * Hex (RGB): <b>#b666d2</b>
	 */
	@ColorInt public static final int RICH_LILAC = Color.rgb(182, 102, 210);

	/**
	 * Constant value: <b>-5230496 [0xffb03060]</b>
	 * <p>
	 * Hex (RGB): <b>#b03060</b>
	 */
	@ColorInt public static final int RICH_MAROON = Color.rgb(176, 48, 96);

	/**
	 * Constant value: <b>-12301256 [0xff444c38]</b>
	 * <p>
	 * Hex (RGB): <b>#444c38</b>
	 */
	@ColorInt public static final int RIFLE_GREEN = Color.rgb(68, 76, 56);

	/**
	 * Constant value: <b>-16724788 [0xffcccc]</b>
	 * <p>
	 * Hex (RGB): <b>#00cccc</b>
	 */
	@ColorInt public static final int ROBIN_EGG_BLUE = Color.rgb(0, 204, 204);

	/**
	 * Constant value: <b>-7700083 [0xff8a818d]</b>
	 * <p>
	 * Hex (RGB): <b>#8a818d</b>
	 */
	@ColorInt public static final int ROCKET_METALLIC = Color.rgb(138, 129, 141);

	/**
	 * Constant value: <b>-8156778 [0xff838996]</b>
	 * <p>
	 * Hex (RGB): <b>#838996</b>
	 */
	@ColorInt public static final int ROMAN_SILVER = Color.rgb(131, 137, 150);

	/**
	 * Constant value: <b>-65409 [0xffff007f]</b>
	 * <p>
	 * Hex (RGB): <b>#ff007f</b>
	 */
	@ColorInt public static final int ROSE = Color.rgb(255, 0, 127);

	/**
	 * Constant value: <b>-441698 [0xfff9429e]</b>
	 * <p>
	 * Hex (RGB): <b>#f9429e</b>
	 */
	@ColorInt public static final int ROSE_BONBON = Color.rgb(249, 66, 158);

	/**
	 * Constant value: <b>-10008506 [0xff674846]</b>
	 * <p>
	 * Hex (RGB): <b>#674846</b>
	 */
	@ColorInt public static final int ROSE_EBONY = Color.rgb(103, 72, 70);

	/**
	 * Constant value: <b>-4755847 [0xffb76e79]</b>
	 * <p>
	 * Hex (RGB): <b>#b76e79</b>
	 */
	@ColorInt public static final int ROSE_GOLD = Color.rgb(183, 110, 121);

	/**
	 * Constant value: <b>-1890762 [0xffe32636]</b>
	 * <p>
	 * Hex (RGB): <b>#e32636</b>
	 */
	@ColorInt public static final int ROSE_MADDER = Color.rgb(227, 38, 54);

	/**
	 * Constant value: <b>-39220 [0xffff66cc]</b>
	 * <p>
	 * Hex (RGB): <b>#ff66cc</b>
	 */
	@ColorInt public static final int ROSE_PINK = Color.rgb(255, 102, 204);

	/**
	 * Constant value: <b>-5597015 [0xffaa98a9]</b>
	 * <p>
	 * Hex (RGB): <b>#aa98a9</b>
	 */
	@ColorInt public static final int ROSE_QUARTZ = Color.rgb(170, 152, 169);

	/**
	 * Constant value: <b>-4055466 [0xffc21e56]</b>
	 * <p>
	 * Hex (RGB): <b>#c21e56</b>
	 */
	@ColorInt public static final int ROSE_RED = Color.rgb(194, 30, 86);

	/**
	 * Constant value: <b>-7316131 [0xff905d5d]</b>
	 * <p>
	 * Hex (RGB): <b>#905d5d</b>
	 */
	@ColorInt public static final int ROSE_TAUPE = Color.rgb(144, 93, 93);

	/**
	 * Constant value: <b>-5550510 [0xffab4e52]</b>
	 * <p>
	 * Hex (RGB): <b>#ab4e52</b>
	 */
	@ColorInt public static final int ROSE_VALE = Color.rgb(171, 78, 82);

	/**
	 * Constant value: <b>-10158069 [0xff65000b]</b>
	 * <p>
	 * Hex (RGB): <b>#65000b</b>
	 */
	@ColorInt public static final int ROSEWOOD = Color.rgb(101, 0, 11);

	/**
	 * Constant value: <b>-2883584 [0xffd40000]</b>
	 * <p>
	 * Hex (RGB): <b>#d40000</b>
	 */
	@ColorInt public static final int ROSSO_CORSA = Color.rgb(212, 0, 0);

	/**
	 * Constant value: <b>-4419697 [0xffbc8f8f]</b>
	 * <p>
	 * Hex (RGB): <b>#bc8f8f</b>
	 */
	@ColorInt public static final int ROSY_BROWN = Color.rgb(188, 143, 143);

	/**
	 * Constant value: <b>-16762712 [0xff38a8]</b>
	 * <p>
	 * Hex (RGB): <b>#0038a8</b>
	 */
	@ColorInt public static final int ROYAL_AZURE = Color.rgb(0, 56, 168);

	/**
	 * Constant value: <b>-16768154 [0xff2366]</b>
	 * <p>
	 * Hex (RGB): <b>#002366</b>
	 */
	@ColorInt public static final int ROYAL_BLUE_TRADITIONAL = Color.rgb(0, 35, 102);

	/**
	 * Constant value: <b>-12490271 [0xff4169e1]</b>
	 * <p>
	 * Hex (RGB): <b>#4169e1</b>
	 */
	@ColorInt public static final int ROYAL_BLUE_WEB = Color.rgb(65, 105, 225);

	/**
	 * Constant value: <b>-3527534 [0xffca2c92]</b>
	 * <p>
	 * Hex (RGB): <b>#ca2c92</b>
	 */
	@ColorInt public static final int ROYAL_FUCHSIA = Color.rgb(202, 44, 146);

	/**
	 * Constant value: <b>-8891991 [0xff7851a9]</b>
	 * <p>
	 * Hex (RGB): <b>#7851a9</b>
	 */
	@ColorInt public static final int ROYAL_PURPLE = Color.rgb(120, 81, 169);

	/**
	 * Constant value: <b>-337314 [0xfffada5e]</b>
	 * <p>
	 * Hex (RGB): <b>#fada5e</b>
	 */
	@ColorInt public static final int ROYAL_YELLOW = Color.rgb(250, 218, 94);

	/**
	 * Constant value: <b>-3258762 [0xffce4676]</b>
	 * <p>
	 * Hex (RGB): <b>#ce4676</b>
	 */
	@ColorInt public static final int RUBER = Color.rgb(206, 70, 118);

	/**
	 * Constant value: <b>-3080106 [0xffd10056]</b>
	 * <p>
	 * Hex (RGB): <b>#d10056</b>
	 */
	@ColorInt public static final int RUBINE_RED = Color.rgb(209, 0, 86);

	/**
	 * Constant value: <b>-2092705 [0xffe0115f]</b>
	 * <p>
	 * Hex (RGB): <b>#e0115f</b>
	 */
	@ColorInt public static final int RUBY = Color.rgb(224, 17, 95);

	/**
	 * Constant value: <b>-6614754 [0xff9b111e]</b>
	 * <p>
	 * Hex (RGB): <b>#9b111e</b>
	 */
	@ColorInt public static final int RUBY_RED = Color.rgb(155, 17, 30);

	/**
	 * Constant value: <b>-65496 [0xffff0028]</b>
	 * <p>
	 * Hex (RGB): <b>#ff0028</b>
	 */
	@ColorInt public static final int RUDDY = Color.rgb(255, 0, 40);

	/**
	 * Constant value: <b>-4496088 [0xffbb6528]</b>
	 * <p>
	 * Hex (RGB): <b>#bb6528</b>
	 */
	@ColorInt public static final int RUDDY_BROWN = Color.rgb(187, 101, 40);

	/**
	 * Constant value: <b>-1995114 [0xffe18e96]</b>
	 * <p>
	 * Hex (RGB): <b>#e18e96</b>
	 */
	@ColorInt public static final int RUDDY_PINK = Color.rgb(225, 142, 150);

	/**
	 * Constant value: <b>-5759993 [0xffa81c07]</b>
	 * <p>
	 * Hex (RGB): <b>#a81c07</b>
	 */
	@ColorInt public static final int RUFOUS = Color.rgb(168, 28, 7);

	/**
	 * Constant value: <b>-8370661 [0xff80461b]</b>
	 * <p>
	 * Hex (RGB): <b>#80461b</b>
	 */
	@ColorInt public static final int RUSSET = Color.rgb(128, 70, 27);

	/**
	 * Constant value: <b>-9989529 [0xff679267]</b>
	 * <p>
	 * Hex (RGB): <b>#679267</b>
	 */
	@ColorInt public static final int RUSSIAN_GREEN = Color.rgb(103, 146, 103);

	/**
	 * Constant value: <b>-13494451 [0xff32174d]</b>
	 * <p>
	 * Hex (RGB): <b>#32174d</b>
	 */
	@ColorInt public static final int RUSSIAN_VIOLET = Color.rgb(50, 23, 77);

	/**
	 * Constant value: <b>-4767474 [0xffb7410e]</b>
	 * <p>
	 * Hex (RGB): <b>#b7410e</b>
	 */
	@ColorInt public static final int RUST = Color.rgb(183, 65, 14);

	/**
	 * Constant value: <b>-2479037 [0xffda2c43]</b>
	 * <p>
	 * Hex (RGB): <b>#da2c43</b>
	 */
	@ColorInt public static final int RUSTY_RED = Color.rgb(218, 44, 67);

	/**
	 * Constant value: <b>-16755137 [0xff563f]</b>
	 * <p>
	 * Hex (RGB): <b>#00563f</b>
	 */
	@ColorInt public static final int SACRAMENTO_STATE_GREEN = Color.rgb(0, 86, 63);

	/**
	 * Constant value: <b>-7650029 [0xff8b4513]</b>
	 * <p>
	 * Hex (RGB): <b>#8b4513</b>
	 */
	@ColorInt public static final int SADDLE_BROWN = Color.rgb(139, 69, 19);

	/**
	 * Constant value: <b>-39168 [0xffff6700]</b>
	 * <p>
	 * Hex (RGB): <b>#ff6700</b>
	 */
	@ColorInt public static final int SAFETY_ORANGE_BLAZE_ORANGE = Color.rgb(255, 103, 0);

	/**
	 * Constant value: <b>-1125886 [0xffeed202]</b>
	 * <p>
	 * Hex (RGB): <b>#eed202</b>
	 */
	@ColorInt public static final int SAFETY_YELLOW = Color.rgb(238, 210, 2);

	/**
	 * Constant value: <b>-736208 [0xfff4c430]</b>
	 * <p>
	 * Hex (RGB): <b>#f4c430</b>
	 */
	@ColorInt public static final int SAFFRON = Color.rgb(244, 196, 48);

	/**
	 * Constant value: <b>-14472838 [0xff23297a]</b>
	 * <p>
	 * Hex (RGB): <b>#23297a</b>
	 */
	@ColorInt public static final int ST_PATRICK_S_BLUE = Color.rgb(35, 41, 122);

	/**
	 * Constant value: <b>-29591 [0xffff8c69]</b>
	 * <p>
	 * Hex (RGB): <b>#ff8c69</b>
	 */
	@ColorInt public static final int SALMON = Color.rgb(255, 140, 105);

	/**
	 * Constant value: <b>-28252 [0xffff91a4]</b>
	 * <p>
	 * Hex (RGB): <b>#ff91a4</b>
	 */
	@ColorInt public static final int SALMON_PINK = Color.rgb(255, 145, 164);

	/**
	 * Constant value: <b>-4017536 [0xffc2b280]</b>
	 * <p>
	 * Hex (RGB): <b>#c2b280</b>
	 */
	@ColorInt public static final int SAND = Color.rgb(194, 178, 128);

	/**
	 * Constant value: <b>-6917865 [0xff967117]</b>
	 * <p>
	 * Hex (RGB): <b>#967117</b>
	 */
	@ColorInt public static final int SAND_DUNE = Color.rgb(150, 113, 23);

	/**
	 * Constant value: <b>-1256128 [0xffecd540]</b>
	 * <p>
	 * Hex (RGB): <b>#ecd540</b>
	 */
	@ColorInt public static final int SANDSTORM = Color.rgb(236, 213, 64);

	/**
	 * Constant value: <b>-744352 [0xfff4a460]</b>
	 * <p>
	 * Hex (RGB): <b>#f4a460</b>
	 */
	@ColorInt public static final int SANDY_BROWN = Color.rgb(244, 164, 96);

	/**
	 * Constant value: <b>-6917865 [0xff967117]</b>
	 * <p>
	 * Hex (RGB): <b>#967117</b>
	 */
	@ColorInt public static final int SANDY_TAUPE = Color.rgb(150, 113, 23);

	/**
	 * Constant value: <b>-7208950 [0xff92000a]</b>
	 * <p>
	 * Hex (RGB): <b>#92000a</b>
	 */
	@ColorInt public static final int SANGRIA = Color.rgb(146, 0, 10);

	/**
	 * Constant value: <b>-11502294 [0xff507d2a]</b>
	 * <p>
	 * Hex (RGB): <b>#507d2a</b>
	 */
	@ColorInt public static final int SAP_GREEN = Color.rgb(80, 125, 42);

	/**
	 * Constant value: <b>-15772998 [0xfff52ba]</b>
	 * <p>
	 * Hex (RGB): <b>#0f52ba</b>
	 */
	@ColorInt public static final int SAPPHIRE = Color.rgb(15, 82, 186);

	/**
	 * Constant value: <b>-16750683 [0xff67a5]</b>
	 * <p>
	 * Hex (RGB): <b>#0067a5</b>
	 */
	@ColorInt public static final int SAPPHIRE_BLUE = Color.rgb(0, 103, 165);

	/**
	 * Constant value: <b>-3432139 [0xffcba135]</b>
	 * <p>
	 * Hex (RGB): <b>#cba135</b>
	 */
	@ColorInt public static final int SATIN_SHEEN_GOLD = Color.rgb(203, 161, 53);

	/**
	 * Constant value: <b>-56320 [0xffff2400]</b>
	 * <p>
	 * Hex (RGB): <b>#ff2400</b>
	 */
	@ColorInt public static final int SCARLET = Color.rgb(255, 36, 0);

	/**
	 * Constant value: <b>-192971 [0xfffd0e35]</b>
	 * <p>
	 * Hex (RGB): <b>#fd0e35</b>
	 */
	@ColorInt public static final int SCARLET_CRAYOLA = Color.rgb(253, 14, 53);

	/**
	 * Constant value: <b>-28241 [0xffff91af]</b>
	 * <p>
	 * Hex (RGB): <b>#ff91af</b>
	 */
	@ColorInt public static final int SCHAUSS_PINK = Color.rgb(255, 145, 175);

	/**
	 * Constant value: <b>-10240 [0xffffd800]</b>
	 * <p>
	 * Hex (RGB): <b>#ffd800</b>
	 */
	@ColorInt public static final int SCHOOL_BUS_YELLOW = Color.rgb(255, 216, 0);

	/**
	 * Constant value: <b>-8978576 [0xff76ff70]</b>
	 * <p>
	 * Hex (RGB): <b>#76ff70</b>
	 */
	@ColorInt public static final int SCREAMIN_GREEN = Color.rgb(118, 255, 112);

	/**
	 * Constant value: <b>-16750188 [0xff6994]</b>
	 * <p>
	 * Hex (RGB): <b>#006994</b>
	 */
	@ColorInt public static final int SEA_BLUE = Color.rgb(0, 105, 148);

	/**
	 * Constant value: <b>-13697141 [0xff2eff8b]</b>
	 * <p>
	 * Hex (RGB): <b>#2eff8b</b>
	 */
	@ColorInt public static final int SEA_GREEN = Color.rgb(46, 255, 139);

	/**
	 * Constant value: <b>-13495276 [0xff321414]</b>
	 * <p>
	 * Hex (RGB): <b>#321414</b>
	 */
	@ColorInt public static final int SEAL_BROWN = Color.rgb(50, 20, 20);

	/**
	 * Constant value: <b>-2578 [0xfffff5ee]</b>
	 * <p>
	 * Hex (RGB): <b>#fff5ee</b>
	 */
	@ColorInt public static final int SEASHELL = Color.rgb(255, 245, 238);

	/**
	 * Constant value: <b>-17920 [0xffffba00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffba00</b>
	 */
	@ColorInt public static final int SELECTIVE_YELLOW = Color.rgb(255, 186, 0);

	/**
	 * Constant value: <b>-9420268 [0xff704214]</b>
	 * <p>
	 * Hex (RGB): <b>#704214</b>
	 */
	@ColorInt public static final int SEPIA = Color.rgb(112, 66, 20);

	/**
	 * Constant value: <b>-7702179 [0xff8a795d]</b>
	 * <p>
	 * Hex (RGB): <b>#8a795d</b>
	 */
	@ColorInt public static final int SHADOW = Color.rgb(138, 121, 93);

	/**
	 * Constant value: <b>-12303 [0xffffcff1]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcff1</b>
	 */
	@ColorInt public static final int SHAMPOO = Color.rgb(255, 207, 241);

	/**
	 * Constant value: <b>-16736672 [0xff9e60]</b>
	 * <p>
	 * Hex (RGB): <b>#009e60</b>
	 */
	@ColorInt public static final int SHAMROCK_GREEN = Color.rgb(0, 158, 96);

	/**
	 * Constant value: <b>-7351296 [0xff8fd400]</b>
	 * <p>
	 * Hex (RGB): <b>#8fd400</b>
	 */
	@ColorInt public static final int SHEEN_GREEN = Color.rgb(143, 212, 0);

	/**
	 * Constant value: <b>-2521451 [0xffd98695]</b>
	 * <p>
	 * Hex (RGB): <b>#d98695</b>
	 */
	@ColorInt public static final int SHIMMERING_BLUSH = Color.rgb(217, 134, 149);

	/**
	 * Constant value: <b>-258112 [0xfffc0fc0]</b>
	 * <p>
	 * Hex (RGB): <b>#fc0fc0</b>
	 */
	@ColorInt public static final int SHOCKING_PINK = Color.rgb(252, 15, 192);

	/**
	 * Constant value: <b>-36865 [0xffff6fff]</b>
	 * <p>
	 * Hex (RGB): <b>#ff6fff</b>
	 */
	@ColorInt public static final int SHOCKING_PINK_CRAYOLA = Color.rgb(255, 111, 255);

	/**
	 * Constant value: <b>-7852777 [0xff882d17]</b>
	 * <p>
	 * Hex (RGB): <b>#882d17</b>
	 */
	@ColorInt public static final int SIENNA = Color.rgb(136, 45, 23);

	/**
	 * Constant value: <b>-4144960 [0xffc0c0c0]</b>
	 * <p>
	 * Hex (RGB): <b>#c0c0c0</b>
	 */
	@ColorInt public static final int SILVER = Color.rgb(192, 192, 192);

	/**
	 * Constant value: <b>-5460820 [0xffacacac]</b>
	 * <p>
	 * Hex (RGB): <b>#acacac</b>
	 */
	@ColorInt public static final int SILVER_CHALICE = Color.rgb(172, 172, 172);

	/**
	 * Constant value: <b>-10647110 [0xff5d89ba]</b>
	 * <p>
	 * Hex (RGB): <b>#5d89ba</b>
	 */
	@ColorInt public static final int SILVER_LAKE_BLUE = Color.rgb(93, 137, 186);

	/**
	 * Constant value: <b>-4280659 [0xffbeaead]</b>
	 * <p>
	 * Hex (RGB): <b>#beaead</b>
	 */
	@ColorInt public static final int SILVER_PINK = Color.rgb(190, 174, 173);

	/**
	 * Constant value: <b>-4210238 [0xffbfc1c2]</b>
	 * <p>
	 * Hex (RGB): <b>#bfc1c2</b>
	 */
	@ColorInt public static final int SILVER_SAND = Color.rgb(191, 193, 194);

	/**
	 * Constant value: <b>-3456757 [0xffcb410b]</b>
	 * <p>
	 * Hex (RGB): <b>#cb410b</b>
	 */
	@ColorInt public static final int SINOPIA = Color.rgb(203, 65, 11);

	/**
	 * Constant value: <b>-16747404 [0xff7474]</b>
	 * <p>
	 * Hex (RGB): <b>#007474</b>
	 */
	@ColorInt public static final int SKOBELOFF = Color.rgb(0, 116, 116);

	/**
	 * Constant value: <b>-7876885 [0xff87ceeb]</b>
	 * <p>
	 * Hex (RGB): <b>#87ceeb</b>
	 */
	@ColorInt public static final int SKY_BLUE = Color.rgb(135, 206, 235);

	/**
	 * Constant value: <b>-3182161 [0xffcf71af]</b>
	 * <p>
	 * Hex (RGB): <b>#cf71af</b>
	 */
	@ColorInt public static final int SKY_MAGENTA = Color.rgb(207, 113, 175);

	/**
	 * Constant value: <b>-9807155 [0xff6a5acd]</b>
	 * <p>
	 * Hex (RGB): <b>#6a5acd</b>
	 */
	@ColorInt public static final int SLATE_BLUE = Color.rgb(106, 90, 205);

	/**
	 * Constant value: <b>-9404272 [0xff708090]</b>
	 * <p>
	 * Hex (RGB): <b>#708090</b>
	 */
	@ColorInt public static final int SLATE_GRAY = Color.rgb(112, 128, 144);

	/**
	 * Constant value: <b>-16764007 [0xff3399]</b>
	 * <p>
	 * Hex (RGB): <b>#003399</b>
	 */
	@ColorInt public static final int SMALT_DARK_POWDER_BLUE = Color.rgb(0, 51, 153);

	/**
	 * Constant value: <b>-3653242 [0xffc84186]</b>
	 * <p>
	 * Hex (RGB): <b>#c84186</b>
	 */
	@ColorInt public static final int SMITTEN = Color.rgb(200, 65, 134);

	/**
	 * Constant value: <b>-9207178 [0xff738276]</b>
	 * <p>
	 * Hex (RGB): <b>#738276</b>
	 */
	@ColorInt public static final int SMOKE = Color.rgb(115, 130, 118);

	/**
	 * Constant value: <b>-7127743 [0xff933d41]</b>
	 * <p>
	 * Hex (RGB): <b>#933d41</b>
	 */
	@ColorInt public static final int SMOKEY_TOPAZ = Color.rgb(147, 61, 65);

	/**
	 * Constant value: <b>-15725560 [0xff100c08]</b>
	 * <p>
	 * Hex (RGB): <b>#100c08</b>
	 */
	@ColorInt public static final int SMOKY_BLACK = Color.rgb(16, 12, 8);

	/**
	 * Constant value: <b>-1297 [0xfffffaef]</b>
	 * <p>
	 * Hex (RGB): <b>#fffaef</b>
	 */
	@ColorInt public static final int SNOW = Color.rgb(255, 250, 239);

	/**
	 * Constant value: <b>-3225482 [0xffcec876]</b>
	 * <p>
	 * Hex (RGB): <b>#cec876</b>
	 */
	@ColorInt public static final int SOAP = Color.rgb(206, 200, 118);

	/**
	 * Constant value: <b>-9079435 [0xff757575]</b>
	 * <p>
	 * Hex (RGB): <b>#757575</b>
	 */
	@ColorInt public static final int SONIC_SILVER = Color.rgb(117, 117, 117);

	/**
	 * Constant value: <b>-6417642 [0xff9e1316]</b>
	 * <p>
	 * Hex (RGB): <b>#9e1316</b>
	 */
	@ColorInt public static final int SPARTAN_CRIMSON = Color.rgb(158, 19, 22);

	/**
	 * Constant value: <b>-14866095 [0xff1d2951]</b>
	 * <p>
	 * Hex (RGB): <b>#1d2951</b>
	 */
	@ColorInt public static final int SPACE_CADET = Color.rgb(29, 41, 81);

	/**
	 * Constant value: <b>-8358606 [0xff807532]</b>
	 * <p>
	 * Hex (RGB): <b>#807532</b>
	 */
	@ColorInt public static final int SPANISH_BISTRE = Color.rgb(128, 117, 50);

	/**
	 * Constant value: <b>-16748360 [0xff70b8]</b>
	 * <p>
	 * Hex (RGB): <b>#0070b8</b>
	 */
	@ColorInt public static final int SPANISH_BLUE = Color.rgb(0, 112, 184);

	/**
	 * Constant value: <b>-3080121 [0xffd10047]</b>
	 * <p>
	 * Hex (RGB): <b>#d10047</b>
	 */
	@ColorInt public static final int SPANISH_CARMINE = Color.rgb(209, 0, 71);

	/**
	 * Constant value: <b>-1762740 [0xffe51a4c]</b>
	 * <p>
	 * Hex (RGB): <b>#e51a4c</b>
	 */
	@ColorInt public static final int SPANISH_CRIMSON = Color.rgb(229, 26, 76);

	/**
	 * Constant value: <b>-6776680 [0xff989898]</b>
	 * <p>
	 * Hex (RGB): <b>#989898</b>
	 */
	@ColorInt public static final int SPANISH_GRAY = Color.rgb(152, 152, 152);

	/**
	 * Constant value: <b>-1548032 [0xffe86100]</b>
	 * <p>
	 * Hex (RGB): <b>#e86100</b>
	 */
	@ColorInt public static final int SPANISH_ORANGE = Color.rgb(232, 97, 0);

	/**
	 * Constant value: <b>-16711681 [0xffffff]</b>
	 * <p>
	 * Hex (RGB): <b>#00ffff</b>
	 */
	@ColorInt public static final int SPANISH_SKY_BLUE = Color.rgb(0, 255, 255);

	/**
	 * Constant value: <b>-16744612 [0xff7f5c]</b>
	 * <p>
	 * Hex (RGB): <b>#007f5c</b>
	 */
	@ColorInt public static final int SPANISH_VIRIDIAN = Color.rgb(0, 127, 92);

	/**
	 * Constant value: <b>-15744772 [0xfffc0fc]</b>
	 * <p>
	 * Hex (RGB): <b>#0fc0fc</b>
	 */
	@ColorInt public static final int SPIRO_DISCO_BALL = Color.rgb(15, 192, 252);

	/**
	 * Constant value: <b>-5768192 [0xffa7fc00]</b>
	 * <p>
	 * Hex (RGB): <b>#a7fc00</b>
	 */
	@ColorInt public static final int SPRING_BUD = Color.rgb(167, 252, 0);

	/**
	 * Constant value: <b>-16711809 [0xffff7f]</b>
	 * <p>
	 * Hex (RGB): <b>#00ff7f</b>
	 */
	@ColorInt public static final int SPRING_GREEN = Color.rgb(0, 255, 127);

	/**
	 * Constant value: <b>-16745544 [0xff7bb8]</b>
	 * <p>
	 * Hex (RGB): <b>#007bb8</b>
	 */
	@ColorInt public static final int STAR_COMMAND_BLUE = Color.rgb(0, 123, 184);

	/**
	 * Constant value: <b>-12156236 [0xff4682b4]</b>
	 * <p>
	 * Hex (RGB): <b>#4682b4</b>
	 */
	@ColorInt public static final int STEEL_BLUE = Color.rgb(70, 130, 180);

	/**
	 * Constant value: <b>-3394612 [0xffcc33cc]</b>
	 * <p>
	 * Hex (RGB): <b>#cc33cc</b>
	 */
	@ColorInt public static final int STEEL_PINK = Color.rgb(204, 51, 204);

	/**
	 * Constant value: <b>-337314 [0xfffada5e]</b>
	 * <p>
	 * Hex (RGB): <b>#fada5e</b>
	 */
	@ColorInt public static final int STIL_DE_GRAIN_YELLOW = Color.rgb(250, 218, 94);

	/**
	 * Constant value: <b>-6750208 [0xff990000]</b>
	 * <p>
	 * Hex (RGB): <b>#990000</b>
	 */
	@ColorInt public static final int STIZZA = Color.rgb(153, 0, 0);

	/**
	 * Constant value: <b>-11573654 [0xff4f666a]</b>
	 * <p>
	 * Hex (RGB): <b>#4f666a</b>
	 */
	@ColorInt public static final int STORMCLOUD = Color.rgb(79, 102, 106);

	/**
	 * Constant value: <b>-1779345 [0xffe4d96f]</b>
	 * <p>
	 * Hex (RGB): <b>#e4d96f</b>
	 */
	@ColorInt public static final int STRAW = Color.rgb(228, 217, 111);

	/**
	 * Constant value: <b>-238963 [0xfffc5a8d]</b>
	 * <p>
	 * Hex (RGB): <b>#fc5a8d</b>
	 */
	@ColorInt public static final int STRAWBERRY = Color.rgb(252, 90, 141);

	/**
	 * Constant value: <b>-13261 [0xffffcc33]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcc33</b>
	 */
	@ColorInt public static final int SUNGLOW = Color.rgb(255, 204, 51);

	/**
	 * Constant value: <b>-1857449 [0xffe3a857]</b>
	 * <p>
	 * Hex (RGB): <b>#e3a857</b>
	 */
	@ColorInt public static final int SUNRAY = Color.rgb(227, 168, 87);

	/**
	 * Constant value: <b>-338267 [0xfffad6a5]</b>
	 * <p>
	 * Hex (RGB): <b>#fad6a5</b>
	 */
	@ColorInt public static final int SUNSET = Color.rgb(250, 214, 165);

	/**
	 * Constant value: <b>-171181 [0xfffd6353]</b>
	 * <p>
	 * Hex (RGB): <b>#fd6353</b>
	 */
	@ColorInt public static final int SUNSET_ORANGE = Color.rgb(253, 99, 83);

	/**
	 * Constant value: <b>-3183703 [0xffcf6ba9]</b>
	 * <p>
	 * Hex (RGB): <b>#cf6ba9</b>
	 */
	@ColorInt public static final int SUPER_PINK = Color.rgb(207, 107, 169);

	/**
	 * Constant value: <b>-2968436 [0xffd2b48c]</b>
	 * <p>
	 * Hex (RGB): <b>#d2b48c</b>
	 */
	@ColorInt public static final int TAN = Color.rgb(210, 180, 140);

	/**
	 * Constant value: <b>-439040 [0xfff94d00]</b>
	 * <p>
	 * Hex (RGB): <b>#f94d00</b>
	 */
	@ColorInt public static final int TANGELO = Color.rgb(249, 77, 0);

	/**
	 * Constant value: <b>-883456 [0xfff28500]</b>
	 * <p>
	 * Hex (RGB): <b>#f28500</b>
	 */
	@ColorInt public static final int TANGERINE = Color.rgb(242, 133, 0);

	/**
	 * Constant value: <b>-13312 [0xffffcc00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcc00</b>
	 */
	@ColorInt public static final int TANGERINE_YELLOW = Color.rgb(255, 204, 0);

	/**
	 * Constant value: <b>-1805958 [0xffe4717a]</b>
	 * <p>
	 * Hex (RGB): <b>#e4717a</b>
	 */
	@ColorInt public static final int TANGO_PINK = Color.rgb(228, 113, 122);

	/**
	 * Constant value: <b>-12043214 [0xff483c32]</b>
	 * <p>
	 * Hex (RGB): <b>#483c32</b>
	 */
	@ColorInt public static final int TAUPE = Color.rgb(72, 60, 50);

	/**
	 * Constant value: <b>-7633527 [0xff8b8589]</b>
	 * <p>
	 * Hex (RGB): <b>#8b8589</b>
	 */
	@ColorInt public static final int TAUPE_GRAY = Color.rgb(139, 133, 137);

	/**
	 * Constant value: <b>-3084096 [0xffd0f0c0]</b>
	 * <p>
	 * Hex (RGB): <b>#d0f0c0</b>
	 */
	@ColorInt public static final int TEA_GREEN = Color.rgb(208, 240, 192);

	/**
	 * Constant value: <b>-490631 [0xfff88379]</b>
	 * <p>
	 * Hex (RGB): <b>#f88379</b>
	 */
	@ColorInt public static final int TEA_ROSE_ORANGE = Color.rgb(248, 131, 121);

	/**
	 * Constant value: <b>-736574 [0xfff4c2c2]</b>
	 * <p>
	 * Hex (RGB): <b>#f4c2c2</b>
	 */
	@ColorInt public static final int TEA_ROSE_ROSE = Color.rgb(244, 194, 194);

	/**
	 * Constant value: <b>-16744320 [0xff8080]</b>
	 * <p>
	 * Hex (RGB): <b>#008080</b>
	 */
	@ColorInt public static final int TEAL = Color.rgb(0, 128, 128);

	/**
	 * Constant value: <b>-13208184 [0xff367588]</b>
	 * <p>
	 * Hex (RGB): <b>#367588</b>
	 */
	@ColorInt public static final int TEAL_BLUE = Color.rgb(54, 117, 136);

	/**
	 * Constant value: <b>-6691149 [0xff99e6b3]</b>
	 * <p>
	 * Hex (RGB): <b>#99e6b3</b>
	 */
	@ColorInt public static final int TEAL_DEER = Color.rgb(153, 230, 179);

	/**
	 * Constant value: <b>-16743809 [0xff827f]</b>
	 * <p>
	 * Hex (RGB): <b>#00827f</b>
	 */
	@ColorInt public static final int TEAL_GREEN = Color.rgb(0, 130, 127);

	/**
	 * Constant value: <b>-3197834 [0xffcf3476]</b>
	 * <p>
	 * Hex (RGB): <b>#cf3476</b>
	 */
	@ColorInt public static final int TELEMAGENTA = Color.rgb(207, 52, 118);

	/**
	 * Constant value: <b>-3320064 [0xffcd5700]</b>
	 * <p>
	 * Hex (RGB): <b>#cd5700</b>
	 */
	@ColorInt public static final int TENNE_TAWNY = Color.rgb(205, 87, 0);

	/**
	 * Constant value: <b>-1936805 [0xffe2725b]</b>
	 * <p>
	 * Hex (RGB): <b>#e2725b</b>
	 */
	@ColorInt public static final int TERRA_COTTA = Color.rgb(226, 114, 91);

	/**
	 * Constant value: <b>-2572328 [0xffd8bfd8]</b>
	 * <p>
	 * Hex (RGB): <b>#d8bfd8</b>
	 */
	@ColorInt public static final int THISTLE = Color.rgb(216, 191, 216);

	/**
	 * Constant value: <b>-2199647 [0xffde6fa1]</b>
	 * <p>
	 * Hex (RGB): <b>#de6fa1</b>
	 */
	@ColorInt public static final int THULIAN_PINK = Color.rgb(222, 111, 161);

	/**
	 * Constant value: <b>-226900 [0xfffc89ac]</b>
	 * <p>
	 * Hex (RGB): <b>#fc89ac</b>
	 */
	@ColorInt public static final int TICKLE_ME_PINK = Color.rgb(252, 137, 172);

	/**
	 * Constant value: <b>-16074059 [0xffabab5]</b>
	 * <p>
	 * Hex (RGB): <b>#0abab5</b>
	 */
	@ColorInt public static final int TIFFANY_BLUE = Color.rgb(10, 186, 181);

	/**
	 * Constant value: <b>-2060996 [0xffe08d3c]</b>
	 * <p>
	 * Hex (RGB): <b>#e08d3c</b>
	 */
	@ColorInt public static final int TIGER_S_EYE = Color.rgb(224, 141, 60);

	/**
	 * Constant value: <b>-2369582 [0xffdbd7d2]</b>
	 * <p>
	 * Hex (RGB): <b>#dbd7d2</b>
	 */
	@ColorInt public static final int TIMBERWOLF = Color.rgb(219, 215, 210);

	/**
	 * Constant value: <b>-1120768 [0xffeee600]</b>
	 * <p>
	 * Hex (RGB): <b>#eee600</b>
	 */
	@ColorInt public static final int TITANIUM_YELLOW = Color.rgb(238, 230, 0);

	/**
	 * Constant value: <b>-40121 [0xffff6347]</b>
	 * <p>
	 * Hex (RGB): <b>#ff6347</b>
	 */
	@ColorInt public static final int TOMATO = Color.rgb(255, 99, 71);

	/**
	 * Constant value: <b>-9147200 [0xff746cc0]</b>
	 * <p>
	 * Hex (RGB): <b>#746cc0</b>
	 */
	@ColorInt public static final int TOOLBOX = Color.rgb(116, 108, 192);

	/**
	 * Constant value: <b>-14212 [0xffffc87c]</b>
	 * <p>
	 * Hex (RGB): <b>#ffc87c</b>
	 */
	@ColorInt public static final int TOPAZ = Color.rgb(255, 200, 124);

	/**
	 * Constant value: <b>-192971 [0xfffd0e35]</b>
	 * <p>
	 * Hex (RGB): <b>#fd0e35</b>
	 */
	@ColorInt public static final int TRACTOR_RED = Color.rgb(253, 14, 53);

	/**
	 * Constant value: <b>-8355712 [0xff808080]</b>
	 * <p>
	 * Hex (RGB): <b>#808080</b>
	 */
	@ColorInt public static final int TROLLEY_GREY = Color.rgb(128, 128, 128);

	/**
	 * Constant value: <b>-16747170 [0xff755e]</b>
	 * <p>
	 * Hex (RGB): <b>#00755e</b>
	 */
	@ColorInt public static final int TROPICAL_RAIN_FOREST = Color.rgb(0, 117, 94);

	/**
	 * Constant value: <b>-16747569 [0xff73cf]</b>
	 * <p>
	 * Hex (RGB): <b>#0073cf</b>
	 */
	@ColorInt public static final int TRUE_BLUE = Color.rgb(0, 115, 207);

	/**
	 * Constant value: <b>-12485183 [0xff417dc1]</b>
	 * <p>
	 * Hex (RGB): <b>#417dc1</b>
	 */
	@ColorInt public static final int TUFTS_BLUE = Color.rgb(65, 125, 193);

	/**
	 * Constant value: <b>-30835 [0xffff878d]</b>
	 * <p>
	 * Hex (RGB): <b>#ff878d</b>
	 */
	@ColorInt public static final int TULIP = Color.rgb(255, 135, 141);

	/**
	 * Constant value: <b>-2184568 [0xffdeaa88]</b>
	 * <p>
	 * Hex (RGB): <b>#deaa88</b>
	 */
	@ColorInt public static final int TUMBLEWEED = Color.rgb(222, 170, 136);

	/**
	 * Constant value: <b>-4885887 [0xffb57281]</b>
	 * <p>
	 * Hex (RGB): <b>#b57281</b>
	 */
	@ColorInt public static final int TURKISH_ROSE = Color.rgb(181, 114, 129);

	/**
	 * Constant value: <b>-13576760 [0xff30d5c8]</b>
	 * <p>
	 * Hex (RGB): <b>#30d5c8</b>
	 */
	@ColorInt public static final int TURQUOISE = Color.rgb(48, 213, 200);

	/**
	 * Constant value: <b>-16711697 [0xffffef]</b>
	 * <p>
	 * Hex (RGB): <b>#00ffef</b>
	 */
	@ColorInt public static final int TURQUOISE_BLUE = Color.rgb(0, 255, 239);

	/**
	 * Constant value: <b>-6236492 [0xffa0d6b4]</b>
	 * <p>
	 * Hex (RGB): <b>#a0d6b4</b>
	 */
	@ColorInt public static final int TURQUOISE_GREEN = Color.rgb(160, 214, 180);

	/**
	 * Constant value: <b>-338267 [0xfffad6a5]</b>
	 * <p>
	 * Hex (RGB): <b>#fad6a5</b>
	 */
	@ColorInt public static final int TUSCAN = Color.rgb(250, 214, 165);

	/**
	 * Constant value: <b>-9482697 [0xff6f4e37]</b>
	 * <p>
	 * Hex (RGB): <b>#6f4e37</b>
	 */
	@ColorInt public static final int TUSCAN_BROWN = Color.rgb(111, 78, 55);

	/**
	 * Constant value: <b>-8638416 [0xff7c3030]</b>
	 * <p>
	 * Hex (RGB): <b>#7c3030</b>
	 */
	@ColorInt public static final int TUSCAN_RED = Color.rgb(124, 48, 48);

	/**
	 * Constant value: <b>-6259877 [0xffa07b5b]</b>
	 * <p>
	 * Hex (RGB): <b>#a07b5b</b>
	 */
	@ColorInt public static final int TUSCAN_TAN = Color.rgb(160, 123, 91);

	/**
	 * Constant value: <b>-4154983 [0xffc09999]</b>
	 * <p>
	 * Hex (RGB): <b>#c09999</b>
	 */
	@ColorInt public static final int TUSCANY = Color.rgb(192, 153, 153);

	/**
	 * Constant value: <b>-7714453 [0xff8a496b]</b>
	 * <p>
	 * Hex (RGB): <b>#8a496b</b>
	 */
	@ColorInt public static final int TWILIGHT_LAVENDER = Color.rgb(138, 73, 107);

	/**
	 * Constant value: <b>-10091972 [0xff66023c]</b>
	 * <p>
	 * Hex (RGB): <b>#66023c</b>
	 */
	@ColorInt public static final int TYRIAN_PURPLE = Color.rgb(102, 2, 60);

	/**
	 * Constant value: <b>-16763990 [0xff33aa]</b>
	 * <p>
	 * Hex (RGB): <b>#0033aa</b>
	 */
	@ColorInt public static final int UA_BLUE = Color.rgb(0, 51, 170);

	/**
	 * Constant value: <b>-2555828 [0xffd9004c]</b>
	 * <p>
	 * Hex (RGB): <b>#d9004c</b>
	 */
	@ColorInt public static final int UA_RED = Color.rgb(217, 0, 76);

	/**
	 * Constant value: <b>-7833405 [0xff8878c3]</b>
	 * <p>
	 * Hex (RGB): <b>#8878c3</b>
	 */
	@ColorInt public static final int UBE = Color.rgb(136, 120, 195);

	/**
	 * Constant value: <b>-11310955 [0xff536895]</b>
	 * <p>
	 * Hex (RGB): <b>#536895</b>
	 */
	@ColorInt public static final int UCLA_BLUE = Color.rgb(83, 104, 149);

	/**
	 * Constant value: <b>-19712 [0xffffb300]</b>
	 * <p>
	 * Hex (RGB): <b>#ffb300</b>
	 */
	@ColorInt public static final int UCLA_GOLD = Color.rgb(255, 179, 0);

	/**
	 * Constant value: <b>-12791696 [0xff3cd070]</b>
	 * <p>
	 * Hex (RGB): <b>#3cd070</b>
	 */
	@ColorInt public static final int UFO_GREEN = Color.rgb(60, 208, 112);

	/**
	 * Constant value: <b>-15594865 [0xff120a8f]</b>
	 * <p>
	 * Hex (RGB): <b>#120a8f</b>
	 */
	@ColorInt public static final int ULTRAMARINE = Color.rgb(18, 10, 143);

	/**
	 * Constant value: <b>-12491019 [0xff4166f5]</b>
	 * <p>
	 * Hex (RGB): <b>#4166f5</b>
	 */
	@ColorInt public static final int ULTRAMARINE_BLUE = Color.rgb(65, 102, 245);

	/**
	 * Constant value: <b>-36865 [0xffff6fff]</b>
	 * <p>
	 * Hex (RGB): <b>#ff6fff</b>
	 */
	@ColorInt public static final int ULTRA_PINK = Color.rgb(255, 111, 255);

	/**
	 * Constant value: <b>-10268345 [0xff635147]</b>
	 * <p>
	 * Hex (RGB): <b>#635147</b>
	 */
	@ColorInt public static final int UMBER = Color.rgb(99, 81, 71);

	/**
	 * Constant value: <b>-8758 [0xffffddca]</b>
	 * <p>
	 * Hex (RGB): <b>#ffddca</b>
	 */
	@ColorInt public static final int UNBLEACHED_SILK = Color.rgb(255, 221, 202);

	/**
	 * Constant value: <b>-10775835 [0xff5b92e5]</b>
	 * <p>
	 * Hex (RGB): <b>#5b92e5</b>
	 */
	@ColorInt public static final int UNITED_NATIONS_BLUE = Color.rgb(91, 146, 229);

	/**
	 * Constant value: <b>-4749529 [0xffb78727]</b>
	 * <p>
	 * Hex (RGB): <b>#b78727</b>
	 */
	@ColorInt public static final int UNIVERSITY_OF_CALIFORNIA_GOLD = Color.rgb(183, 135, 39);

	/**
	 * Constant value: <b>-154 [0xffffff66]</b>
	 * <p>
	 * Hex (RGB): <b>#ffff66</b>
	 */
	@ColorInt public static final int UNMELLOW_YELLOW = Color.rgb(255, 255, 102);

	/**
	 * Constant value: <b>-16694239 [0xff14421]</b>
	 * <p>
	 * Hex (RGB): <b>#014421</b>
	 */
	@ColorInt public static final int UP_FOREST_GREEN = Color.rgb(1, 68, 33);

	/**
	 * Constant value: <b>-8711917 [0xff7b1113]</b>
	 * <p>
	 * Hex (RGB): <b>#7b1113</b>
	 */
	@ColorInt public static final int UP_MAROON = Color.rgb(123, 17, 19);

	/**
	 * Constant value: <b>-5365719 [0xffae2029]</b>
	 * <p>
	 * Hex (RGB): <b>#ae2029</b>
	 */
	@ColorInt public static final int UPSDELL_RED = Color.rgb(174, 32, 41);

	/**
	 * Constant value: <b>-1987295 [0xffe1ad21]</b>
	 * <p>
	 * Hex (RGB): <b>#e1ad21</b>
	 */
	@ColorInt public static final int UROBILIN = Color.rgb(225, 173, 33);

	/**
	 * Constant value: <b>-16756840 [0xff4f98]</b>
	 * <p>
	 * Hex (RGB): <b>#004f98</b>
	 */
	@ColorInt public static final int USAFA_BLUE = Color.rgb(0, 79, 152);

	/**
	 * Constant value: <b>-6750208 [0xff990000]</b>
	 * <p>
	 * Hex (RGB): <b>#990000</b>
	 */
	@ColorInt public static final int USC_CARDINAL = Color.rgb(153, 0, 0);

	/**
	 * Constant value: <b>-13312 [0xffffcc00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffcc00</b>
	 */
	@ColorInt public static final int USC_GOLD = Color.rgb(255, 204, 0);

	/**
	 * Constant value: <b>-557312 [0xfff77f00]</b>
	 * <p>
	 * Hex (RGB): <b>#f77f00</b>
	 */
	@ColorInt public static final int UNIVERSITY_OF_TENNESSEE_ORANGE = Color.rgb(247, 127, 0);

	/**
	 * Constant value: <b>-2949057 [0xffd3003f]</b>
	 * <p>
	 * Hex (RGB): <b>#d3003f</b>
	 */
	@ColorInt public static final int UTAH_CRIMSON = Color.rgb(211, 0, 63);

	/**
	 * Constant value: <b>-793173 [0xfff3e5ab]</b>
	 * <p>
	 * Hex (RGB): <b>#f3e5ab</b>
	 */
	@ColorInt public static final int VANILLA = Color.rgb(243, 229, 171);

	/**
	 * Constant value: <b>-815191 [0xfff38fa9]</b>
	 * <p>
	 * Hex (RGB): <b>#f38fa9</b>
	 */
	@ColorInt public static final int VANILLA_ICE = Color.rgb(243, 143, 169);

	/**
	 * Constant value: <b>-3820712 [0xffc5b358]</b>
	 * <p>
	 * Hex (RGB): <b>#c5b358</b>
	 */
	@ColorInt public static final int VEGAS_GOLD = Color.rgb(197, 179, 88);

	/**
	 * Constant value: <b>-3667947 [0xffc80815]</b>
	 * <p>
	 * Hex (RGB): <b>#c80815</b>
	 */
	@ColorInt public static final int VENETIAN_RED = Color.rgb(200, 8, 21);

	/**
	 * Constant value: <b>-12340306 [0xff43b3ae]</b>
	 * <p>
	 * Hex (RGB): <b>#43b3ae</b>
	 */
	@ColorInt public static final int VERDIGRIS = Color.rgb(67, 179, 174);

	/**
	 * Constant value: <b>-1883596 [0xffe34234]</b>
	 * <p>
	 * Hex (RGB): <b>#e34234</b>
	 */
	@ColorInt public static final int VERMILION_CINNABAR = Color.rgb(227, 66, 52);

	/**
	 * Constant value: <b>-2541538 [0xffd9381e]</b>
	 * <p>
	 * Hex (RGB): <b>#d9381e</b>
	 */
	@ColorInt public static final int VERMILION_PLOCHERE = Color.rgb(217, 56, 30);

	/**
	 * Constant value: <b>-6283024 [0xffa020f0]</b>
	 * <p>
	 * Hex (RGB): <b>#a020f0</b>
	 */
	@ColorInt public static final int VERONICA = Color.rgb(160, 32, 240);

	/**
	 * Constant value: <b>-7405313 [0xff8f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#8f00ff</b>
	 */
	@ColorInt public static final int VIOLET = Color.rgb(143, 0, 255);

	/**
	 * Constant value: <b>-8453889 [0xff7f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#7f00ff</b>
	 */
	@ColorInt public static final int VIOLET_COLOR_WHEEL = Color.rgb(127, 0, 255);

	/**
	 * Constant value: <b>-7994961 [0xff8601af]</b>
	 * <p>
	 * Hex (RGB): <b>#8601af</b>
	 */
	@ColorInt public static final int VIOLET_RYB = Color.rgb(134, 1, 175);

	/**
	 * Constant value: <b>-1146130 [0xffee82ee]</b>
	 * <p>
	 * Hex (RGB): <b>#ee82ee</b>
	 */
	@ColorInt public static final int VIOLET_WEB = Color.rgb(238, 130, 238);

	/**
	 * Constant value: <b>-13481294 [0xff324ab2]</b>
	 * <p>
	 * Hex (RGB): <b>#324ab2</b>
	 */
	@ColorInt public static final int VIOLET_BLUE = Color.rgb(50, 74, 178);

	/**
	 * Constant value: <b>-568428 [0xfff75394]</b>
	 * <p>
	 * Hex (RGB): <b>#f75394</b>
	 */
	@ColorInt public static final int VIOLET_RED = Color.rgb(247, 83, 148);

	/**
	 * Constant value: <b>-12549523 [0xff40826d]</b>
	 * <p>
	 * Hex (RGB): <b>#40826d</b>
	 */
	@ColorInt public static final int VIRIDIAN = Color.rgb(64, 130, 109);

	/**
	 * Constant value: <b>-16738664 [0xff9698]</b>
	 * <p>
	 * Hex (RGB): <b>#009698</b>
	 */
	@ColorInt public static final int VIRIDIAN_GREEN = Color.rgb(0, 150, 152);

	/**
	 * Constant value: <b>-7198940 [0xff922724]</b>
	 * <p>
	 * Hex (RGB): <b>#922724</b>
	 */
	@ColorInt public static final int VIVID_AUBURN = Color.rgb(146, 39, 36);

	/**
	 * Constant value: <b>-6349515 [0xff9f1d35]</b>
	 * <p>
	 * Hex (RGB): <b>#9f1d35</b>
	 */
	@ColorInt public static final int VIVID_BURGUNDY = Color.rgb(159, 29, 53);

	/**
	 * Constant value: <b>-2482815 [0xffda1d81]</b>
	 * <p>
	 * Hex (RGB): <b>#da1d81</b>
	 */
	@ColorInt public static final int VIVID_CERISE = Color.rgb(218, 29, 129);

	/**
	 * Constant value: <b>-3407617 [0xffcc00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#cc00ff</b>
	 */
	@ColorInt public static final int VIVID_ORCHID = Color.rgb(204, 0, 255);

	/**
	 * Constant value: <b>-16724737 [0xffccff]</b>
	 * <p>
	 * Hex (RGB): <b>#00ccff</b>
	 */
	@ColorInt public static final int VIVID_SKY_BLUE = Color.rgb(0, 204, 255);

	/**
	 * Constant value: <b>-24439 [0xffffa089]</b>
	 * <p>
	 * Hex (RGB): <b>#ffa089</b>
	 */
	@ColorInt public static final int VIVID_TANGERINE = Color.rgb(255, 160, 137);

	/**
	 * Constant value: <b>-6356737 [0xff9f00ff]</b>
	 * <p>
	 * Hex (RGB): <b>#9f00ff</b>
	 */
	@ColorInt public static final int VIVID_VIOLET = Color.rgb(159, 0, 255);

	/**
	 * Constant value: <b>-16760254 [0xff4242]</b>
	 * <p>
	 * Hex (RGB): <b>#004242</b>
	 */
	@ColorInt public static final int WARM_BLACK = Color.rgb(0, 66, 66);

	/**
	 * Constant value: <b>-5966599 [0xffa4f4f9]</b>
	 * <p>
	 * Hex (RGB): <b>#a4f4f9</b>
	 */
	@ColorInt public static final int WATERSPOUT = Color.rgb(164, 244, 249);

	/**
	 * Constant value: <b>-10202030 [0xff645452]</b>
	 * <p>
	 * Hex (RGB): <b>#645452</b>
	 */
	@ColorInt public static final int WENGE = Color.rgb(100, 84, 82);

	/**
	 * Constant value: <b>-663885 [0xfff5deb3]</b>
	 * <p>
	 * Hex (RGB): <b>#f5deb3</b>
	 */
	@ColorInt public static final int WHEAT = Color.rgb(245, 222, 179);

	/**
	 * Constant value: <b>-1 [0xffffffff]</b>
	 * <p>
	 * Hex (RGB): <b>#ffffff</b>
	 */
	@ColorInt public static final int WHITE = Color.rgb(255, 255, 255);

	/**
	 * Constant value: <b>-657931 [0xfff5f5f5]</b>
	 * <p>
	 * Hex (RGB): <b>#f5f5f5</b>
	 */
	@ColorInt public static final int WHITE_SMOKE = Color.rgb(245, 245, 245);

	/**
	 * Constant value: <b>-6115888 [0xffa2add0]</b>
	 * <p>
	 * Hex (RGB): <b>#a2add0</b>
	 */
	@ColorInt public static final int WILD_BLUE_YONDER = Color.rgb(162, 173, 208);

	/**
	 * Constant value: <b>-2658142 [0xffd770a2]</b>
	 * <p>
	 * Hex (RGB): <b>#d770a2</b>
	 */
	@ColorInt public static final int WILD_ORCHID = Color.rgb(215, 112, 162);

	/**
	 * Constant value: <b>-48220 [0xffff43a4]</b>
	 * <p>
	 * Hex (RGB): <b>#ff43a4</b>
	 */
	@ColorInt public static final int WILD_STRAWBERRY = Color.rgb(255, 67, 164);

	/**
	 * Constant value: <b>-234363 [0xfffc6c85]</b>
	 * <p>
	 * Hex (RGB): <b>#fc6c85</b>
	 */
	@ColorInt public static final int WILD_WATERMELON = Color.rgb(252, 108, 133);

	/**
	 * Constant value: <b>-5810942 [0xffa75502]</b>
	 * <p>
	 * Hex (RGB): <b>#a75502</b>
	 */
	@ColorInt public static final int WINDSOR_TAN = Color.rgb(167, 85, 2);

	/**
	 * Constant value: <b>-9294025 [0xff722f37]</b>
	 * <p>
	 * Hex (RGB): <b>#722f37</b>
	 */
	@ColorInt public static final int WINE = Color.rgb(114, 47, 55);

	/**
	 * Constant value: <b>-10014393 [0xff673147]</b>
	 * <p>
	 * Hex (RGB): <b>#673147</b>
	 */
	@ColorInt public static final int WINE_DREGS = Color.rgb(103, 49, 71);

	/**
	 * Constant value: <b>-3563300 [0xffc9a0dc]</b>
	 * <p>
	 * Hex (RGB): <b>#c9a0dc</b>
	 */
	@ColorInt public static final int WISTERIA = Color.rgb(201, 160, 220);

	/**
	 * Constant value: <b>-4089237 [0xffc19a6b]</b>
	 * <p>
	 * Hex (RGB): <b>#c19a6b</b>
	 */
	@ColorInt public static final int WOOD_BROWN = Color.rgb(193, 154, 107);

	/**
	 * Constant value: <b>-9206152 [0xff738678]</b>
	 * <p>
	 * Hex (RGB): <b>#738678</b>
	 */
	@ColorInt public static final int XANADU = Color.rgb(115, 134, 120);

	/**
	 * Constant value: <b>-15774318 [0xfff4d92]</b>
	 * <p>
	 * Hex (RGB): <b>#0f4d92</b>
	 */
	@ColorInt public static final int YALE_BLUE = Color.rgb(15, 77, 146);

	/**
	 * Constant value: <b>-14931903 [0xff1c2841]</b>
	 * <p>
	 * Hex (RGB): <b>#1c2841</b>
	 */
	@ColorInt public static final int YANKEES_BLUE = Color.rgb(28, 40, 65);

	/**
	 * Constant value: <b>-256 [0xffffff00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffff00</b>
	 */
	@ColorInt public static final int YELLOW = Color.rgb(255, 255, 0);

	/**
	 * Constant value: <b>-202621 [0xfffce883]</b>
	 * <p>
	 * Hex (RGB): <b>#fce883</b>
	 */
	@ColorInt public static final int YELLOW_CRAYOLA = Color.rgb(252, 232, 131);

	/**
	 * Constant value: <b>-1061888 [0xffefcc00]</b>
	 * <p>
	 * Hex (RGB): <b>#efcc00</b>
	 */
	@ColorInt public static final int YELLOW_MUNSELL = Color.rgb(239, 204, 0);

	/**
	 * Constant value: <b>-11520 [0xffffd300]</b>
	 * <p>
	 * Hex (RGB): <b>#ffd300</b>
	 */
	@ColorInt public static final int YELLOW_NCS = Color.rgb(255, 211, 0);

	/**
	 * Constant value: <b>-73984 [0xfffedf00]</b>
	 * <p>
	 * Hex (RGB): <b>#fedf00</b>
	 */
	@ColorInt public static final int YELLOW_PANTONE = Color.rgb(254, 223, 0);

	/**
	 * Constant value: <b>-4352 [0xffffef00]</b>
	 * <p>
	 * Hex (RGB): <b>#ffef00</b>
	 */
	@ColorInt public static final int YELLOW_PROCESS = Color.rgb(255, 239, 0);

	/**
	 * Constant value: <b>-65997 [0xfffefe33]</b>
	 * <p>
	 * Hex (RGB): <b>#fefe33</b>
	 */
	@ColorInt public static final int YELLOW_RYB = Color.rgb(254, 254, 51);

	/**
	 * Constant value: <b>-6632142 [0xff9acd32]</b>
	 * <p>
	 * Hex (RGB): <b>#9acd32</b>
	 */
	@ColorInt public static final int YELLOW_GREEN = Color.rgb(154, 205, 50);

	/**
	 * Constant value: <b>-20926 [0xffffae42]</b>
	 * <p>
	 * Hex (RGB): <b>#ffae42</b>
	 */
	@ColorInt public static final int YELLOW_ORANGE = Color.rgb(255, 174, 66);

	/**
	 * Constant value: <b>-4096 [0xfffff000]</b>
	 * <p>
	 * Hex (RGB): <b>#fff000</b>
	 */
	@ColorInt public static final int YELLOW_ROSE = Color.rgb(255, 240, 0);

	/**
	 * Constant value: <b>-16771928 [0xff14a8]</b>
	 * <p>
	 * Hex (RGB): <b>#0014a8</b>
	 */
	@ColorInt public static final int ZAFFRE = Color.rgb(0, 20, 168);

	/**
	 * Constant value: <b>-13887992 [0xff2c1608]</b>
	 * <p>
	 * Hex (RGB): <b>#2c1608</b>
	 */
	@ColorInt public static final int ZINNWALDITE_BROWN = Color.rgb(44, 22, 8);

	/**
	 * Constant value: <b>-12998770 [0xff39a78e]</b>
	 * <p>
	 * Hex (RGB): <b>#39a78e</b>
	 */
	@ColorInt public static final int ZOMP = Color.rgb(57, 167, 142);
}