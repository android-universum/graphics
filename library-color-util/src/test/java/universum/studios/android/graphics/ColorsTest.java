/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.graphics;

import android.graphics.Color;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assume.assumeFalse;

/**
 * @author Martin Albedinsky
 */
@SuppressWarnings("ResourceType")
public final class ColorsTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		Colors.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<Colors> constructor = Colors.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testWithAlphaRatio() {
		// Arrange:
		final int color = Color.parseColor("#FF795548");
		float ratio = 0.01f;
		while (ratio < 1.0f) {
			// Act:
			final int colorWithAlpha = Colors.withAlpha(color, ratio);
			// Assert:
			assertThat(Color.alpha(colorWithAlpha), is((int) (ratio * 255)));
			assertThat(Color.red(colorWithAlpha), is(Color.red(color)));
			assertThat(Color.green(colorWithAlpha), is(Color.green(color)));
			assertThat(Color.blue(colorWithAlpha), is(Color.blue(color)));
			ratio += 0.01f;
		}
	}

	@Test public void testWithAlphaValue() {
		// Arrange:
		for (int i = 0; i < 255; i++) {
			final int color = Colors.random(false);
			// Act:
			final int colorWithAlpha = Colors.withAlpha(color, i);
			// Assert:
			assertThat(Color.alpha(colorWithAlpha), is(i));
			assertThat(Color.red(colorWithAlpha), is(Color.red(color)));
			assertThat(Color.green(colorWithAlpha), is(Color.green(color)));
			assertThat(Color.blue(colorWithAlpha), is(Color.blue(color)));
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWithAlphaRatioOutOfRangeOver() {
		// Act:
		Colors.withAlpha(0, 1.1f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWithAlphaRatioOutOfRangeUnder() {
		// Act:
		Colors.withAlpha(0, -0.1f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWithAlphaOutOfRangeOver() {
		// Act:
		Colors.withAlpha(0, 256);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWithAlphaOutOfRangeUnder() {
		// Act:
		Colors.withAlpha(0, -1);
	}

	@Test public void testBlendWithDefaultRatio() {
		// Arrange:
		final int firstColor = Color.parseColor("#FF2196F3");
		final int secondColor = Color.parseColor("#FF4CAF50");
		// Act + Assert:
		assertThat(Colors.blend(firstColor, secondColor), is(Colors.blend(firstColor, secondColor, 0.5f)));
	}

	@Test public void testBlendWithRatio() {
		// Arrange:
		final int firstColor = Color.parseColor("#FF2196F3");
		final int secondColor = Color.parseColor("#FF4CAF50");
		// Act + Assert:
		assertThat(Colors.blend(firstColor, secondColor, 0f), is(secondColor));
		assertThat(Colors.blend(firstColor, secondColor, 1f), is(firstColor));
		assertThat(Colors.blend(firstColor, secondColor, 0.5f), is(not(firstColor)));
		assertThat(Colors.blend(firstColor, secondColor, 0.5f), is(not(secondColor)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBlendWithRatioOutOfRangeOver() {
		// Act:
		Colors.blend(0, 0, 1.1f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBlendWithRatioOutOfRangeUnder() {
		// Act:
		Colors.blend(0, 0, -0.1f);
	}

	@Test public void testBrighter() {
		// Arrange:
		final int color = Color.parseColor("#FF3F51B5");
		// Act + Assert:
		assertThat(Colors.brighter(color, 0f), is(color));
		float fraction = 0.01f;
		while (fraction < 1.0f) {
			final int brighterColor = Colors.brighter(color, fraction);
			assertThat(brighterColor, greaterThan(color));
			assertThat(Color.alpha(brighterColor), is(Color.alpha(color)));
			fraction += 0.01f;
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBrighterWithFractionOutOfRangeOver() {
		// Act:
		Colors.brighter(0, 1.1f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBrighterWithFractionOutOfRangeUnder() {
		// Act:
		Colors.brighter(0, -0.1f);
	}

	@Test public void testDarker() {
		// Arrange:
		final int color = Color.parseColor("#FFF44336");
		// Act + Assert:
		assertThat(Colors.darker(color, 0f), is(color));
		float fraction = 0.01f;
		while (fraction < 1.0f) {
			final int darkerColor = Colors.darker(color, fraction);
			assertThat(darkerColor, lessThan(color));
			assertThat(Color.alpha(darkerColor), is(Color.alpha(color)));
			fraction += 0.01f;
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDarkerWithFractionOutOfRangeOver() {
		// Act:
		Colors.darker(0, 1.1f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDarkerWithFractionOutOfRangeUnder() {
		// Act:
		Colors.darker(0, -0.1f);
	}

	@Test public void testRandom() {
		// Arrange:
		final int colorsCount = 1000;
		final List<Integer> colors = new ArrayList<>();
		for (int i = 0; i < colorsCount; i++) {
			// Act:
			final int randomColor = Colors.random(i % 2 == 0);
			// Assert:
			assumeFalse(colors.contains(randomColor));
			colors.add(randomColor);
		}
	}

	@Test public void testHexName() {
		// Act + Assert:
		assertThat(Colors.hexName(Color.RED), is("#ffff0000"));
	}

	@Test public void testHexNameWithoutAlpha() {
		// Act + Assert:
		assertThat(Colors.hexName(Color.RED, false), is("#ff0000"));
	}
}