/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.graphics;

import android.graphics.Color;

import java.util.Random;

import androidx.annotation.ColorInt;
import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

/**
 * Utility class that may be used to change components of a specific color, like its alpha or to
 * blend two colors together.
 *
 * <h3>Utility methods</h3>
 * <ul>
 * <li>{@link #withAlpha(int, float)}</li>
 * <li>{@link #withAlpha(int, int)}</li>
 * <li>{@link #blend(int, int)}</li>
 * <li>{@link #blend(int, int, float)}</li>
 * <li>{@link #brighter(int, float)}</li>
 * <li>{@link #darker(int, float)}</li>
 * <li>{@link #random(boolean)}</li>
 * <li>{@link #hexName(int)}</li>
 * <li>{@link #hexName(int, boolean)}</li>
 * </ul>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class Colors {

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "Colors";

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Minimum value for color component (red, green, blue, alpha).
	 */
	private static final int COMPONENT_VALUE_MIN = 0;

	/**
	 * Maximum value for color component (red, green, blue, alpha).
	 */
	private static final int COMPONENT_VALUE_MAX = 255;

	/**
	 * Default ratio used for color component operations like blending, brightening, etc.
	 */
	private static final float RATIO_DEFAULT = 0.5f;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/*
	 * Constructors ================================================================================
	 */

	/**
	 */
	private Colors() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Same as {@link #withAlpha(int, int)} with alpha ratio support.
	 * <p>
	 * Alpha ratio must be in the [0.0, 1.0] range, otherwise an exception is thrown.
	 *
	 * @param color      Color to transform.
	 * @param alphaRatio Ratio used to calculate value of alpha component (0.0 = fully transparent, 1.0 = fully opaque).
	 * @return New color with the same <b>RGB</b> values and alpha value in the requested ratio.
	 * @throws IllegalArgumentException If the specified alpha ratio is not from the required range.
	 */
	@ColorInt public static int withAlpha(@ColorInt final int color, @FloatRange(from = 0, to = 1) final float alphaRatio) {
		if (alphaRatio < 0.0 || alphaRatio > 1.0) {
			throw new IllegalArgumentException("Alpha ratio(" + alphaRatio + ") is out of [0.0, 1.0f] range.");
		}
		return withAlpha(color, (int) (alphaRatio * COMPONENT_VALUE_MAX));
	}

	/**
	 * Transforms the given <var>color</var> in a way that changes its alpha value to the given one.
	 * <p>
	 * Alpha value must be in the [0, 255] range, otherwise an exception is thrown.
	 *
	 * @param color Color to transform.
	 * @param alpha New value of alpha component (0 = fully transparent, 255 = fully opaque).
	 * @return New color with the same <b>RGB</b> values and the requested alpha value.
	 * @throws IllegalArgumentException If the specified alpha is not from the required range.
	 */
	@ColorInt public static int withAlpha(@ColorInt final int color, @IntRange(from = COMPONENT_VALUE_MIN, to = COMPONENT_VALUE_MAX) final int alpha) {
		if (alpha < COMPONENT_VALUE_MIN || alpha > COMPONENT_VALUE_MAX) {
			throw new IllegalArgumentException("Alpha(" + alpha + ") is out of [0, 255] range.");
		}
		return Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color));
	}

	/**
	 * Same as {@link #blend(int, int, float)} with {@code 0.5} blending ratio.
	 */
	@ColorInt public static int blend(@ColorInt final int first, @ColorInt final int second) {
		return blend(first, second, RATIO_DEFAULT);
	}

	/**
	 * Blends the given colors together in the specified <var>blending</var>.
	 * <p>
	 * Blending ratio must be in the [0.0, 1.0] range, otherwise an exception is thrown.
	 *
	 * @param first         First color to be blended with the second one.
	 * @param second        Second color to be blended with the first one.
	 * @param blendingRatio Ratio used to blend the given two colors. Use ratio from range [0.0, 1.0].
	 *                      {@code 1.0} ratio will return <var>first</var>, {@code 0.0} ratio
	 *                      will return <var>second</var> color.
	 * @return New color blended from the given colors according to the requested ratio.
	 * @throws IllegalArgumentException If the specified blending ratio is not from the required range.
	 */
	@SuppressWarnings("UnnecessaryLocalVariable")
	@ColorInt public static int blend(@ColorInt final int first, @ColorInt final int second, @FloatRange(from = 0, to = 1) final float blendingRatio) {
		if (blendingRatio < 0.0 || blendingRatio > 1.0) {
			throw new IllegalArgumentException("Blending ratio(" + blendingRatio + ") is out of [0.0, 1.0f] range.");
		}
		// Initialize ratios for each color.
		// fc = first color, sc = second color
		final float fcRatio = blendingRatio;
		final float scRatio = 1.0f - blendingRatio;
		// Initialize new components for blended color.
		final int red = (int) ((Color.red(first) * fcRatio) + (Color.red(second) * scRatio));
		final int green = (int) ((Color.green(first) * fcRatio) + (Color.green(second) * scRatio));
		final int blue = (int) ((Color.blue(first) * fcRatio) + (Color.blue(second) * scRatio));
		final int alpha = (int) ((Color.alpha(first) * fcRatio) + (Color.alpha(second) * scRatio));
		return Color.argb(alpha, red, green, blue);
	}

	/**
	 * Transforms the given <var>color</var> by changing its components (except alpha) to the brightest
	 * ones by multiplying theirs current value with ({@code 1.0 + fraction}).
	 * <p>
	 * Transforming fraction must be in the [0.0, 1.0] range, otherwise an exception is thrown.
	 *
	 * @param fraction Fraction used to transform color components. Use for example {@code 0.75} if
	 *                 the resulting color should be brighter by 75%.
	 * @return New brighter color or the specified {@code color}.
	 * @throws IllegalArgumentException If the specified fraction is not from the required range.
	 *
	 * @see #darker(int, float)
	 * @see #blend(int, int, float)
	 */
	@ColorInt public static int brighter(@ColorInt final int color, @FloatRange(from = 0, to = 1) final float fraction) {
		if (fraction < 0.0f || fraction > 1.0f) {
			throw new IllegalArgumentException("Fraction(" + fraction + ") is out of [0.0, 1.0f] range.");
		}
		// Make color components brighter using given fraction.
		final int red = (int) Math.round(Color.red(color) * (1.0 + fraction));
		final int green = (int) Math.round(Color.green(color) * (1.0 + fraction));
		final int blue = (int) Math.round(Color.blue(color) * (1.0 + fraction));
		return Color.argb(
				Color.alpha(color),
				// Ensure that the calculated components are in the allowed interval.
				ensureThatComponentValueIsInAllowedInterval(red),
				ensureThatComponentValueIsInAllowedInterval(green),
				ensureThatComponentValueIsInAllowedInterval(blue)
		);
	}

	/**
	 * Transforms the given <var>color</var> by changing its components (except alpha) to the darkest
	 * ones by multiplying theirs current value with ({@code 1.0 - fraction}).
	 * <p>
	 * Transforming fraction must be in the [0.0, 1.0] range, otherwise an exception is thrown.
	 *
	 * @param fraction Fraction used to transform color components. Use for example {@code 0.25} if
	 *                 the resulting color should be darker by 25%.
	 * @return New darker color or the specified {@code color}.
	 * @throws IllegalArgumentException If the specified fraction is not from the required range.
	 *
	 * @see #brighter(int, float)
	 * @see #blend(int, int, float)
	 */
	@ColorInt public static int darker(@ColorInt final int color, @FloatRange(from = 0, to = 1) final float fraction) {
		if (fraction < 0.0f || fraction > 1.0f) {
			throw new IllegalArgumentException("Fraction(" + fraction + ") is out of [0.0, 1.0f] range.");
		}
		// Make color components darker using given fraction.
		final int red = (int) Math.round(Color.red(color) * (1.0 - fraction));
		final int green = (int) Math.round(Color.green(color) * (1.0 - fraction));
		final int blue = (int) Math.round(Color.blue(color) * (1.0 - fraction));
		return Color.argb(
				Color.alpha(color),
				// Ensure that the calculated components are in the allowed interval.
				ensureThatComponentValueIsInAllowedInterval(red),
				ensureThatComponentValueIsInAllowedInterval(green),
				ensureThatComponentValueIsInAllowedInterval(blue)
		);
	}

	/**
	 * Ensures that the given color component <var>value</var> is in
	 * [{@link #COMPONENT_VALUE_MIN}, {@link #COMPONENT_VALUE_MAX}] interval.
	 *
	 * @param value The component value to be ensured.
	 * @return The given value if it is in the allowed interval. If the given value is lower than
	 * {@link #COMPONENT_VALUE_MIN} the {@link #COMPONENT_VALUE_MIN} is returned. If the given value
	 * is greater than {@link #COMPONENT_VALUE_MAX} the {@link #COMPONENT_VALUE_MAX} is returned.
	 */
	private static int ensureThatComponentValueIsInAllowedInterval(final int value) {
		return Math.max(COMPONENT_VALUE_MIN, Math.min(COMPONENT_VALUE_MAX, value));
	}

	/**
	 * Returns a new color with random components.
	 *
	 * @param randomAlpha If {@code true}, the random selection will be also applied to the alpha
	 *                    component, otherwise alpha component will be set to {@code 255}.
	 * @return New random color.
	 */
	@ColorInt public static int random(final boolean randomAlpha) {
		final Random rand = new Random();
		return Color.argb(
				randomAlpha ? rand.nextInt(COMPONENT_VALUE_MAX + 1) : COMPONENT_VALUE_MAX,
				rand.nextInt(COMPONENT_VALUE_MAX + 1),
				rand.nextInt(COMPONENT_VALUE_MAX + 1),
				rand.nextInt(COMPONENT_VALUE_MAX + 1)
		);
	}

	/**
	 * Same as {@link #hexName(int, boolean)} with <var>withAlpha</var> flag set to {@code true}.
	 */
	@NonNull public static String hexName(@ColorInt final int color) {
		return hexName(color, true);
	}

	/**
	 * Returns the name of the specified color represented by hexadecimal values of its current components.
	 *
	 * @param color     The color of which hexadecimal name to obtain.
	 * @param withAlpha If {@code true}, the hexadecimal name will be returned also with the alpha
	 *                  component, otherwise without it.
	 * @return Hexadecimal name of the color, like: {@code black color => #000000, white color => #ffffff or with
	 * alpha #33000000, #00ffffff}.
	 */
	@NonNull public static String hexName(@ColorInt final int color, final boolean withAlpha) {
		String name = withAlpha ? createComponentValueHexName(Color.alpha(color)) : "";
		name += createComponentValueHexName(Color.red(color));
		name += createComponentValueHexName(Color.green(color));
		name += createComponentValueHexName(Color.blue(color));
		return "#" + name;
	}

	/**
	 * Creates a displayable name of the given color component <var>value</var>.
	 *
	 * @param value The component value for which to create name.
	 * @return Hexadecimal name of the color component value always two chars in length.
	 */
	private static String createComponentValueHexName(final int value) {
		final String valueAsString = Integer.toString(value, 16);
		return valueAsString.length() == 2 ? valueAsString : "0" + valueAsString;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}