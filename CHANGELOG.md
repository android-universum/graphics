Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.1.2](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 25.04.2020

- Regular **maintenance**.

### [1.1.1](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.1.0](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 14.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.0.3](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 12.06.2018

- Small updates.

### [1.0.2](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 03.11.2017

- **Updated** to the latest **to date available** dependencies. **Mainly** to the _Android Platform 27 (8.1)_.

### [1.0.1](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 28.07.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_.

### [1.0.0](https://bitbucket.org/android-universum/graphics/wiki/version/1.x) ###
> 17.01.2017

- First production release.